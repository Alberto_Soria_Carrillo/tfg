package jUnit;

import clasesAuxiliares.*;
import com.google.gson.Gson;
import libreriaAlberto.Crypto;
import modelo.Modelo;
import rsa.RSA;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.time.LocalDate;
import java.time.LocalTime;

/**
 * Clase ModelTest. encargado de realizar los test de funcionamiento de la clase model
 *
 * @author Alberto Soria Carrillo
 */
class ModeloTest {

    @org.junit.jupiter.api.Test
    void descrifrarTrabajador() throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException, NoSuchProviderException, IOException {
        RSA rsa =  new RSA();
        rsa.genKeyPair(4096);
        Modelo modelo = new Modelo();

        Trabajador trabajador = new Trabajador();
        trabajador.setId(1);
        trabajador.setDni_cif("Prueba1");
        trabajador.setDni_cif("Prueba1");
        trabajador.setNumero_ss("Prueba1");
        trabajador.setNombre("Prueba1");
        trabajador.setApellido1("Prueba1");
        trabajador.setApellido2("Prueba1");
        trabajador.setFecha_nacimiento("Prueba1");
        trabajador.setGenero("Prueba1");
        trabajador.setEstado("Prueba1");
        trabajador.setNacionalidad("Prueba1");
        trabajador.setHorario("Prueba1");
        trabajador.setTelefono("Prueba1");
        trabajador.setEmail("Prueba1");
        trabajador.setPais("Prueba1");
        trabajador.setCiudad("Prueba1");
        trabajador.setCalle("Prueba1");
        trabajador.setTipo_usuario("Prueba1");
        trabajador.setUser_pass(0);

        trabajador = modelo.descrifrarTrabajador(modelo.EncriptarTrabajador(trabajador,rsa),rsa, new Gson(), new Crypto(), new DataInputStream(new Socket("localhost",4444).getInputStream()), "Montessori2020-2021", false);

    }

    @org.junit.jupiter.api.Test
    void descifrarContratoTrabajador() throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, IOException, NoSuchProviderException, InvalidKeySpecException {
        RSA rsa =  new RSA();
        rsa.genKeyPair(4096);
        Modelo modelo = new Modelo();

        ContratoTrabajador contratoTrabajador = new ContratoTrabajador();

        contratoTrabajador.setId(0);
        contratoTrabajador.setHorario("Prueba1");
        contratoTrabajador.setRango("Prueba1");
        contratoTrabajador.setFecha_inicio("Prueba1");
        contratoTrabajador.setFecha_finalizacion("Prueba1");
        contratoTrabajador.setPago_diario("Prueba1");
        contratoTrabajador.setFecha_despido("Prueba1");
        contratoTrabajador.setTipo_despido("Prueba1");
        contratoTrabajador.setPais("Prueba1");
        contratoTrabajador.setCiudad("Prueba1");
        contratoTrabajador.setCalle("Prueba1");

        contratoTrabajador = modelo.descifrarContratoTrabajador(modelo.EncriptarContratoTrabajador(contratoTrabajador,rsa),rsa, new Gson(), new Crypto(), new DataInputStream(new Socket("localhost",4444).getInputStream()), "Montessori2020-2021", false);

    }

    @org.junit.jupiter.api.Test
    void descrifrarEntradas() throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException, NoSuchProviderException, UnsupportedEncodingException {
        RSA rsa =  new RSA();
        rsa.genKeyPair(4096);
        Modelo modelo = new Modelo();

        Entradas entradas = new Entradas();

        entradas.setFecha(rsa.Encrypt(String.valueOf(LocalDate.now())));
        entradas.setId(0);
        entradas.setSalida(rsa.Encrypt(String.valueOf(LocalTime.now())));
        entradas.setTrabajador(0);

        entradas = modelo.descrifrarEntradas(entradas,rsa);

    }

    @org.junit.jupiter.api.Test
    void descrifrarSalidas() throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException, NoSuchProviderException, UnsupportedEncodingException {
        RSA rsa =  new RSA();
        rsa.genKeyPair(4096);
        Modelo modelo = new Modelo();

        Salidas salidas = new Salidas();

        salidas.setFecha(rsa.Encrypt(String.valueOf(LocalDate.now())));
        salidas.setId(0);
        salidas.setSalida(rsa.Encrypt(String.valueOf(LocalTime.now())));
        salidas.setTrabajador(0);

        salidas = modelo.descrifrarSalidas(salidas,rsa);
    }

    @org.junit.jupiter.api.Test
    void descifrarEmpresa() throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, IOException, NoSuchProviderException, InvalidKeySpecException {
        RSA rsa =  new RSA();
        rsa.genKeyPair(4096);
        Modelo modelo = new Modelo();

        Empresa empresa = new Empresa();

        empresa.setCIF("Prueba1");
        empresa.setId(0);
        empresa.setTexto("Prueba1");
        empresa.setFecha("Prueba1");
        empresa.setPago_notario("Prueba1");
        empresa.setResumen_condiciones("Prueba1");
        empresa.setObservaciones("Prueba1");
        empresa.setNombre_empresa("Prueba1");
        empresa.setTipo_empresa("Prueba1");
        empresa.setCapital_aportado("Prueba1");
        empresa.setPais("Prueba1");
        empresa.setCiudad("Prueba1");
        empresa.setCalle("Prueba1");

        Notario notario = new Notario();

        notario.setDNI_CIF("Prueba1");
        notario.setId(0);
        notario.setNombre("Prueba1");
        notario.setNumero_colegiado("Prueba1");
        notario.setPais("Prueba1");
        notario.setCiudad("Prueba1");
        notario.setCalle("Prueba1");

        empresa.setNotario_id(notario);

        empresa = modelo.descifrarEmpresa(modelo.EncriptarEmpresa(empresa,rsa), rsa, new Gson(), new Crypto(), new DataInputStream(new Socket("localhost",4444).getInputStream()), "Montessori2020-2021", false);

    }

    @org.junit.jupiter.api.Test
    void descifrarAportadores() throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException, NoSuchProviderException, InvalidKeySpecException {
        RSA rsa =  new RSA();
        rsa.genKeyPair(4096);
        Modelo modelo = new Modelo();

        Aportadores aportadores = new Aportadores();

        aportadores.setDNI_CIF("Prueba1");
        aportadores.setId(0);
        aportadores.setNombre("Prueba1");
        aportadores.setCantidad("Prueba1");
        aportadores.setMoneda("Prueba1");

        aportadores = modelo.descifrarAportadores(modelo.EncriptarAportadores(aportadores,rsa, false),rsa);
    }

    @org.junit.jupiter.api.Test
    void descifrarPapeles() throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException, NoSuchProviderException, InvalidKeySpecException {

        RSA rsa =  new RSA();
        rsa.genKeyPair(4096);
        Modelo modelo = new Modelo();

        Papeles papeles =  new Papeles();

        papeles.setId(0);
        papeles.setNombre("Prueba1");
        papeles.setNumero_referencia("Prueba1");
        papeles.setDescripcion("Prueba1");
        papeles.setObservaciones("Prueba1");
        papeles.setRuta("Prueba1");
        papeles.setFecha_guardado("Prueba1");
        papeles.setFecha_papeles("Prueba1");

        papeles = modelo.descifrarPapeles(modelo.EncriptarPapeles(papeles,rsa),rsa);

    }

    @org.junit.jupiter.api.Test
    void descifrarAccionistasParticipadores() throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException, NoSuchProviderException, InvalidKeySpecException {

        RSA rsa =  new RSA();
        rsa.genKeyPair(4096);
        Modelo modelo = new Modelo();

        AccionistasParticipadores accionistasParticipadores =  new AccionistasParticipadores();

        accionistasParticipadores.setDNI_CIF("Prueba1");
        accionistasParticipadores.setId(0);
        accionistasParticipadores.setNombre("Prueba1");
        accionistasParticipadores.setPorcetnaje("Prueba1");
        accionistasParticipadores.setTotales("Prueba1");
        accionistasParticipadores.setTipo_moneda("Prueba1");

        accionistasParticipadores = modelo.descifrarAccionistasParticipadores(modelo.EncriptarAccionistasParticipadores(accionistasParticipadores,rsa),rsa);

    }

    @org.junit.jupiter.api.Test
    void descifrarTestigo() throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException, NoSuchProviderException, InvalidKeySpecException {

        RSA rsa =  new RSA();
        rsa.genKeyPair(4096);
        Modelo modelo = new Modelo();

        Testigos testigos =  new Testigos();

        testigos.setId(0);
        testigos.setDNI_CIF("Prueba1");
        testigos.setNombre("Prueba1");

        testigos = modelo.descifrarTestigo(modelo.EncriptarTestigos(testigos,rsa),rsa);
    }

    @org.junit.jupiter.api.Test
    void descifararNorario() throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException, NoSuchProviderException, InvalidKeySpecException {

        RSA rsa =  new RSA();
        rsa.genKeyPair(4096);
        Modelo modelo = new Modelo();

        Notario notario = new Notario();

        notario.setDNI_CIF("Prueba1");
        notario.setId(0);
        notario.setNombre("Prueba1");
        notario.setNumero_colegiado("Prueba1");
        notario.setPais("Prueba1");
        notario.setCiudad("Prueba1");
        notario.setCalle("Prueba1");

        notario = modelo.descifararNorario(modelo.EncriptarNotario(notario,rsa),rsa);

    }

    @org.junit.jupiter.api.Test
    void encriptarEmpresa() throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException, NoSuchProviderException, InvalidKeySpecException {
        RSA rsa =  new RSA();
        rsa.genKeyPair(4096);
        Modelo modelo = new Modelo();

        Empresa empresa = new Empresa();

        empresa.setCIF("Prueba1");
        empresa.setId(0);
        empresa.setTexto("Prueba1");
        empresa.setFecha("Prueba1");
        empresa.setPago_notario("Prueba1");
        empresa.setResumen_condiciones("Prueba1");
        empresa.setObservaciones("Prueba1");
        empresa.setNombre_empresa("Prueba1");
        empresa.setTipo_empresa("Prueba1");
        empresa.setCapital_aportado("Prueba1");
        empresa.setPais("Prueba1");
        empresa.setCiudad("Prueba1");
        empresa.setCalle("Prueba1");

        Notario notario = new Notario();

        notario.setDNI_CIF("Prueba1");
        notario.setId(0);
        notario.setNombre("Prueba1");
        notario.setNumero_colegiado("Prueba1");
        notario.setPais("Prueba1");
        notario.setCiudad("Prueba1");
        notario.setCalle("Prueba1");

        empresa.setNotario_id(notario);

        empresa = modelo.EncriptarEmpresa(empresa,rsa);

    }

    @org.junit.jupiter.api.Test
    void encriptarAportadores() throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException, NoSuchProviderException, InvalidKeySpecException {

        RSA rsa =  new RSA();
        rsa.genKeyPair(4096);
        Modelo modelo = new Modelo();

        Aportadores aportadores = new Aportadores();

        aportadores.setDNI_CIF("Prueba1");
        aportadores.setId(0);
        aportadores.setNombre("Prueba1");
        aportadores.setCantidad("Prueba1");
        aportadores.setMoneda("Prueba1");

        aportadores = modelo.EncriptarAportadores(aportadores,rsa, false);

    }

    @org.junit.jupiter.api.Test
    void encriptarPapeles() throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException, NoSuchProviderException, InvalidKeySpecException {

        RSA rsa =  new RSA();
        rsa.genKeyPair(4096);
        Modelo modelo = new Modelo();

        Papeles papeles =  new Papeles();

        papeles.setId(0);
        papeles.setNombre("Prueba1");
        papeles.setNumero_referencia("Prueba1");
        papeles.setDescripcion("Prueba1");
        papeles.setObservaciones("Prueba1");
        papeles.setRuta("Prueba1");
        papeles.setFecha_guardado("Prueba1");
        papeles.setFecha_papeles("Prueba1");

        papeles = modelo.EncriptarPapeles(papeles,rsa);

    }

    @org.junit.jupiter.api.Test
    void encriptarAccionistasParticipadores() throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException, NoSuchProviderException, InvalidKeySpecException {
        RSA rsa =  new RSA();
        rsa.genKeyPair(4096);
        Modelo modelo = new Modelo();

        AccionistasParticipadores accionistasParticipadores =  new AccionistasParticipadores();

        accionistasParticipadores.setDNI_CIF("Prueba1");
        accionistasParticipadores.setId(0);
        accionistasParticipadores.setNombre("Prueba1");
        accionistasParticipadores.setPorcetnaje("Prueba1");
        accionistasParticipadores.setTotales("Prueba1");
        accionistasParticipadores.setTipo_moneda("Prueba1");

        accionistasParticipadores = modelo.EncriptarAccionistasParticipadores(accionistasParticipadores,rsa);
    }

    @org.junit.jupiter.api.Test
    void encriptarTestigos() throws NoSuchPaddingException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, InvalidKeySpecException {
        RSA rsa =  new RSA();
        rsa.genKeyPair(4096);
        Modelo modelo = new Modelo();

        Testigos testigos =  new Testigos();

        testigos.setId(0);
        testigos.setDNI_CIF("Prueba1");
        testigos.setNombre("Prueba1");

        testigos = modelo.EncriptarTestigos(testigos,rsa);
    }

    @org.junit.jupiter.api.Test
    void encriptarNotario() throws NoSuchPaddingException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, InvalidKeySpecException {

        RSA rsa =  new RSA();
        rsa.genKeyPair(4096);
        Modelo modelo = new Modelo();

        Notario notario = new Notario();

        notario.setDNI_CIF("Prueba1");
        notario.setId(0);
        notario.setNombre("Prueba1");
        notario.setNumero_colegiado("Prueba1");
        notario.setPais("Prueba1");
        notario.setCiudad("Prueba1");
        notario.setCalle("Prueba1");

        notario = notario = modelo.EncriptarNotario(notario,rsa);
    }

    @org.junit.jupiter.api.Test
    void encriptarTrabajador() throws NoSuchPaddingException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, InvalidKeySpecException {

        RSA rsa =  new RSA();
        rsa.genKeyPair(4096);
        Modelo modelo = new Modelo();

        Trabajador trabajador = new Trabajador();

        trabajador.setId(1);
        trabajador.setDni_cif("Prueba1");
        trabajador.setDni_cif("Prueba1");
        trabajador.setNumero_ss("Prueba1");
        trabajador.setNombre("Prueba1");
        trabajador.setApellido1("Prueba1");
        trabajador.setApellido2("Prueba1");
        trabajador.setFecha_nacimiento("Prueba1");
        trabajador.setGenero("Prueba1");
        trabajador.setEstado("Prueba1");
        trabajador.setNacionalidad("Prueba1");
        trabajador.setHorario("Prueba1");
        trabajador.setTelefono("Prueba1");
        trabajador.setEmail("Prueba1");
        trabajador.setPais("Prueba1");
        trabajador.setCiudad("Prueba1");
        trabajador.setCalle("Prueba1");
        trabajador.setTipo_usuario("Prueba1");
        trabajador.setUser_pass(0);

        trabajador = modelo.EncriptarTrabajador(trabajador,rsa);

    }

    @org.junit.jupiter.api.Test
    void encriptarContratoTrabajador() throws NoSuchPaddingException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, InvalidKeySpecException {
        RSA rsa =  new RSA();
        rsa.genKeyPair(4096);
        Modelo modelo = new Modelo();

        ContratoTrabajador contratoTrabajador = new ContratoTrabajador();
        contratoTrabajador.setId(0);
        contratoTrabajador.setHorario("Prueba1");
        contratoTrabajador.setRango("Prueba1");
        contratoTrabajador.setFecha_inicio("Prueba1");
        contratoTrabajador.setFecha_finalizacion("Prueba1");
        contratoTrabajador.setPago_diario("Prueba1");
        contratoTrabajador.setFecha_despido("Prueba1");
        contratoTrabajador.setTipo_despido("Prueba1");
        contratoTrabajador.setPais("Prueba1");
        contratoTrabajador.setCiudad("Prueba1");
        contratoTrabajador.setCalle("Prueba1");

        contratoTrabajador = modelo.EncriptarContratoTrabajador(contratoTrabajador,rsa);

    }

}