package main;

import controlador.Controlador;
import modelo.Modelo;
import vista.VistaInicio;

import java.io.IOException;

/**
 * Clase Main. Arranca el programa
 */
public class Main {
    /**
     * Metodo main encargado de iniciar el programa
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {

        Controlador controldaor = new Controlador(new Modelo(), new VistaInicio());


    }
}
