package print;

import clasesAuxiliares.Empresa;
import clasesAuxiliares.Trabajador;
import com.itextpdf.text.*;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.*;
import controlador.Controlador;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

/**
 * Clase Print. Clase encargada de imprimir los informes
 *
 * @author Alberto Soria Carrillo
 */
public class Print {

    // Atributos
    private int admin = 0;
    private int gere = 0;
    private int trab = 0;


    /**
     * Crea un pdf con la informacion de los trabajadores
     *
     * @param trabajadors
     * @param logo
     * @throws IOException
     * @throws DocumentException
     */
    public void listaTrabajadores(ArrayList<Trabajador> trabajadors, File logo) throws IOException, DocumentException {

        File file = null;
        String ruta = System.getProperty("user.home");
        String rutaFinal = "";

        int count = 0;

        while (true) {
            rutaFinal = ruta + "/Desktop/Lista_Trabajadores_numero_" + count + "_PDF.pdf";

            file = new File(rutaFinal);

            if (!file.exists()) {
                break;
            }
            count++;

        }

        // objetos usados para crear el documento
        Document document = new Document();
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(rutaFinal));
        writer.setViewerPreferences(PdfWriter.PageModeUseThumbs);
        writer.setPageSize(PageSize.A4);
        Paragraph paragraphTiulo = new Paragraph();
        Paragraph paragraphTexto = new Paragraph();
        PdfPTable pdfPTable = new PdfPTable(4);

        // creacion de la imagen
        Image img1 = Image.getInstance(logo.getAbsolutePath());
        int medida = 50;
        img1.scaleToFit(medida, ((medida / img1.getWidth()) * img1.getHeight()));
        img1.setAlignment(Chunk.ALIGN_RIGHT);

        // creacion del titulo del documento
        paragraphTiulo.setAlignment(Chunk.ALIGN_CENTER);
        paragraphTiulo.setFont(FontFactory.getFont("Arial", 20, Font.UNDERLINE, BaseColor.GRAY));
        paragraphTiulo.add(new Chunk("Lista de trabajadores"));

        // Creacion del cuerpo del documento
        paragraphTexto.setAlignment(Chunk.ALIGN_LEFT);
        paragraphTexto.setFont(FontFactory.getFont("MV Boli", 10, Font.NORMAL, BaseColor.BLACK));
        paragraphTexto.add(new Chunk("\nInforme de los trabajadores a fecha " + LocalDate.now() + " " + LocalTime.now() + "\n"));

        for (int i = 0; i < trabajadors.size(); i++) {

            paragraphTexto.add(new Chunk("\n"));
            paragraphTexto.setFont(FontFactory.getFont("MV Boli", 12, Font.UNDERLINE, BaseColor.BLACK));
            paragraphTexto.add(new Chunk("Nombre del trabajador: " + trabajadors.get(i).getNombre() + " " + trabajadors.get(i).getApellido1()) + " " + trabajadors.get(i).getApellido2() + "\n");
            paragraphTexto.setFont(FontFactory.getFont("MV Boli", 10, Font.NORMAL, BaseColor.BLACK));

            if (trabajadors.get(i).getTipo_usuario().equalsIgnoreCase(Controlador.adnimnistrador)) {
                admin++;
            } else if (trabajadors.get(i).getTipo_usuario().equalsIgnoreCase(Controlador.encargado)) {
                gere++;
            } else {
                trab++;
            }

            paragraphTexto.add(new Chunk("Rango de Acceso: " + trabajadors.get(i).getTipo_usuario()) + "\n");
            paragraphTexto.add(new Chunk("Documento Nacional de Identidad: " + trabajadors.get(i).getDni_cif() + "\n"));
            paragraphTexto.add(new Chunk("Numero de la Seguridad Social: " + trabajadors.get(i).getNumero_ss() + "\n"));
            paragraphTexto.add(new Chunk("Fecha de Nacimiento: " + trabajadors.get(i).getFecha_nacimiento()) + "\n");
            String genero = "no definido";

            if (trabajadors.get(i).getGenero().equalsIgnoreCase("true")) {
                genero = "hombre";
            } else if (trabajadors.get(i).getGenero().equalsIgnoreCase("false")) {
                genero = "mujer";
            }
            paragraphTexto.add(new Chunk("Gereno: " + genero + "\n"));
            paragraphTexto.add(new Chunk("Estado: " + trabajadors.get(i).getEstado() + "\n"));
            paragraphTexto.add(new Chunk("Nacionalidad: " + trabajadors.get(i).getNacionalidad() + "\n"));
            paragraphTexto.add(new Chunk("Pais de Residencia: " + trabajadors.get(i).getPais() + "\n"));
            paragraphTexto.add(new Chunk("Ciudad de Residencia: " + trabajadors.get(i).getCiudad() + "\n"));
            paragraphTexto.add(new Chunk("Calle de Residencia: " + trabajadors.get(i).getCalle() + "\n"));

        }

        paragraphTexto.add(new Chunk("\n\n"));

        // creacion de la tabla para la informacion de la cantidad de usuarioas por tipo
        pdfPTable.setWidthPercentage(80);
        pdfPTable.getDefaultCell().setBorderColor(BaseColor.DARK_GRAY);

        pdfPTable.addCell(Controlador.adnimnistrador);
        pdfPTable.addCell(Controlador.encargado);
        pdfPTable.addCell(Controlador.trabajado);
        pdfPTable.addCell("Total");

        pdfPTable.addCell(String.valueOf(admin));
        pdfPTable.addCell(String.valueOf(gere));
        pdfPTable.addCell(String.valueOf(trab));
        pdfPTable.addCell(String.valueOf(admin + gere + trab));

        // grafico
        // Definiendo la fuente de datos
        DefaultPieDataset data = new DefaultPieDataset();
        data.setValue(Controlador.adnimnistrador, admin);
        data.setValue(Controlador.encargado, gere);
        data.setValue(Controlador.trabajado, trab);

        // Creando el gráfico
        JFreeChart chart = ChartFactory.createPieChart(
                "Grafico de trabajadores por Rangos", // Título del gráfico
                data, // DataSet
                true, // Leyenda
                true, // ToolTips
                true);

        BufferedImage bufferedImage = chart.createBufferedImage(500, 300);
        Image image = Image.getInstance(bufferedImage, null);

        // cierre y implementacion de los elementos en el documento
        document.getPageSize();
        document.open();
        document.add(img1);
        document.add(paragraphTiulo);
        document.add(paragraphTexto);
        document.newPage();
        document.add(pdfPTable);
        document.add(image);
        document.close();

    }

    /**
     * Crea un pdf con la inforacion de la empresa
     *
     * @param empresa
     * @param logo
     * @throws IOException
     * @throws DocumentException
     */
    public void datosEmpresa(Empresa empresa, File logo) throws IOException, DocumentException {

        File file = null;
        String ruta = System.getProperty("user.home");
        String rutaFinal = "";

        int count = 0;

        while (true) {
            rutaFinal = ruta + "/Desktop/Datos_de_la_Empresa_numero_" + count + "_PDF.pdf";

            file = new File(rutaFinal);

            if (!file.exists()) {
                break;
            }
            count++;

        }

        // objetos usados para crear el documento
        Document document = new Document(PageSize.A4.rotate());
        PdfWriter.getInstance(document, new FileOutputStream(rutaFinal));
        Paragraph paragraphTiulo1 = new Paragraph();
        Paragraph paragraphEmpresa = new Paragraph();
        Paragraph paragraphNotario = new Paragraph();
        Paragraph paragraphTestigos = new Paragraph();
        Paragraph paragraphAccionistasParticipadores = new Paragraph();
        Paragraph paragraphAportadores = new Paragraph();


        PdfPTable pdfPTableTestigos = new PdfPTable(2);
        PdfPTable pdfPTableAccionistasParticipadores = new PdfPTable(4);
        PdfPTable pdfPTableAportadores = new PdfPTable(3);

        // creacion de la imagen
        Image img1 = Image.getInstance(logo.getAbsolutePath());
        int medida = 50;
        img1.scaleToFit(medida, ((medida / img1.getWidth()) * img1.getHeight()));
        img1.setAlignment(Chunk.ALIGN_LEFT);

        // creacion del titulo del documento
        paragraphTiulo1.setAlignment(Chunk.ALIGN_CENTER);
        paragraphTiulo1.setFont(FontFactory.getFont("Arial", 20, Font.UNDERLINE, BaseColor.GRAY));
        paragraphTiulo1.add(new Chunk("Datos de la empresa"));

        // creacion del cuerpo texto con los datos de empresa
        paragraphEmpresa.add(new Chunk("\n"));
        paragraphEmpresa.setFont(FontFactory.getFont("MV Boli", 12, Font.UNDERLINE, BaseColor.BLACK));
        paragraphEmpresa.add(new Chunk("Datos de la empresa"));
        paragraphEmpresa.setFont(FontFactory.getFont("MV Boli", 10, Font.NORMAL, BaseColor.BLACK));
        paragraphEmpresa.add(new Chunk("\nNombre de la empresa: " + empresa.getNombre_empresa() + "\n"));
        paragraphEmpresa.add(new Chunk("numero CIF: " + empresa.getCIF() + "\n"));
        paragraphEmpresa.add(new Chunk("Fecha de la creacion: " + empresa.getFecha() + "\n"));
        paragraphEmpresa.add(new Chunk("Tipo de empresa: " + empresa.getTipo_empresa() + "\n"));
        paragraphEmpresa.add(new Chunk("Capital social aportador: " + empresa.getCapital_aportado() + "\n"));
        paragraphEmpresa.add(new Chunk("Pago realizado al notario: " + empresa.getPago_notario() + "\n"));
        paragraphEmpresa.add(new Chunk("Pais: " + empresa.getPais() + "\n"));
        paragraphEmpresa.add(new Chunk("Ciudad: " + empresa.getCiudad() + "\n"));
        paragraphEmpresa.add(new Chunk("Calle: " + empresa.getCalle() + "\n"));
        paragraphEmpresa.add(new Chunk("Simbolo de la empresa: " + logo.getName() + "\n"));


        // creacion del cuerpo texto del notario
        paragraphNotario.add(new Chunk("\n"));
        paragraphNotario.setFont(FontFactory.getFont("MV Boli", 12, Font.UNDERLINE, BaseColor.BLACK));
        paragraphNotario.add(new Chunk("Datos del Notario"));
        paragraphNotario.add(new Chunk("\n"));

        if (empresa.getNotario_id() != null) {
            paragraphNotario.setFont(FontFactory.getFont("MV Boli", 10, Font.NORMAL, BaseColor.BLACK));
            paragraphNotario.add(new Chunk("Nombre: " + empresa.getNotario_id().getNombre() + "\n"));
            paragraphNotario.add(new Chunk("Documento Nacional de Identidad: " + empresa.getNotario_id().getDNI_CIF() + "\n"));
            paragraphNotario.add(new Chunk("Numero de colegiado: " + empresa.getNotario_id().getNumero_colegiado() + "\n"));
            paragraphNotario.add(new Chunk("Pais: " + empresa.getNotario_id().getPais() + "\n"));
            paragraphNotario.add(new Chunk("Ciudad: " + empresa.getNotario_id().getCiudad() + "\n"));
            paragraphNotario.add(new Chunk("Calle: " + empresa.getNotario_id().getCalle() + "\n"));
        }

        // creacion de la tabla testigo
        paragraphTestigos.add(new Chunk("\n"));
        paragraphTestigos.setFont(FontFactory.getFont("MV Boli", 12, Font.UNDERLINE, BaseColor.BLACK));
        paragraphTestigos.add(new Chunk("Datos de los testigos"));
        paragraphTestigos.setFont(FontFactory.getFont("MV Boli", 10, Font.NORMAL, BaseColor.BLACK));
        paragraphTestigos.add(new Chunk("\n\n"));

        pdfPTableTestigos.addCell("Nombre");
        pdfPTableTestigos.addCell("Documento Nacional de Identidad");

        for (int i = 0; i < empresa.getTestigos().size(); i++) {
            pdfPTableTestigos.addCell(empresa.getTestigos().get(i).getNombre());
            pdfPTableTestigos.addCell(empresa.getTestigos().get(i).getDNI_CIF());
        }

        // creacion de la tabla de AccionistasParticipadores
        paragraphAccionistasParticipadores.add(new Chunk("\n"));
        paragraphAccionistasParticipadores.setFont(FontFactory.getFont("MV Boli", 12, Font.UNDERLINE, BaseColor.BLACK));
        paragraphAccionistasParticipadores.add(new Chunk("Datos de los Accionistas/Participadores"));
        paragraphAccionistasParticipadores.setFont(FontFactory.getFont("MV Boli", 10, Font.NORMAL, BaseColor.BLACK));
        paragraphAccionistasParticipadores.add(new Chunk("\n\n"));

        pdfPTableAccionistasParticipadores.addCell("Nombre");
        pdfPTableAccionistasParticipadores.addCell("Documento Nacional de identidad");
        pdfPTableAccionistasParticipadores.addCell("Porcentaje");
        pdfPTableAccionistasParticipadores.addCell("Total aportado");

        for (int i = 0; i < empresa.getAccionistas_participadores().size(); i++) {
            pdfPTableAccionistasParticipadores.addCell(empresa.getAccionistas_participadores().get(i).getNombre());
            pdfPTableAccionistasParticipadores.addCell(empresa.getAccionistas_participadores().get(i).getDNI_CIF());
            pdfPTableAccionistasParticipadores.addCell(empresa.getAccionistas_participadores().get(i).getPorcetnaje());
            pdfPTableAccionistasParticipadores.addCell(empresa.getAccionistas_participadores().get(i).getTotales() + " " + empresa.getAccionistas_participadores().get(i).getTipo_moneda());
        }

        // creacion de la tabla de Aportadores
        paragraphAportadores.add(new Chunk("\n"));
        paragraphAportadores.setFont(FontFactory.getFont("MV Boli", 12, Font.UNDERLINE, BaseColor.BLACK));
        paragraphAportadores.add(new Chunk("Datos de los Accionistas/Participadores"));
        paragraphAportadores.setFont(FontFactory.getFont("MV Boli", 10, Font.NORMAL, BaseColor.BLACK));
        paragraphAportadores.add(new Chunk("\n\n"));

        pdfPTableAportadores.addCell("Nombre");
        pdfPTableAportadores.addCell("Documento Nacional de Identidad");
        pdfPTableAportadores.addCell("Dinero aportado");

        for (int i = 0; i < empresa.getAportadors().size(); i++) {
            pdfPTableAportadores.addCell(empresa.getAportadors().get(i).getNombre());
            pdfPTableAportadores.addCell(empresa.getAportadors().get(i).getDNI_CIF());
            pdfPTableAportadores.addCell(empresa.getAportadors().get(i).getCantidad() + " " + empresa.getAportadors().get(i).getMoneda());
        }

        // cierre y implementacion de los elementos en el documento

        document.open();
        document.add(paragraphTiulo1);
        document.add(paragraphEmpresa);
        document.add(img1);
        document.add(paragraphNotario);
        document.newPage();
        document.add(paragraphTestigos);
        document.add(pdfPTableTestigos);
        document.add(paragraphAccionistasParticipadores);
        document.add(pdfPTableAccionistasParticipadores);
        document.add(paragraphAportadores);
        document.add(pdfPTableAportadores);
        document.close();

    }

}
