package controlador;

import clasesAuxiliares.*;
import com.google.gson.Gson;
import com.itextpdf.text.DocumentException;
import libreriaAlberto.Crypto;
import modelo.Modelo;
import print.Print;
import rsa.RSA;
import vista.*;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.Socket;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;

/**
 * Clase Controlador. Se encarga de controlar la ejecucion del programa y de la imvocacion de metodos de la clase modelo
 *
 * @author Alberto Soria Carrillo
 */
public class Controlador implements ActionListener, TableModelListener, ItemListener, WindowListener, MouseListener {

    // Atributo
    private Modelo modelo;
    private VistaInicio vistaInicio;
    private RSA rsaCliente;
    private RSA rsaServer;
    private Socket socket;
    private DataInputStream entrada;
    private DataOutputStream salida;
    private Gson gson;
    private Crypto crypto;
    private Trabajador trabajador;
    private Empresa empresa;
    private VistaAdmin vistaAdmin;
    private int opc; // variable para guardar valores temporales
    private int fila = -1; // fila de la tabla
    private int column = 0; // columna de la tabla
    private int idAuxTrabajadorSeleccionado = 0; // guarda el trabajador seleccionado para trabajar con los contratos
    private int idAuxEliminarEntradaSalida = 0; // guarda el trabajador seleccionado para trabajar con las entradas y salidas
    private ArrayList<Trabajador> trabajadors;
    private VistaAuxiliar vistaAuxiliar;
    private VistaEncargado vistaEncargado;
    private VistaTrabajador vistaTrabajador;
    private ArrayList<Entradas> entradas;
    private ArrayList<Salidas> salidas;
    public static File logo = new File("img/logo.png"); // logo de la empresa
    private Print print = new Print(); // clase que se ocupa de la impresion de los pdf
    private Boolean bNewLogo = false;

    public static final String adnimnistrador = "Administrador";
    public static final String encargado = "Encargado";
    public static final String trabajado = "Trabajador";
    public final static String passAes = "Montessori2020-2021";

    // Constructor
    public Controlador(Modelo modelo, VistaInicio vistaInicio) throws IOException {

        this.modelo = modelo;
        this.vistaInicio = vistaInicio;
        modelo.windowsLstener(vistaInicio, this);
        modelo.setActionListener(vistaInicio, this);

        gson = new Gson();
        crypto = new Crypto();
        rsaServer = new RSA();

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            switch (e.getActionCommand()) {
                case "VistaInicioLogin":
                    // Genera una cifrado RSA unico para la conexion
                    rsaCliente = new RSA();
                    rsaCliente.genKeyPair(4096);
                    // Crea una conexion con el servidor
                    socket = new Socket("localhost", 4444);
                    // Crea los objetos para la transmision de información
                    entrada = new DataInputStream(socket.getInputStream());
                    salida = new DataOutputStream(socket.getOutputStream());
                    // envio de la pass publica
                    salida.writeUTF(crypto.encriptar(rsaCliente.getPublicKeyString(), passAes));
                    // recepcionde la pass publica del servidor
                    rsaServer.setPublicKeyString(crypto.desencriptar(entrada.readUTF(), passAes));
                    // envio de los datos de usuario y contraseña
                    NamePassUser namePassUser = new NamePassUser();
                    namePassUser.setNombre(rsaServer.Encrypt(vistaInicio.textField1.getText()));
                    namePassUser.setPass(rsaServer.Encrypt(vistaInicio.passwordField1.getText()));
                    salida.writeUTF(crypto.encriptar(gson.toJson(namePassUser), passAes));
                    // recepcion del trabajador y lo descifra
                    trabajador = modelo.descrifrarTrabajador(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Trabajador.class), rsaCliente, gson, crypto, entrada, passAes, true);
                    // comprueba que el trabajador que ha recibido no es una señal de no existe
                    if (trabajador != null) {
                        // comprueba el tipo de usuario
                        if (trabajador.getTipo_usuario().equalsIgnoreCase(adnimnistrador)) {
                            empresa = modelo.descifrarEmpresa(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Empresa.class), rsaCliente, gson, crypto, entrada, passAes, true);
                            trabajadors = modelo.recibirTrabajador(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Integer.class), rsaCliente, gson, crypto, entrada, passAes);
                            vistaAdmin = new VistaAdmin();
                            vistaAdmin.addWindowListener(this);
                            modelo.rellenarVistaAdmin(vistaAdmin, trabajador, empresa, trabajadors, salida, gson, crypto, passAes);
                            modelo.windowsLstener(vistaAdmin, this);
                            modelo.setActionListener(vistaAdmin, this);
                            modelo.addTableListener(vistaAdmin, this);
                            modelo.addMouseListener(vistaAdmin, this);
                            modelo.addItemSelection(vistaAdmin, this);
                            modelo.rellenarIconoVentana(vistaAdmin, empresa, salida, crypto, gson, passAes, logo,socket);

                            vistaInicio.textField1.setText("");
                            vistaInicio.passwordField1.setText("");
                            vistaInicio.dispose();
                        } else if (trabajador.getTipo_usuario().equalsIgnoreCase(encargado)) {
                            trabajadors = modelo.recibirTrabajador(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Integer.class), rsaCliente, gson, crypto, entrada, passAes);
                            vistaEncargado = new VistaEncargado();
                            modelo.windowsLstener(vistaEncargado, this);
                            modelo.setActionListener(vistaEncargado, this);
                            modelo.addTableListener(vistaEncargado, this);
                            modelo.rellenarVistaEncargado(vistaEncargado, trabajadors);
                            modelo.setMouseListener(vistaEncargado, this);

                            if (logo.exists()) {
                                vistaEncargado.setIconImage(new ImageIcon(logo.getPath()).getImage());
                            } else {
                                logo = new File("img/delete.png");
                                vistaEncargado.setIconImage(new ImageIcon(logo.getPath()).getImage());
                            }

                            vistaInicio.textField1.setText("");
                            vistaInicio.passwordField1.setText("");
                            vistaInicio.dispose();
                        } else if (trabajador.getTipo_usuario().equalsIgnoreCase(trabajado)) {
                            vistaTrabajador = new VistaTrabajador();
                            modelo.windowsLstener(vistaTrabajador,this);
                            modelo.rellenarVistaTrabajador(vistaTrabajador, trabajador);
                            modelo.setActionListener(vistaTrabajador, this);

                            if (logo.exists()) {
                                vistaTrabajador.setIconImage(new ImageIcon(logo.getPath()).getImage());
                            } else {
                                logo = new File("img/delete.png");
                                vistaTrabajador.setIconImage(new ImageIcon(logo.getPath()).getImage());
                            }

                            vistaInicio.textField1.setText("");
                            vistaInicio.passwordField1.setText("");
                            vistaInicio.dispose();
                        }
                    }

                    break;
                case "VistaAdminEditar":
                    if (vistaAdmin.editarButton.getText().equalsIgnoreCase("Editar")) {
                        modelo.editarVistaAdminPanel1(vistaAdmin, true);
                        vistaAdmin.cancelarButton.setVisible(true);
                        vistaAdmin.logoButton.setVisible(true);
                        vistaAdmin.editarButton.setText("Guardar");
                    } else {
                        modelo.editarVistaAdminPanel1(vistaAdmin, false);
                        vistaAdmin.cancelarButton.setVisible(false);
                        vistaAdmin.logoButton.setVisible(false);
                        vistaAdmin.editarButton.setText("Editar");
                        modelo.guardarDatosVistaAdminP1(vistaAdmin, trabajador, empresa);

                        Trabajador rsaServerTrabajador = modelo.EncriptarTrabajador(trabajador, rsaServer);
                        Empresa rsaServerEmpresa = modelo.EncriptarEmpresa(empresa, rsaServer);
                        opc = 1;
                        envioOpc();
                        salida.writeUTF(crypto.encriptar(gson.toJson(rsaServerTrabajador), passAes));
                        opc = 2;
                        envioOpc();
                        salida.writeUTF(crypto.encriptar(gson.toJson(rsaServerEmpresa), passAes));

                        if (bNewLogo) {

                            opc = 24;
                            envioOpc();
                            modelo.enviarImagen(logo, passAes, socket);
                            modelo.guardarLogo(logo);
                            modelo.enviarIdPapelesLogo(empresa.getPapeles(), salida, rsaServer, crypto, passAes, gson);
                            vistaAdmin.setIconImage(new ImageIcon(logo.getPath()).getImage());

                        }
                    }
                    break;
                case "VistaADminCancelar":
                    modelo.rellenarVistaAdmin(vistaAdmin, trabajador, empresa, trabajadors, salida, gson, crypto, passAes);
                    modelo.editarVistaAdminPanel1(vistaAdmin, false);
                    vistaAdmin.editarButton.setText("Editar");
                    vistaAdmin.cancelarButton.setVisible(false);
                    vistaAdmin.logoButton.setVisible(false);
                    break;
                case "VistaAdminGuardar":
                    if (fila != -1 && column != -1) {
                        System.out.println("fila " + fila);
                        System.out.println("columna " + column);
                        ContratoTrabajador rsaServerContratoTrabajador = modelo.EncriptarContratoTrabajador(modelo.guardarDatosContratoTrabajador(modelo.selecionarContrato(trabajador.getContrato_trabajador(), vistaAdmin, fila, column), vistaAdmin), rsaServer);
                        opc = 3;
                        envioOpc();
                        salida.writeUTF(crypto.encriptar(gson.toJson(rsaServerContratoTrabajador), passAes));
                        modelo.rellenarTablaContrato(vistaAdmin.dtmContratoAdmin, trabajador.getContrato_trabajador());
                    }
                    break;
                case "VistaAdminNuevo":
                    ContratoTrabajador rsaServerContratoTrabajador = modelo.EncriptarContratoTrabajador(modelo.guardarDatosContratoTrabajador(new ContratoTrabajador(), vistaAdmin), rsaServer);
                    opc = 4;
                    envioOpc();
                    salida.writeUTF(crypto.encriptar(gson.toJson(rsaServerContratoTrabajador), passAes));
                    trabajador.getContrato_trabajador().add(modelo.descifrarContratoTrabajador(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), ContratoTrabajador.class), rsaCliente, gson, crypto, entrada, passAes, true));
                    modelo.rellenarTablaContrato(vistaAdmin.dtmContratoAdmin, trabajador.getContrato_trabajador());

                    break;
                case "VistaAdminGuardarNotario":
                    Notario rsaServerNotario = modelo.EncriptarNotario(modelo.guardarDatosNotario(empresa.getNotario_id(), vistaAdmin), rsaServer);
                    opc = 5;
                    envioOpc();
                    salida.writeUTF(crypto.encriptar(gson.toJson(rsaServerNotario), passAes));
                    if (empresa.getNotario_id().getId() == 0) {
                        opc = empresa.getId();
                        envioOpc(); // aqui se usa para enviar el id de la empresa, para emparejar el notario
                        empresa.getNotario_id().setId(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Integer.class));
                    }
                    break;
                case "VistaAdminNuevoAccionista":
                    opc = 6;
                    envioOpc();
                    empresa.getAccionistas_participadores().add(modelo.descifrarAccionistasParticipadores(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), AccionistasParticipadores.class), rsaCliente));
                    modelo.rellenarTablaAccinistas(vistaAdmin.dtmAccinistas, empresa.getAccionistas_participadores());
                    break;
                case "VistaAdminNuevoAportador":
                    opc = 7;
                    envioOpc();
                    empresa.getAportadors().add(modelo.descifrarAportadores(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Aportadores.class), rsaCliente));
                    modelo.rellenarTablaAportadores(vistaAdmin.dtmAportadores, empresa.getAportadors());
                    break;
                case "VistaAdminNuevoTestigo":
                    opc = 8;
                    envioOpc();
                    empresa.getTestigos().add(modelo.descifrarTestigo(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Testigos.class), rsaCliente));
                    modelo.rellenarTablaTestigos(vistaAdmin.dtmTestigos, empresa.getTestigos());
                    break;
                case "VistaAdminEliminarAccionisa":
                    opc = 9;
                    envioOpc();
                    opc = Integer.valueOf((String) vistaAdmin.accionistasParticipadoresTable.getValueAt(vistaAdmin.accionistasParticipadoresTable.getSelectedRows()[0], 0));
                    envioOpc();

                    for (int i = 0; i < empresa.getAccionistas_participadores().size(); i++) {
                        if (empresa.getAccionistas_participadores().get(i).getId() == opc) {
                            empresa.getAccionistas_participadores().remove(i);
                            break;
                        }
                    }
                    modelo.rellenarTablaAccinistas(vistaAdmin.dtmAccinistas, empresa.getAccionistas_participadores());
                    break;
                case "VistaAdminEliminarAportadores":
                    opc = 10;
                    envioOpc();
                    opc = Integer.valueOf((String) vistaAdmin.aportadoresTable.getValueAt(vistaAdmin.aportadoresTable.getSelectedRows()[0], 0));
                    envioOpc();

                    for (int i = 0; i < empresa.getAportadors().size(); i++) {
                        if (empresa.getAportadors().get(i).getId() == opc) {
                            empresa.getAportadors().remove(i);
                            break;
                        }
                    }
                    modelo.rellenarTablaAportadores(vistaAdmin.dtmAportadores, empresa.getAportadors());
                    break;
                case "VistaAdminElminarTestigos":
                    opc = 11;
                    envioOpc();
                    opc = Integer.valueOf((String) vistaAdmin.testigosTable.getValueAt(vistaAdmin.testigosTable.getSelectedRows()[0], 0));
                    envioOpc();

                    for (int i = 0; i < empresa.getTestigos().size(); i++) {
                        if (empresa.getTestigos().get(i).getId() == opc) {
                            empresa.getTestigos().remove(i);
                            break;
                        }
                    }
                    modelo.rellenarTablaTestigos(vistaAdmin.dtmTestigos, empresa.getTestigos());
                    break;
                case "VistaAdminNuevoTrabajador":
                    String name = JOptionPane.showInputDialog("Ingrese el nombre del usuario: ");
                    String pass = JOptionPane.showInputDialog("Ingrese la contraseña del usuario: ");
                    if (name != null && pass != null) {
                        if (!name.equalsIgnoreCase("") && !pass.equalsIgnoreCase("")) {
                            opc = 15;
                            envioOpc(); // envia la opcion
                            opc = empresa.getId();
                            envioOpc(); // envia el id de la empresa para enlazar el usuario

                            // envia el nombre de usuario y la contraseña
                            NamePassUser rsaServerNamePassUser = new NamePassUser();
                            rsaServerNamePassUser.setNombre(rsaServer.Encrypt(name));
                            rsaServerNamePassUser.setPass(rsaServer.Encrypt(pass));
                            salida.writeUTF(crypto.encriptar(gson.toJson(rsaServerNamePassUser), passAes));
                            // recibe el trabajador y lo anexa al array
                            Trabajador aux = modelo.descrifrarTrabajador(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Trabajador.class), rsaCliente, gson, crypto, entrada, passAes, true);
                            trabajadors.add(aux);

                            modelo.rellenarVistaAdmin(vistaAdmin, trabajador, empresa, trabajadors, salida, gson, crypto, passAes);
                        } else {
                            JOptionPane.showMessageDialog(null, "Los datos no pueden estar vacios");
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "Los datos no pueden estar vacios");
                    }
                    break;
                case "VistaAdminNuevoContratoTrabajador":
                    opc = 16;
                    envioOpc();
                    // Envio del id del trabajador
                    opc = idAuxTrabajadorSeleccionado;
                    int idTrabajador = opc;
                    envioOpc();
                    // envio del id de la empresa
                    opc = empresa.getId();
                    envioOpc();
                    // recepcion del contrato
                    ContratoTrabajador aux = modelo.descifrarContratoTrabajador(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), ContratoTrabajador.class), rsaCliente, gson, crypto, entrada, passAes,true);

                    // agrega el contrato al trabajador
                    for (int i = 0; i < trabajadors.size(); i++) {
                        if (trabajadors.get(i).getId() == idTrabajador) {
                            trabajadors.get(i).getContrato_trabajador().add(aux);
                            vistaAdmin.dtmContratoTrabajadores.setNumRows(0);

                            if (trabajador.getId() == idTrabajador) {
                                trabajador.getContrato_trabajador().add(aux);
                                vistaAdmin.dtmContratoAdmin.setNumRows(0);
                            }

                            modelo.rellenarVistaAdmin(vistaAdmin, trabajador, empresa, trabajadors, salida, gson, crypto, passAes);
                            modelo.rellenarTablaContratoTrabajador(trabajadors.get(i), vistaAdmin.dtmContratoTrabajadores, vistaAdmin.contratosTrabajadorTable);
                        }
                    }

                    break;
                case "VistaAdminVerEntradas":

                    opc = 17;
                    envioOpc();
                    opc = idAuxTrabajadorSeleccionado;
                    envioOpc();

                    if (entradas == null) {
                        entradas = new ArrayList<>();
                    } else {
                        entradas.clear();
                    }

                    int countEntradas = gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Integer.class);
                    for (int i = 0; i < countEntradas; i++) {
                        entradas.add(modelo.descrifrarEntradas(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Entradas.class), rsaCliente));
                    }

                    if (vistaAuxiliar == null) {
                        vistaAuxiliar = new VistaAuxiliar("Entradas");
                    } else if (!vistaAuxiliar.isVisible()) {
                        vistaAuxiliar.setVisible(true);
                    }

                    vistaAuxiliar.opc = "Entradas";
                    modelo.setActionListener(vistaAuxiliar, this);
                    modelo.addTableListener(vistaAuxiliar, this);
                    modelo.rellenarTablaAuxiliar(vistaAuxiliar, entradas);
                    vistaAuxiliar.tituloLabel.setText(vistaAuxiliar.opc);
                    vistaAuxiliar.setIconImage(vistaAdmin.getIconImage());

                    break;
                case "VistaAdminVerSalidas":
                    opc = 18;
                    envioOpc();
                    opc = idAuxTrabajadorSeleccionado;
                    envioOpc();

                    if (salidas == null) {
                        salidas = new ArrayList<>();
                    } else {
                        salidas.clear();
                    }

                    int countSalidas = gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Integer.class);
                    for (int i = 0; i < countSalidas; i++) {
                        salidas.add(modelo.descrifrarSalidas(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Salidas.class), rsaCliente));
                    }

                    if (vistaAuxiliar == null) {
                        vistaAuxiliar = new VistaAuxiliar("Salidas");
                    } else if (!vistaAuxiliar.isVisible()) {
                        vistaAuxiliar.setVisible(true);
                    }

                    vistaAuxiliar.opc = "Salidas";
                    modelo.setActionListener(vistaAuxiliar, this);
                    modelo.addTableListener(vistaAuxiliar, this);
                    modelo.rellenarTablaAuxiliar(vistaAuxiliar, salidas, true);
                    vistaAuxiliar.tituloLabel.setText(vistaAuxiliar.opc);
                    vistaAuxiliar.setIconImage(vistaAdmin.getIconImage());

                    break;
                case "VistaAuxiliarEliminar":

                    this.idAuxEliminarEntradaSalida = Integer.valueOf(String.valueOf(vistaAuxiliar.dtm.getValueAt(vistaAuxiliar.auxTable.getSelectedRows()[0], 0)));

                    if (this.idAuxEliminarEntradaSalida >= 0) {
                        opc = 19;
                        envioOpc();
                        System.out.println("envia la opcion " + opc);
                        salida.writeUTF(crypto.encriptar(gson.toJson(vistaAuxiliar.opc), passAes));
                        System.out.println("envia la tabla " + vistaAuxiliar.opc);
                        opc = this.idAuxEliminarEntradaSalida;
                        envioOpc();
                        System.out.println("envia el id " + idAuxEliminarEntradaSalida);

                        if (vistaAuxiliar.opc.equalsIgnoreCase("Entradas")) {
                            System.out.println("entra en entrada");
                            for (int i = 0; i < trabajadors.size(); i++) {
                                for (int o = 0; o < trabajadors.get(i).getEntrada().size(); o++) {
                                    if (trabajadors.get(i).getEntrada().get(o).getId() == this.idAuxEliminarEntradaSalida) {
                                        trabajadors.get(i).getEntrada().remove(o);
                                        modelo.rellenarTablaAuxiliar(vistaAuxiliar, trabajadors.get(i).getEntrada());
                                        break;
                                    }
                                }
                            }
                        } else {
                            System.out.println("entra en salida");
                            for (int i = 0; i < trabajadors.size(); i++) {
                                for (int o = 0; o < trabajadors.get(i).getSalida().size(); o++) {
                                    if (trabajadors.get(i).getSalida().get(o).getId() == this.idAuxEliminarEntradaSalida) {
                                        trabajadors.get(i).getSalida().remove(o);
                                        modelo.rellenarTablaAuxiliar(vistaAuxiliar, trabajadors.get(i).getSalida(), true);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    break;
                case "VistaAdminEliminarTrabajador":
                    this.idAuxTrabajadorSeleccionado = Integer.valueOf((String) vistaAdmin.trabajadoresTable.getValueAt(vistaAdmin.trabajadoresTable.getSelectedRows()[0], 0));
                    if (idAuxTrabajadorSeleccionado != trabajador.getId()) {
                        String motivo = JOptionPane.showInputDialog("Motivo del despido del trabajador: ");
                        if (!motivo.equalsIgnoreCase("")) {
                            opc = 20;
                            envioOpc();
                            opc = this.idAuxTrabajadorSeleccionado;
                            envioOpc();
                            salida.writeUTF(crypto.encriptar(gson.toJson(rsaServer.Encrypt(motivo)), passAes));
                            modelo.eliminarTrabajador(trabajadors, this.idAuxTrabajadorSeleccionado);
                        } else {
                            JOptionPane.showMessageDialog(null, "No es legal despedir a la gente sin motivo.");
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "El trabajador que se desea eleminiar es el mismo que dispone de esta sesion.");
                    }
                    modelo.rellenarVistaAdmin(vistaAdmin, trabajador, empresa, trabajadors, salida, gson, crypto, passAes);
                    vistaAdmin.dtmContratoTrabajadores.setRowCount(0);
                    break;
                case "VistaAdminEliminarContratoTrabajador":
                    int idContrato = Integer.valueOf((String) vistaAdmin.contratosTrabajadorTable.getValueAt(vistaAdmin.contratosTrabajadorTable.getSelectedRows()[0], 0));
                    for (int i = 0; i < trabajadors.size(); i++) {
                        for (int o = 0; o < trabajadors.get(i).getContrato_trabajador().size(); o++) {
                            if (trabajadors.get(i).getContrato_trabajador().get(o).getId() == idContrato) {
                                if (trabajadors.get(i).getContrato_trabajador().size() > 1) {
                                    trabajadors.get(i).getContrato_trabajador().remove(o);
                                    opc = 21;
                                    envioOpc();
                                    opc = idContrato;
                                    envioOpc();
                                    modelo.rellenarVistaAdmin(vistaAdmin, trabajador, empresa, trabajadors, salida, gson, crypto, passAes);
                                    modelo.rellenarTablaContratoTrabajador(trabajadors.get(i), vistaAdmin.dtmContratoTrabajadores, vistaAdmin.contratosTrabajadorTable);
                                    break;
                                } else {
                                    if (JOptionPane.showConfirmDialog(null, "Si eliminas este contrato el trabajador no tendra contrato y sera eliminado.\n ¿Quiere eliminar el contrato y el trabajador?") == JOptionPane.YES_OPTION) {
                                        String motivo = JOptionPane.showInputDialog("Motivo del despido del trabajador: ");
                                        if (!motivo.equalsIgnoreCase("")) {
                                            trabajadors.remove(i);
                                            opc = 20;
                                            envioOpc();
                                            opc = this.idAuxTrabajadorSeleccionado;
                                            envioOpc();
                                            salida.writeUTF(crypto.encriptar(gson.toJson(rsaServer.Encrypt(motivo)), passAes));
                                            modelo.eliminarTrabajador(trabajadors, this.idAuxTrabajadorSeleccionado);
                                            modelo.rellenarVistaAdmin(vistaAdmin, trabajador, empresa, trabajadors, salida, gson, crypto, passAes);
                                            vistaAdmin.dtmContratoTrabajadores.setRowCount(0);
                                        } else {
                                            JOptionPane.showMessageDialog(null, "No es legal despedir a la gente sin motivo.");
                                        }
                                        break;
                                    } else {
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    break;
                case "VistaAdminRegistrarEntrada":
                    opc = 22;
                    envioOpc();
                    opc = trabajador.getId();
                    envioOpc();

                    Entradas entradas = modelo.descrifrarEntradas(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Entradas.class), rsaCliente);

                    trabajador.getEntrada().add(entradas);
                    if (vistaEncargado != null || vistaAdmin != null) {
                        for (int i = 0; i < trabajadors.size(); i++) {
                            if (trabajadors.get(i).getId() == trabajador.getId()) {
                                trabajadors.get(i).getEntrada().add(entradas);
                            }
                        }
                    }

                    JOptionPane.showMessageDialog(null, "Entrada registrada");

                    if (vistaTrabajador != null && vistaTrabajador.isVisible()){
                        envioCierreSesion();
                        Image imageIcon = vistaTrabajador.getIconImage();
                        vistaInicio.setVisible(true);
                        vistaInicio.setIconImage(imageIcon);
                        vistaTrabajador.dispose();
                    }

                    break;
                case "VistaAdminRegistrarSalida":

                    opc = 23;
                    envioOpc();
                    opc = trabajador.getId();
                    envioOpc();
                    Salidas salidas1 = modelo.descrifrarSalidas(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Salidas.class), rsaCliente);

                    trabajador.getSalida().add(salidas1);

                    if (vistaEncargado != null || vistaAdmin != null) {
                        for (int i = 0; i < trabajadors.size(); i++) {
                            if (trabajadors.get(i).getId() == trabajador.getId()) {
                                trabajadors.get(i).getSalida().add(salidas1);
                            }
                        }
                    }

                    JOptionPane.showMessageDialog(null, "Salida registrada");

                    if (vistaTrabajador != null && vistaTrabajador.isVisible()){
                        envioCierreSesion();
                        Image imageIcon = vistaTrabajador.getIconImage();
                        vistaInicio.setVisible(true);
                        vistaInicio.setIconImage(imageIcon);
                        vistaTrabajador.dispose();
                    }

                    break;
                case "VistaAdminLogo":

                    JFileChooser selectorArchivos = new JFileChooser();
                    selectorArchivos.setFileSelectionMode(JFileChooser.FILES_ONLY);
                    FileNameExtensionFilter filtro = new FileNameExtensionFilter("Imagenes", "png");
                    selectorArchivos.setFileFilter(filtro);
                    if (selectorArchivos.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                        logo = selectorArchivos.getSelectedFile();
                        vistaAdmin.logoTipoTextField.setText(logo.getPath());
                        bNewLogo = true;
                    }

                    break;
                case "VistaAdminImpimirTrabajadores":
                    print.listaTrabajadores(trabajadors, logo);
                    break;
                case "VistaAdminImprimirEmpresa":
                    print.datosEmpresa(empresa, logo);
                    break;

            }
        } catch (NoSuchPaddingException ex) {
            ex.printStackTrace();
        } catch (UnsupportedEncodingException ex) {
            ex.printStackTrace();
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        } catch (IllegalBlockSizeException ex) {
            ex.printStackTrace();
        } catch (BadPaddingException ex) {
            ex.printStackTrace();
        } catch (NoSuchProviderException ex) {
            ex.printStackTrace();
        } catch (InvalidKeyException ex) {
            ex.printStackTrace();
        } catch (InvalidKeySpecException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (DocumentException ex) {
            ex.printStackTrace();
        }

    }

    /**
     * Envias la opcion elegida al servidor
     *
     * @throws NoSuchPaddingException
     * @throws IOException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     */
    private void envioOpc() throws NoSuchPaddingException, IOException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, InvalidKeyException {
        salida.writeUTF(crypto.encriptar(gson.toJson(opc), passAes));
    }

    @Override
    public void itemStateChanged(ItemEvent e) {

    }

    @Override
    public void tableChanged(TableModelEvent e) {
        try {

            if (e.getType() == TableModelEvent.UPDATE) {
                int fila = e.getFirstRow();
                if (vistaAdmin != null && vistaAdmin.isVisible()) {
                    if (e.getSource() == vistaAdmin.dtmAportadores) {
                        opc = 12;
                        envioOpc();
                        Aportadores rsaServerAportadores = modelo.EncriptarAportadores(modelo.guardarDatosAportadores(modelo.selecionarAportador(empresa.getAportadors(), Integer.parseInt((String) vistaAdmin.dtmAportadores.getValueAt(fila, 0))), vistaAdmin.dtmAportadores, fila), rsaServer, true);
                        salida.writeUTF(crypto.encriptar(gson.toJson(rsaServerAportadores), passAes));
                        modelo.rellenarTablaAportadores(vistaAdmin.dtmAportadores, empresa.getAportadors());
                    } else if (e.getSource() == vistaAdmin.dtmTestigos) {
                        opc = 13;
                        envioOpc();
                        Testigos rsaServerTestigos = modelo.EncriptarTestigos(modelo.guardarDatosTestigos(modelo.selecionarTestigo(empresa.getTestigos(), Integer.parseInt((String) vistaAdmin.dtmTestigos.getValueAt(fila, 0))), vistaAdmin.dtmTestigos, fila), rsaServer);
                        salida.writeUTF(crypto.encriptar(gson.toJson(rsaServerTestigos), passAes));
                        modelo.rellenarTablaTestigos(vistaAdmin.dtmTestigos, empresa.getTestigos());
                    } else if (e.getSource() == vistaAdmin.dtmAccinistas) {
                        opc = 14;
                        envioOpc();
                        AccionistasParticipadores rsaServerAccionistasParticipadores = modelo.EncriptarAccionistasParticipadores(modelo.guardarAccionistasParticipadores(modelo.selecionarAccionistasParticipadores(empresa.getAccionistas_participadores(), Integer.parseInt((String) vistaAdmin.dtmAccinistas.getValueAt(fila, 0))), vistaAdmin.dtmAccinistas, fila), rsaServer);
                        salida.writeUTF(crypto.encriptar(gson.toJson(rsaServerAccionistasParticipadores), passAes));
                        modelo.rellenarTablaAccinistas(vistaAdmin.dtmAccinistas, empresa.getAccionistas_participadores());
                    } else if (e.getSource() == vistaAdmin.dtmTrabajadores) {
                        opc = 1;
                        envioOpc();
                        Trabajador rsaSeverTrabajador = modelo.EncriptarTrabajador(modelo.guardarDatosTrabajador(modelo.selecionarTrabajador(trabajadors, Integer.parseInt((String) vistaAdmin.dtmTrabajadores.getValueAt(fila, 0))), vistaAdmin.dtmTrabajadores, fila, trabajador), rsaServer);
                        salida.writeUTF(crypto.encriptar(gson.toJson(rsaSeverTrabajador), passAes));
                        modelo.rellenarVistaAdmin(vistaAdmin, trabajador, empresa, trabajadors, salida, gson, crypto, passAes);
                    } else if (e.getSource() == vistaAdmin.dtmContratoTrabajadores) {
                        opc = 3;
                        envioOpc();
                        int idContratrato = Integer.parseInt((String) vistaAdmin.dtmContratoTrabajadores.getValueAt(fila, 0));
                        ContratoTrabajador rsaServerContratoTrabajador = modelo.EncriptarContratoTrabajador(modelo.guardarDatosContratoTrabajador(modelo.selecionarContratoTrabajo(trabajadors, idContratrato), vistaAdmin.dtmContratoTrabajadores, fila, trabajador), rsaServer);
                        salida.writeUTF(crypto.encriptar(gson.toJson(rsaServerContratoTrabajador), passAes));
                        modelo.rellenarVistaAdmin(vistaAdmin, trabajador, empresa, trabajadors, salida, gson, crypto, passAes);
                    }

                }

                if (vistaEncargado != null && vistaEncargado.isVisible()) {
                    if (e.getSource() == vistaEncargado.dtmTrabajador) {
                        opc = 1;
                        envioOpc();
                        Trabajador rsaSeverTrabajador = modelo.EncriptarTrabajador(modelo.guardarDatosTrabajador(modelo.selecionarTrabajador(trabajadors, Integer.parseInt((String) vistaEncargado.dtmTrabajador.getValueAt(fila, 0))), vistaEncargado.dtmTrabajador, fila, trabajador), rsaServer);
                        salida.writeUTF(crypto.encriptar(gson.toJson(rsaSeverTrabajador), passAes));
                        modelo.rellenarVistaEncargado(vistaEncargado, trabajadors);
                    } else if (e.getSource() == vistaEncargado.dtmContratoTrabajadoresTrabajador) {
                        opc = 3;
                        envioOpc();
                        int idContratrato = Integer.parseInt((String) vistaEncargado.dtmContratoTrabajadoresTrabajador.getValueAt(fila, 0));
                        ContratoTrabajador rsaServerContratoTrabajador = modelo.EncriptarContratoTrabajador(modelo.guardarDatosContratoTrabajador(modelo.selecionarContratoTrabajo(trabajadors, idContratrato), vistaEncargado.dtmContratoTrabajadoresTrabajador, fila, trabajador), rsaServer);
                        salida.writeUTF(crypto.encriptar(gson.toJson(rsaServerContratoTrabajador), passAes));
                        modelo.rellenarVistaEncargado(vistaEncargado, trabajadors);
                    }
                }
            }
        } catch (NoSuchPaddingException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (IllegalBlockSizeException ex) {
            ex.printStackTrace();
        } catch (BadPaddingException ex) {
            ex.printStackTrace();
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        } catch (InvalidKeyException ex) {
            ex.printStackTrace();
        } catch (InvalidKeySpecException ex) {
            ex.printStackTrace();
        } catch (NoSuchProviderException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {
        if (e.getWindow() == vistaInicio) {
            System.out.println("Programa cerrado 1");
        } else if (e.getWindow() == vistaAdmin) {
            envioCierreSesion();

            if (vistaAuxiliar != null) {
                vistaAuxiliar.dispose();
            }
            Image imageIcon = vistaAdmin.getIconImage();
            vistaInicio.setVisible(true);
            vistaInicio.setIconImage(imageIcon);
        } else if (e.getWindow() == vistaEncargado) {
            envioCierreSesion();
            Image imageIcon = vistaEncargado.getIconImage();
            vistaInicio.setVisible(true);
            vistaInicio.setIconImage(imageIcon);
        } else if (e.getWindow() == vistaTrabajador) {
            envioCierreSesion();
            Image imageIcon = vistaTrabajador.getIconImage();
            vistaInicio.setVisible(true);
            vistaInicio.setIconImage(imageIcon);
        }
    }

    /**
     * Envia la señal de cierre de session
     */
    private void envioCierreSesion() {
        try {
            opc = 99;
            envioOpc();
        } catch (NoSuchPaddingException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (IllegalBlockSizeException ex) {
            ex.printStackTrace();
        } catch (BadPaddingException ex) {
            ex.printStackTrace();
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        } catch (InvalidKeyException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (vistaAdmin != null && vistaAdmin.isVisible()) {
            fila = vistaAdmin.contratosAdminTable.rowAtPoint(e.getPoint());
            if (e.getSource() == vistaAdmin.contratosAdminTable) {
                modelo.rellenarPanelContrato(modelo.selecionarContrato(trabajador.getContrato_trabajador(), vistaAdmin, fila, 0), vistaAdmin);
            } else if (e.getSource() == vistaAdmin.trabajadoresTable) {
                this.idAuxTrabajadorSeleccionado = Integer.valueOf((String) vistaAdmin.trabajadoresTable.getValueAt(vistaAdmin.trabajadoresTable.getSelectedRows()[0], 0));
                modelo.rellenarTablaContratoTrabajador(modelo.selecionarTrabajador(trabajadors, this.idAuxTrabajadorSeleccionado), vistaAdmin.dtmContratoTrabajadores, vistaAdmin.contratosTrabajadorTable);
            } else if (e.getSource() == vistaAdmin.imgLabelFinal) {
                try {
                    Desktop.getDesktop().browse(new URL("https://www.youtube.com/watch?v=i_cVJgIz_Cs").toURI());
                } catch (IOException | URISyntaxException e1) {
                    e1.printStackTrace();
                }
            }
        }

        if (vistaEncargado != null && vistaEncargado.isVisible()) {
            fila = vistaEncargado.encargdorTable.rowAtPoint(e.getPoint());
            if (e.getSource() == vistaEncargado.encargdorTable) {
                this.idAuxTrabajadorSeleccionado = Integer.valueOf((String) vistaEncargado.encargdorTable.getValueAt(vistaEncargado.encargdorTable.getSelectedRows()[0], 0));
                modelo.rellenarTablaContratoTrabajador(modelo.selecionarTrabajador(trabajadors, this.idAuxTrabajadorSeleccionado), vistaEncargado.dtmContratoTrabajadoresTrabajador, vistaEncargado.contratoTrabajadorTable);

            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
