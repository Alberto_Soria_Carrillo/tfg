package vista;

import controlador.Controlador;

import javax.swing.*;
import java.io.File;

/**
 * Clase VistaInicio. Se encarga de la ventana de login del programa
 *
 * @author Alberto Soria Carrillo
 */
public class VistaInicio extends JFrame {
    // Atributos
    private JPanel panel1;
    public JTextField textField1;
    public JPasswordField passwordField1;
    public JButton loginButton;

    // Constructor
    public VistaInicio() {

        File mk = new File("img");
        mk.mkdirs();

        this.setTitle("Acceso de Usuarios");

        if (Controlador.logo.exists()) {
            this.setIconImage(new ImageIcon(Controlador.logo.getPath()).getImage());
        } else {
            this.setIconImage(new ImageIcon(new File("img/delete.png").getAbsolutePath()).getImage());
        }

        addActionCommand();

        this.add(panel1);

        this.setResizable(false);
        this.pack();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);

    }

    /**
     * Agrega las referencias a los botones
     */
    private void addActionCommand() {

        loginButton.setActionCommand("VistaInicioLogin");

    }

}
