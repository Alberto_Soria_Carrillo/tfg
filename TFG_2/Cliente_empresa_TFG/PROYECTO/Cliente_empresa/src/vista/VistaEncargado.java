package vista;

import controlador.Controlador;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

/**
 * Clase VistaEncargado. se encarga de ejecutar la ventana del encargado
 *
 * @author Alberto Soria Carrillo
 */

public class VistaEncargado extends JFrame {

    // Atributos
    public JPanel panel1;
    public JTable encargdorTable;
    public JButton entradaButton;
    public JButton salidaButton;
    public JTable contratoTrabajadorTable;

    public DefaultTableModel dtmTrabajador;
    public DefaultTableModel dtmContratoTrabajadoresTrabajador;

    // Constrctor
    public VistaEncargado(){

        this.setTitle(Controlador.encargado);
        this.add(panel1);

        this.dtmTrabajador = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                super.isCellEditable(row, column);

                return column != 0 && column!= 16;
            }
        };
        this.dtmTrabajador.setColumnIdentifiers(new String[]{"id", "Dni", "Numero SS", "Nombre", "1 apellido", "2 apellido", "Fecha de nacimiento", "Genero", "Estado", "Nacionalidad", "Horario", "Telefono", "Email", "Pais", "Ciudad", "Calle", "Tipo de usuario"});
        this.encargdorTable.setModel(this.dtmTrabajador);

        this.dtmContratoTrabajadoresTrabajador = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                super.isCellEditable(row, column);

                return column != 0;
            }
        };
        this.dtmContratoTrabajadoresTrabajador.setColumnIdentifiers(new String[]{"id", "Horario", "Rango", "Fecha inicio", "Fecha finalización", "Pago", "Fecha despido", "Tipo despido", "Pais", "Ciudad", "Calle"});
        this.contratoTrabajadorTable.setModel(this.dtmContratoTrabajadoresTrabajador);

        addActionComand();

        this.setResizable(true);
        this.pack();
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setVisible(true);

    }

    /**
     * Agrega las referencias a los botones
     */
    private void addActionComand() {
        entradaButton.setActionCommand("VistaAdminRegistrarEntrada");
        salidaButton.setActionCommand("VistaAdminRegistrarSalida");
    }
}
