package vista;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

/**
 * Clase VistaAuxiliar. Se encarga de ejecutar la ventana auxiliar para la informacion de las entradas y las salidas de los trabajadores
 * @author Alberto Soria Carrillo
 */
public class VistaAuxiliar extends JFrame{

    // Atributos
    public JPanel panel1;
    public JTable auxTable;
    public JLabel tituloLabel;
    public JButton eliminarButton;

    public DefaultTableModel dtm;
    public String opc;

    // Constructor
    public VistaAuxiliar(String opc){

        this.opc = opc;
        this.dtm = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int row, int column) {
                super.isCellEditable(row, column);

                return column > 0;
            }
        };
        this.dtm .setColumnIdentifiers(new String[]{"id", "Fecha", this.opc});
        this.auxTable.setModel(this.dtm);

        this.addActionComand();
        this.add(panel1);
        this.setResizable(false);
        this.pack();
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setVisible(true);
    }

    /**
     * Agrega las referencias a los botones
     */
    private void addActionComand() {

        eliminarButton.setActionCommand("VistaAuxiliarEliminar");

    }


}
