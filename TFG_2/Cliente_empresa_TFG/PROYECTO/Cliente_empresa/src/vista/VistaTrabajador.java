package vista;

import controlador.Controlador;

import javax.swing.*;

/**
 * Clase VistaTrabajador. Clase que ejecuta la ventana para la vista del trabajador
 *
 * @author Alberto Soria Carrillo
 */
public class VistaTrabajador extends  JFrame{

    // Atributos
    public JButton entrarButton;
    public JPanel panel1;
    public JButton salidaButton;
    public JTextField nombreTextField;
    public JTextField apellido1TextField;
    public JTextField apellido2TextField;

    // Constructor
    public VistaTrabajador(){

        this.setTitle(Controlador.trabajado);

        this.add(panel1);

        addActionComand();

        this.setResizable(true);
        this.pack();
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setVisible(true);
    }

    /**
     * Agrega las referencias a los botones
     */
    private void addActionComand() {

        entrarButton.setActionCommand("VistaAdminRegistrarEntrada");
        salidaButton.setActionCommand("VistaAdminRegistrarSalida");

    }

}
