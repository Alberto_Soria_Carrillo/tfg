package vista;

import com.github.lgooddatepicker.components.DatePicker;
import controlador.Controlador;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

/**
 * Clase VistaAdmin. Ejecuta la ventana del administrador
 *
 * @author Alberto Soria Carrillo
 */
public class VistaAdmin extends JFrame {

    // Atributos
    public JPanel infoEmpresaPanel;
    public JTextField cifEmpresaTextField;
    public JTextField pagoNotarioEmpresaTextField;
    public JTextField nombreEmpresaTextField;
    public JTextField tipoEmpresaTextField;
    public JTextField capitalEmpresaTextField;
    public JTextField paisEmpresaTextField;
    public JTextField ciudadEmpresaTextField;
    public JTextField calleEmpresaTextField;
    public JPanel EmpresaPanel;
    public JTextField NombreTrabajadorTextField;
    public JTextField apellido1TrabajadorTextField;
    public JTextField apellido2TrabajadorTextField;
    public JTextField dniTrabajadorTextField;
    public JTextField ssTrabajadortextField;
    public JRadioButton hombreRadioButton;
    public JRadioButton mujerRadioButton;
    public JRadioButton NDRadioButton;
    public JTextField nacionalidadTrabajadorTextField;
    public JTextField horarioTrabajadorTextField;
    public JTextField telefonoTrabajadorTextField;
    public JTextField emailTrabajadorTextField;
    public JTextField paisTrabajadorTextField;
    public JTextField ciudadTrabajadorTextField;
    public JTextField calleTrabajadorTextField;
    public JPanel AdministradorPanel;
    public JButton editarButton;
    public JButton cancelarButton;
    public DatePicker empresaDatePicker;
    public DatePicker trabajadorDatePicker;
    public JTabbedPane tabbedPane;
    public JPanel contratoAdmin;
    public JTextField tipoDespidoTextField;
    public JTextField paisContratoTextField;
    public JTextField ciudadContratoTextField;
    public JTextField calleContratoTextField;
    public JButton guardarButton;
    public JTable contratosAdminTable;
    public JComboBox horasContratoComboBox;
    public DatePicker contratacionDatePicker;
    public DatePicker fTeoricaDatePicker;
    public DatePicker fRealDatePiker;
    public JComboBox rangoComboBox;
    public JTextField suelodTextField;
    public JButton nuevoButton;
    public JTable accionistasParticipadoresTable;
    public JTable aportadoresTable;
    public JTable testigosTable;
    public JButton nuevoAccionistaButton;
    public JButton eliminarAccinistaButton;
    public JButton nuevoAportadorButton;
    public JButton eliminarAportadorButton;
    public JButton nuevoTestigoButton;
    public JButton eliminarTestigoButton;
    public JTextField nombreNotariotextField;
    public JTextField numeroNotariotextField;
    public JTextField paisNotarioTextField;
    public JTextField ciudadNotariotextField;
    public JTextField calleNotariotextField;
    public JTextField dniNotarioTextField;
    public JButton guardarNotarioButton;
    public JTable trabajadoresTable;
    public JButton nuevoTrabajadoresButton1;
    public JButton eliminarTrabajadoresButton;
    public JTable contratosTrabajadorTable;
    public JButton nuevoContratoTrabajadorButton;
    public JButton eliminarContratoTrabajadorButton;
    public JButton verEntradasButton;
    public JButton verSalidasButton;
    public JButton registrarEntradaButton;
    public JButton registrarSalidaButton;
    public JButton imprimirListaDeTrabajadoresButton;
    public JButton imprimirDatosDeLaButton;
    public JLabel imgLabelFinal;
    public JPanel accionesPanel;
    public JButton logoButton;
    public JTextField logoTipoTextField;

    public DefaultTableModel dtmContratoAdmin;
    public DefaultTableModel dtmAccinistas;
    public DefaultTableModel dtmAportadores;
    public DefaultTableModel dtmTestigos;
    public DefaultTableModel dtmTrabajadores;
    public DefaultTableModel dtmContratoTrabajadores;

    // Constructores
    public VistaAdmin() throws HeadlessException {

        this.setTitle(Controlador.adnimnistrador);

        this.add(tabbedPane);

        this.dtmContratoAdmin = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        this.dtmContratoAdmin.setColumnIdentifiers(new String[]{"Id", "Horario", "Fecha de inicial", "Fecha de finalizacion"});
        this.contratosAdminTable.setModel(dtmContratoAdmin);

        this.dtmAccinistas = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                super.isCellEditable(row, column);

                return column > 0;
            }
        };
        this.dtmAccinistas.setColumnIdentifiers(new String[]{"Id", "Dni", "Nombre", "Porcentaje", "Cuantia", "Tipo de moneda"});
        this.accionistasParticipadoresTable.setModel(this.dtmAccinistas);

        this.dtmAportadores = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                super.isCellEditable(row, column);

                return column > 0;
            }
        };
        this.dtmAportadores.setColumnIdentifiers(new String[]{"Id", "Dni / Cif", "Nombre", "Cuantia", "Tipo de Moneda"});
        this.aportadoresTable.setModel(this.dtmAportadores);

        this.dtmTestigos = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                super.isCellEditable(row, column);

                return column > 0;
            }
        };
        this.dtmTestigos.setColumnIdentifiers(new String[]{"Id", "Dni / Cif", "Nombre"});
        this.testigosTable.setModel(this.dtmTestigos);

        this.dtmTrabajadores = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                super.isCellEditable(row, column);

                return column > 0;
            }
        };
        this.dtmTrabajadores.setColumnIdentifiers(new String[]{"id", "Dni", "Numero SS", "Nombre", "1 apellido", "2 apellido", "Fecha de nacimiento", "Genero", "Estado", "Nacionalidad", "Horario", "Telefono", "Email", "Pais", "Ciudad", "Calle", "Tipo de usuario"});
        this.trabajadoresTable.setModel(this.dtmTrabajadores);

        this.dtmContratoTrabajadores = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                super.isCellEditable(row, column);

                return column > 0;
            }
        };
        this.dtmContratoTrabajadores.setColumnIdentifiers(new String[]{"id", "Horario", "Rango", "Fecha inicio", "Fecha finalización", "Pago", "Fecha despido", "Tipo despido", "Pais", "Ciudad", "Calle"});
        this.contratosTrabajadorTable.setModel(this.dtmContratoTrabajadores);

        addActionCommand();

        this.setResizable(true);
        this.pack();
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setVisible(true);

    }

    /**
     * Agrega las referencias a los botones
     */
    private void addActionCommand() {

        editarButton.setActionCommand("VistaAdminEditar");
        cancelarButton.setActionCommand("VistaADminCancelar");
        guardarButton.setActionCommand("VistaAdminGuardar");
        nuevoButton.setActionCommand("VistaAdminNuevo");
        logoButton.setActionCommand("VistaAdminLogo");

        guardarNotarioButton.setActionCommand("VistaAdminGuardarNotario");

        nuevoAccionistaButton.setActionCommand("VistaAdminNuevoAccionista");
        nuevoAportadorButton.setActionCommand("VistaAdminNuevoAportador");
        nuevoTestigoButton.setActionCommand("VistaAdminNuevoTestigo");
        nuevoTrabajadoresButton1.setActionCommand("VistaAdminNuevoTrabajador");
        nuevoContratoTrabajadorButton.setActionCommand("VistaAdminNuevoContratoTrabajador");

        eliminarAccinistaButton.setActionCommand("VistaAdminEliminarAccionisa");
        eliminarAportadorButton.setActionCommand("VistaAdminEliminarAportadores");
        eliminarTestigoButton.setActionCommand("VistaAdminElminarTestigos");
        eliminarTrabajadoresButton.setActionCommand("VistaAdminEliminarTrabajador");
        eliminarContratoTrabajadorButton.setActionCommand("VistaAdminEliminarContratoTrabajador");

        verEntradasButton.setActionCommand("VistaAdminVerEntradas");
        verSalidasButton.setActionCommand("VistaAdminVerSalidas");

        registrarEntradaButton.setActionCommand("VistaAdminRegistrarEntrada");
        registrarSalidaButton.setActionCommand("VistaAdminRegistrarSalida");

        imprimirListaDeTrabajadoresButton.setActionCommand("VistaAdminImpimirTrabajadores");
        imprimirDatosDeLaButton.setActionCommand("VistaAdminImprimirEmpresa");

    }

}
