package clasesAuxiliares;

/**
 * Clase Notario. Tiene la estructura del objeto notario de la BBDD
 *
 * @author Alberto Soria Carrillo
 */
public class Notario {
	
	private String DNI_CIF;
	private int id;
	private String nombre;
	private String numero_colegiado;
	private String pais;
	private String ciudad;
	private String calle;

	public Notario() {}

	public String getDNI_CIF() {
		return DNI_CIF;
	}

	public void setDNI_CIF(String dNI_CIF) {
		DNI_CIF = dNI_CIF;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNumero_colegiado() {
		return numero_colegiado;
	}

	public void setNumero_colegiado(String numero_colegiado) {
		this.numero_colegiado = numero_colegiado;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}
	
	
	
}
