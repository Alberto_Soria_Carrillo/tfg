package clasesAuxiliares;

/**
 * Clase NamePassUser. Tiene la estructura del objeto name_pass_usser de la BBDD
 *
 * @author Alberto Soria Carrillo
 */
public class NamePassUser {
    // Atributos
    private int id;
    private String nombre;
    private String pass;

    // Constructores
    public NamePassUser() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
}
