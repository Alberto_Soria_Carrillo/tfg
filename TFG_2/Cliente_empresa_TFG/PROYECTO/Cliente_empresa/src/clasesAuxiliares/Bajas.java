package clasesAuxiliares;

/**
 * Clase Bajas. Tiene la estructura del objeto bajas de la BBDD
 *
 * @author Alberto Soria Carrillo
 */
public class Bajas {

	// Atributos
	private int id;
	private String tipo;
	private String mutua;
	private String expediente;
	private String numero_policia;
	private String descripcion;
	private String observacion;
	private String fecha_incio;
	private String fecha_final;
	private Trabajador trabajador;
	
// Getters and Setters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getMutua() {
		return mutua;
	}

	public void setMutua(String mutua) {
		this.mutua = mutua;
	}

	public String getExpediente() {
		return expediente;
	}

	public void setExpediente(String expediente) {
		this.expediente = expediente;
	}

	public String getNumero_policia() {
		return numero_policia;
	}

	public void setNumero_policia(String numero_policia) {
		this.numero_policia = numero_policia;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public String getFecha_incio() {
		return fecha_incio;
	}

	public void setFecha_incio(String fecha_incio) {
		this.fecha_incio = fecha_incio;
	}

	public String getFecha_final() {
		return fecha_final;
	}

	public void setFecha_final(String fecha_final) {
		this.fecha_final = fecha_final;
	}

	public Trabajador getTrabajador() {
		return trabajador;
	}

	public void setTrabajador(Trabajador trabajador) {
		this.trabajador = trabajador;
	}

}
