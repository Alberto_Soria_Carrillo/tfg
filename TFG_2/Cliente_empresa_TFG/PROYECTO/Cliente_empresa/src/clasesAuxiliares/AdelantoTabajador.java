package clasesAuxiliares;

/**
 * Clase AdelatoTrabajador. Tiene la estructura del objeto AdelantoTrabaajador de la BBDD
 *
 * @author Alberto Soria Carrillo
 */
public class AdelantoTabajador {

	// Atributos
	private int id;
	private Trabajador trabajador_id;
	private String cantidad;
	private String fecha;
	
	// Getters and Setters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Trabajador getTrabajador_id() {
		return trabajador_id;
	}

	public void setTrabajador_id(Trabajador trabajador_id) {
		this.trabajador_id = trabajador_id;
	}

	public String getCantidad() {
		return cantidad;
	}

	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

}
