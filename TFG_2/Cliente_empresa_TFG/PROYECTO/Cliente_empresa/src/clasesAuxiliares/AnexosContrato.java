package clasesAuxiliares;

/**
 * Clase AnexosContrato. Tiene la estructura del objeto AnexosContrato de la BBDD
 *
 * @author Alberto Soria Carrillo
 */
public class AnexosContrato {

	// Atributos
	private int id;
	private String texto;
	private String descripcion;
	private String obserbaciones;
	private int contrato_id;
	
	// Getters and Setters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getObserbaciones() {
		return obserbaciones;
	}

	public void setObserbaciones(String obserbaciones) {
		this.obserbaciones = obserbaciones;
	}

	public int getContrato_id() {
		return contrato_id;
	}

	public void setContrato_id(int contrato_id) {
		this.contrato_id = contrato_id;
	}

}
