package clasesAuxiliares;

/**
 * Clase AccionistasParticipadores. Tiene la estructura del objeto AccionistasParticipadores de la BBDD
 */
public class AccionistasParticipadores {

	// Atributos
	private String DNI_CIF;
	private int id;
	private String nombre;
	private String porcetnaje;
	private String totales;
	private String tipo_moneda;
 
	private Empresa empresa_id;

	// Getters and Setters
	public String getDNI_CIF() {
		return DNI_CIF;
	}

	public void setDNI_CIF(String dNI_CIF) {
		DNI_CIF = dNI_CIF;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPorcetnaje() {
		return porcetnaje;
	}

	public void setPorcetnaje(String porcetnaje) {
		this.porcetnaje = porcetnaje;
	}

	public String getTotales() {
		return totales;
	}

	public void setTotales(String totales) {
		this.totales = totales;
	}

	public String getTipo_moneda() {
		return tipo_moneda;
	}

	public void setTipo_moneda(String tipo_moneda) {
		this.tipo_moneda = tipo_moneda;
	}

	public Empresa getEmpresa_id() {
		return empresa_id;
	}

	public void setEmpresa_id(Empresa empresa_id) {
		this.empresa_id = empresa_id;
	}
}
