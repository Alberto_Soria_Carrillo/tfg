package clasesAuxiliares;

/**
 * Clase Testigos. Tiene la estructura del objeto testigos de la BBDD
 *
 * @author Alberto Soria Carrillo
 */
public class Testigos {

	// Atributos
	private String DNI_CIF;
	private int id;
	private String nombre;
	private Empresa empresa_id;

	// Getters and Setters
	public String getDNI_CIF() {
		return DNI_CIF;
	}

	public void setDNI_CIF(String dNI_CIF) {
		DNI_CIF = dNI_CIF;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Empresa getEmpresa_id() {
		return empresa_id;
	}

	public void setEmpresa_id(Empresa empresa_id) {
		this.empresa_id = empresa_id;
	}

	@Override
	public String toString() {
		return "Testigos{" +
				"DNI_CIF='" + DNI_CIF + '\'' +
				", id=" + id +
				", nombre='" + nombre + '\'' +
				", empresa_id=" + empresa_id +
				'}';
	}
}
