package modelo;

import clasesAuxiliares.*;
import com.github.lgooddatepicker.components.DatePicker;
import com.google.gson.Gson;
import controlador.Controlador;
import datePicker.DatePickerTableEditor;
import libreriaAlberto.Crypto;
import rsa.RSA;
import vista.*;

import javax.crypto.*;
import javax.swing.*;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ItemListener;
import java.awt.event.MouseListener;
import java.awt.event.WindowListener;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Clase Modelo. Clase donde se encuentran todos los metodos invocados por la clase Controlador
 *
 * @author Alberto Soria Carrillo
 */
public class Modelo {

    /**
     * Descifra el objeto trabajador recibido
     *
     * @param trabajador
     * @param rsaCliente
     * @return
     * @throws IllegalBlockSizeException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     */
    public Trabajador descrifrarTrabajador(Trabajador trabajador, RSA rsaCliente, Gson gson, Crypto crypto, DataInputStream entrada, String passAes, Boolean b) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, IOException {

        // objeto trabajador
        trabajador.setId(trabajador.getId());
        trabajador.setDni_cif(rsaCliente.Decrypt(trabajador.getDni_cif()));
        trabajador.setNumero_ss(rsaCliente.Decrypt(trabajador.getNumero_ss()));
        trabajador.setNombre(rsaCliente.Decrypt(trabajador.getNombre()));
        trabajador.setApellido1(rsaCliente.Decrypt(trabajador.getApellido1()));
        trabajador.setApellido2(rsaCliente.Decrypt(trabajador.getApellido2()));
        trabajador.setFecha_nacimiento(rsaCliente.Decrypt(trabajador.getFecha_nacimiento()));
        trabajador.setGenero(rsaCliente.Decrypt(trabajador.getGenero()));
        trabajador.setEstado(rsaCliente.Decrypt(trabajador.getEstado()));
        trabajador.setNacionalidad(rsaCliente.Decrypt(trabajador.getNacionalidad()));
        trabajador.setHorario(rsaCliente.Decrypt(trabajador.getHorario()));
        trabajador.setTelefono(rsaCliente.Decrypt(trabajador.getTelefono()));
        trabajador.setEmail(rsaCliente.Decrypt(trabajador.getEmail()));
        trabajador.setPais(rsaCliente.Decrypt(trabajador.getPais()));
        trabajador.setCiudad(rsaCliente.Decrypt(trabajador.getCiudad()));
        trabajador.setCalle(rsaCliente.Decrypt(trabajador.getCalle()));
        trabajador.setTipo_usuario(rsaCliente.Decrypt(trabajador.getTipo_usuario()));

        if (b) {
            // objeto adelanto_trabajador
            int count = gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Integer.class);
            for (int i = 0; i < count; i++) {
                trabajador.getAdelanto_trabajador().add(descrifrarAdelantoTrabajador(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), AdelantoTabajador.class), rsaCliente));
            }

            // objeto Salidas
            count = gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Integer.class);
            for (int i = 0; i < count; i++) {
                trabajador.getSalida().add(descrifrarSalidas(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Salidas.class), rsaCliente));
            }

            // objeto Entradas
            count = gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Integer.class);
            for (int i = 0; i < count; i++) {
                trabajador.getEntrada().add(descrifrarEntradas(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Entradas.class), rsaCliente));
            }

            // objeto Baja
            count = gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Integer.class);
            for (int i = 0; i < count; i++) {
                trabajador.getBaja().add(descrifrarBajas(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Bajas.class), rsaCliente));
            }

            // objeto Contrato
            count = gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Integer.class);
            for (int i = 0; i < count; i++) {
                trabajador.getContrato_trabajador().add(descifrarContratoTrabajador(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), ContratoTrabajador.class), rsaCliente, gson, crypto, entrada, passAes, true));
            }

            // descrifra el responsable evitando el bucle en caso de que el resposable y el trabajador sea la misma persona
            Trabajador responsable = gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Trabajador.class);
            if (responsable.getId() != 0 && responsable.getId() != trabajador.getId()) {
                trabajador.setResposable(descrifrarTrabajador(responsable, rsaCliente, gson, crypto, entrada, passAes, true));
            }
        }
        return trabajador;
    }

    /**
     * Descifra el objeto ContratoTrabajador
     *
     * @param contratoTrabajador
     * @param rsa
     * @param gson
     * @param crypto
     * @param entrada
     * @param passAes
     * @param b
     * @return
     * @throws IllegalBlockSizeException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     * @throws IOException
     */
    public ContratoTrabajador descifrarContratoTrabajador(ContratoTrabajador contratoTrabajador, RSA rsa, Gson gson, Crypto crypto, DataInputStream entrada, String passAes, boolean b) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, IOException {

        contratoTrabajador.setId(contratoTrabajador.getId());
        contratoTrabajador.setHorario(rsa.Decrypt(contratoTrabajador.getHorario()));
        contratoTrabajador.setRango(rsa.Decrypt(contratoTrabajador.getRango()));
        contratoTrabajador.setFecha_inicio(rsa.Decrypt(contratoTrabajador.getFecha_inicio()));
        contratoTrabajador.setFecha_finalizacion(rsa.Decrypt(contratoTrabajador.getFecha_finalizacion()));
        contratoTrabajador.setPago_diario(rsa.Decrypt(contratoTrabajador.getPago_diario()));
        contratoTrabajador.setFecha_despido(rsa.Decrypt(contratoTrabajador.getFecha_despido()));
        contratoTrabajador.setTipo_despido(rsa.Decrypt(contratoTrabajador.getTipo_despido()));
        contratoTrabajador.setPais(rsa.Decrypt(contratoTrabajador.getPais()));
        contratoTrabajador.setCiudad(rsa.Decrypt(contratoTrabajador.getCiudad()));
        contratoTrabajador.setCalle(rsa.Decrypt(contratoTrabajador.getCalle()));

        if (b) {
            int count = gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Integer.class);
            for (int i = 0; i < count; i++) {
                contratoTrabajador.getAnexos_contrato().add(descifrarAnexosContrato(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), AnexosContrato.class), rsa));
            }
        }
        return contratoTrabajador;

    }

    /**
     * Descifra el objeto Anexo de contratos
     *
     * @param anexosContrato
     * @param rsa
     * @return
     * @throws IllegalBlockSizeException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     */
    public AnexosContrato descifrarAnexosContrato(AnexosContrato anexosContrato, RSA rsa) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        anexosContrato.setId(anexosContrato.getId());
        anexosContrato.setTexto(rsa.Decrypt(anexosContrato.getTexto()));
        anexosContrato.setDescripcion(rsa.Decrypt(anexosContrato.getDescripcion()));
        anexosContrato.setObserbaciones(rsa.Decrypt(anexosContrato.getObserbaciones()));

        return anexosContrato;
    }

    /**
     * Descifra el objeto baja
     *
     * @param bajas
     * @param rsaCliente
     * @throws IllegalBlockSizeException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     */
    public Bajas descrifrarBajas(Bajas bajas, RSA rsaCliente) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {

        bajas.setId(bajas.getId());
        bajas.setTipo(rsaCliente.Decrypt(bajas.getTipo()));
        bajas.setMutua(rsaCliente.Decrypt(bajas.getMutua()));
        bajas.setExpediente(rsaCliente.Decrypt(bajas.getExpediente()));
        bajas.setNumero_policia(rsaCliente.Decrypt(bajas.getNumero_policia()));
        bajas.setDescripcion(rsaCliente.Decrypt(bajas.getDescripcion()));
        bajas.setObservacion(rsaCliente.Decrypt(bajas.getObservacion()));
        bajas.setFecha_incio(rsaCliente.Decrypt(bajas.getFecha_incio()));
        bajas.setFecha_final(rsaCliente.Decrypt(bajas.getFecha_final()));

        return bajas;

    }

    /**
     * Descifra el objeto Entrada
     *
     * @param entradas
     * @param rsaCliente
     * @throws IllegalBlockSizeException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     */
    public Entradas descrifrarEntradas(Entradas entradas, RSA rsaCliente) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {

        entradas.setId(entradas.getId());
        entradas.setFecha(rsaCliente.Decrypt(entradas.getFecha()));
        entradas.setSalida(rsaCliente.Decrypt(entradas.getSalida()));

        return entradas;
    }

    /**
     * Descrifra el objeto Salidas
     *
     * @param salidas
     * @param rsaCliente
     * @throws IllegalBlockSizeException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     */
    public Salidas descrifrarSalidas(Salidas salidas, RSA rsaCliente) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {

        salidas.setId(salidas.getId());
        salidas.setFecha(rsaCliente.Decrypt(salidas.getFecha()));
        salidas.setSalida(rsaCliente.Decrypt(salidas.getSalida()));

        return salidas;

    }

    /**
     * Descrifrar el objeto AdelantoTrabajador
     *
     * @param adelantoTabajador
     * @param rsaCliente
     * @return
     * @throws IllegalBlockSizeException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     */
    public AdelantoTabajador descrifrarAdelantoTrabajador(AdelantoTabajador adelantoTabajador, RSA rsaCliente) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {

        adelantoTabajador.setId(adelantoTabajador.getId());
        adelantoTabajador.setCantidad(rsaCliente.Decrypt(adelantoTabajador.getCantidad()));
        adelantoTabajador.setFecha(rsaCliente.Decrypt(adelantoTabajador.getFecha()));

        return adelantoTabajador;

    }


    /**
     * Descifra el objeto empresa
     *
     * @param empresa
     * @param rsa
     * @return
     * @throws IllegalBlockSizeException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     */
    public Empresa descifrarEmpresa(Empresa empresa, RSA rsa, Gson gson, Crypto crypto, DataInputStream entrada, String passAes, boolean b) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, IOException {

        empresa.setId(empresa.getId());
        empresa.setCIF(rsa.Decrypt(empresa.getCIF()));
        empresa.setTexto(rsa.Decrypt(empresa.getTexto()));
        empresa.setFecha(rsa.Decrypt(empresa.getFecha()));
        empresa.setPago_notario(rsa.Decrypt(empresa.getPago_notario()));
        empresa.setResumen_condiciones(rsa.Decrypt(empresa.getResumen_condiciones()));
        empresa.setObservaciones(rsa.Decrypt(empresa.getObservaciones()));
        empresa.setNombre_empresa(rsa.Decrypt(empresa.getNombre_empresa()));
        empresa.setTipo_empresa(rsa.Decrypt(empresa.getTipo_empresa()));
        empresa.setCapital_aportado(rsa.Decrypt(empresa.getCapital_aportado()));
        empresa.setPais(rsa.Decrypt(empresa.getPais()));
        empresa.setCiudad(rsa.Decrypt(empresa.getCiudad()));
        empresa.setCalle(rsa.Decrypt(empresa.getCalle()));

        // objeto notario
        if (b) {
            empresa.setNotario_id(descifararNorario(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Notario.class), rsa));
        }

        if (b) {
            //objeto testigos
            int count = gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Integer.class);
            for (int i = 0; i < count; i++) {
                empresa.getTestigos().add(descifrarTestigo(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Testigos.class), rsa));
            }

            // objeto accinistas participadores
            count = gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Integer.class);
            for (int i = 0; i < count; i++) {
                empresa.getAccionistas_participadores().add(descifrarAccionistasParticipadores(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), AccionistasParticipadores.class), rsa));
            }

            // objeto local
            count = gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Integer.class);
            for (int i = 0; i < count; i++) {
                empresa.getLocales().add(descifrarLocales(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Locales.class), rsa));
            }

            // objeto negocio
            count = gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Integer.class);
            for (int i = 0; i < count; i++) {
                empresa.getNegocio().add(descifrarNegocio(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Negocios.class), rsa));
            }

            // objeto aportadores
            count = gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Integer.class);
            for (int i = 0; i < count; i++) {
                empresa.getAportadors().add(descifrarAportadores(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Aportadores.class), rsa));
            }

            // objeto papeles
            count = gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Integer.class);
            for (int i = 0; i < count; i++) {
                empresa.getPapeles().add(descifrarPapeles(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Papeles.class), rsa));
            }
        }
        return empresa;
    }

    /**
     * Descifra el objeto aportadores
     *
     * @param aportadores
     * @param rsa
     * @throws IllegalBlockSizeException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     */
    public Aportadores descifrarAportadores(Aportadores aportadores, RSA rsa) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {

        aportadores.setId(aportadores.getId());
        aportadores.setDNI_CIF(rsa.Decrypt(aportadores.getDNI_CIF()));
        aportadores.setCantidad(rsa.Decrypt(aportadores.getCantidad()));
        aportadores.setMoneda(rsa.Decrypt(aportadores.getMoneda()));
        aportadores.setNombre(rsa.Decrypt(aportadores.getNombre()));

        if (aportadores.getRepresentante_de() != null) {
            if (aportadores.getRepresentante_de().getId() != 0 && aportadores.getRepresentante_de().getId() != aportadores.getId()) {
                descifrarAportadores(aportadores.getRepresentante_de(), rsa);
            }
        }

        return aportadores;

    }

    /**
     * Descifra el objeto Papeles
     *
     * @param papeles
     * @param rsa
     * @throws IllegalBlockSizeException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     */
    public Papeles descifrarPapeles(Papeles papeles, RSA rsa) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {

        papeles.setId(papeles.getId());
        papeles.setNombre(rsa.Decrypt(papeles.getNombre()));
        papeles.setNumero_referencia(rsa.Decrypt(papeles.getNumero_referencia()));
        papeles.setDescripcion(rsa.Decrypt(papeles.getDescripcion()));
        papeles.setObservaciones(rsa.Decrypt(papeles.getObservaciones()));
        papeles.setRuta(rsa.Decrypt(papeles.getRuta()));
        papeles.setFecha_guardado(rsa.Decrypt(papeles.getFecha_guardado()));
        papeles.setFecha_papeles(rsa.Decrypt(papeles.getFecha_papeles()));

        return papeles;

    }

    /**
     * Descifra el objeto papeles
     *
     * @param negocios
     * @param rsa
     * @throws IllegalBlockSizeException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     */
    public Negocios descifrarNegocio(Negocios negocios, RSA rsa) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {

        negocios.setId(negocios.getId());
        negocios.setNombre_comercial(rsa.Decrypt(negocios.getNombre_comercial()));
        negocios.setTipo(rsa.Decrypt(negocios.getTipo()));
        negocios.setParticipacion(rsa.Decrypt(negocios.getParticipacion()));
        negocios.setPais(rsa.Decrypt(negocios.getPais()));
        negocios.setCiudad(rsa.Decrypt(negocios.getCiudad()));
        negocios.setCalle(rsa.Decrypt(negocios.getCalle()));

        return negocios;

    }

    /**
     * Descifra el objeto Locales
     *
     * @param locales
     * @param rsa
     * @throws IllegalBlockSizeException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     */
    public Locales descifrarLocales(Locales locales, RSA rsa) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {

        locales.setId(locales.getId());
        locales.setMetros_cuadrados(rsa.Decrypt(locales.getMetros_cuadrados()));
        locales.setObservaciones(rsa.Decrypt(locales.getObservaciones()));
        locales.setPais(rsa.Decrypt(locales.getPais()));
        locales.setCiudad(rsa.Decrypt(locales.getCiudad()));
        locales.setCalle(rsa.Decrypt(locales.getCalle()));

        return locales;
    }

    /**
     * Descifra el objeto AccionistasParticipadores
     *
     * @param accPar
     * @param rsa
     * @throws IllegalBlockSizeException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     */
    public AccionistasParticipadores descifrarAccionistasParticipadores(AccionistasParticipadores accPar, RSA rsa) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {

        accPar.setId(accPar.getId());
        accPar.setDNI_CIF(rsa.Decrypt(accPar.getDNI_CIF()));
        accPar.setNombre(rsa.Decrypt(accPar.getNombre()));
        accPar.setPorcetnaje(rsa.Decrypt(accPar.getPorcetnaje()));
        accPar.setTotales(rsa.Decrypt(accPar.getTotales()));
        accPar.setTipo_moneda(rsa.Decrypt(accPar.getTipo_moneda()));

        return accPar;

    }

    /**
     * Descifra el objeto testigo
     *
     * @param testigos
     * @param rsa
     * @throws IllegalBlockSizeException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     */
    public Testigos descifrarTestigo(Testigos testigos, RSA rsa) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {

        testigos.setId(testigos.getId());
        testigos.setDNI_CIF(rsa.Decrypt(testigos.getDNI_CIF()));
        testigos.setNombre(rsa.Decrypt(testigos.getNombre()));

        return testigos;
    }

    /**
     * Descifra el objeto notario
     *
     * @param notario
     * @param rsa
     * @throws IllegalBlockSizeException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     */
    public Notario descifararNorario(Notario notario, RSA rsa) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {

        notario.setId(notario.getId());
        notario.setDNI_CIF(rsa.Decrypt(notario.getDNI_CIF()));
        notario.setNumero_colegiado(rsa.Decrypt(notario.getNumero_colegiado()));
        notario.setNombre(rsa.Decrypt(notario.getNombre()));
        notario.setPais(rsa.Decrypt(notario.getPais()));
        notario.setCiudad(rsa.Decrypt(notario.getCiudad()));
        notario.setCalle(rsa.Decrypt(notario.getCalle()));

        return notario;
    }

    /**
     * Rellena los elementos visuales del a JFrame VistaAdmin
     *
     * @param vistaAdmin
     * @param trabajador
     * @param empresa
     * @param trabajadors
     * @param salida
     * @param gson
     * @param crypto
     * @param passAes
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     * @throws IOException
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     * @throws InvalidKeyException
     */
    public void rellenarVistaAdmin(VistaAdmin vistaAdmin, Trabajador trabajador, Empresa empresa, ArrayList<Trabajador> trabajadors, DataOutputStream salida, Gson gson, Crypto crypto, String passAes) throws NoSuchPaddingException, NoSuchAlgorithmException, IOException, BadPaddingException, IllegalBlockSizeException, InvalidKeyException {

        // panel 1
        vistaAdmin.cifEmpresaTextField.setText(empresa.getCIF());
        vistaAdmin.pagoNotarioEmpresaTextField.setText(empresa.getPago_notario());
        vistaAdmin.nombreEmpresaTextField.setText(empresa.getNombre_empresa());
        vistaAdmin.tipoEmpresaTextField.setText(empresa.getTipo_empresa());
        vistaAdmin.capitalEmpresaTextField.setText(empresa.getCapital_aportado());
        vistaAdmin.paisEmpresaTextField.setText(empresa.getPais());
        vistaAdmin.ciudadEmpresaTextField.setText(empresa.getCiudad());
        vistaAdmin.calleEmpresaTextField.setText(empresa.getCalle());

        // si existe el notario lo rellena, en caso de no existir crea el objeto para prevenir el null pointer
        if (empresa.getNotario_id() != null) {
            rellenarPanelNotario(vistaAdmin, empresa.getNotario_id());
        } else {
            empresa.setNotario_id(new Notario());
        }

        vistaAdmin.NombreTrabajadorTextField.setText(trabajador.getNombre());
        vistaAdmin.apellido1TrabajadorTextField.setText(trabajador.getApellido1());
        vistaAdmin.apellido2TrabajadorTextField.setText(trabajador.getApellido2());
        vistaAdmin.dniTrabajadorTextField.setText(trabajador.getDni_cif());
        vistaAdmin.ssTrabajadortextField.setText(trabajador.getNumero_ss());
        vistaAdmin.nacionalidadTrabajadorTextField.setText(trabajador.getNacionalidad());
        vistaAdmin.horarioTrabajadorTextField.setText(trabajador.getHorario());
        vistaAdmin.telefonoTrabajadorTextField.setText(trabajador.getTelefono());
        vistaAdmin.emailTrabajadorTextField.setText(trabajador.getEmail());
        vistaAdmin.paisTrabajadorTextField.setText(trabajador.getPais());
        vistaAdmin.ciudadTrabajadorTextField.setText(trabajador.getCiudad());
        vistaAdmin.calleTrabajadorTextField.setText(trabajador.getCalle());

        if (trabajador.getGenero().equalsIgnoreCase("true")) {
            vistaAdmin.hombreRadioButton.setSelected(true);
        } else if (trabajador.getGenero().equalsIgnoreCase("false")) {
            vistaAdmin.mujerRadioButton.setSelected(true);
        } else {
            vistaAdmin.NDRadioButton.setSelected(true);
        }

        rellenarDatePicker(vistaAdmin.empresaDatePicker, empresa.getFecha());
        rellenarDatePicker(vistaAdmin.trabajadorDatePicker, trabajador.getFecha_nacimiento());
        rellenarTablaContrato(vistaAdmin.dtmContratoAdmin, trabajador.getContrato_trabajador());
        rellenarTablaAccinistas(vistaAdmin.dtmAccinistas, empresa.getAccionistas_participadores());
        rellenarTablaAportadores(vistaAdmin.dtmAportadores, empresa.getAportadors());
        rellenarTablaTestigos(vistaAdmin.dtmTestigos, empresa.getTestigos());
        rellenarTablaTrabajadores(vistaAdmin.dtmTrabajadores, trabajadors, vistaAdmin.trabajadoresTable);
        rellenarComboBoxHoras(vistaAdmin.horasContratoComboBox);
        rellenarComboBoxRango(vistaAdmin.rangoComboBox);
        rellenarImagenFinal(vistaAdmin.imgLabelFinal);
    }

    /**
     * Agrega el icono a la ventana, en caso de no existir lo descarga del servidor y lo agrega. Si no existe en el servidor agrega la imagen por defecto. Si el negro
     *
     * @param vistaAdmin
     * @param empresa
     * @param salida
     * @param crypto
     * @param gson
     * @param passAes
     * @param logo
     * @throws NoSuchPaddingException
     * @throws IOException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     */
    public void rellenarIconoVentana(VistaAdmin vistaAdmin, Empresa empresa, DataOutputStream salida, Crypto crypto, Gson gson, String passAes, File logo, Socket socket) throws NoSuchPaddingException, IOException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, InvalidKeyException {

        if (logo.exists()) {
            vistaAdmin.setIconImage(new ImageIcon(logo.getPath()).getImage());
        } else {
            for (int i = 0; i < empresa.getPapeles().size(); i++) {
                if (empresa.getPapeles().get(i).getNombre().equalsIgnoreCase("logo")) {
                    salida.writeUTF(crypto.encriptar(gson.toJson(25), passAes));
                    salida.writeUTF(crypto.encriptar(gson.toJson(empresa.getPapeles().get(i).getRuta()), passAes));
                    try {
                        ServerSocket ss = new ServerSocket((socket.getLocalPort() + 2));
                        Socket s2 = ss.accept();
                        OutputStream output = new FileOutputStream(logo.getPath());
                        InputStream in = s2.getInputStream();

                        int bytesRead;
                        byte[] buffer = new byte[1024];
                        while ((bytesRead = in.read(buffer)) != -1) {
                            output.write(buffer, 0, bytesRead);
                        }

                        in.close();
                        s2.close();
                        output.close();
                    } catch (Exception e) {
                        vistaAdmin.setIconImage(new ImageIcon("img/delete.png").getImage());
                        break;
                    }
                    break;
                }
            }

            if (logo.exists()) {
                vistaAdmin.setIconImage(new ImageIcon(logo.getPath()).getImage());
            } else {
                vistaAdmin.setIconImage(new ImageIcon("img/delete.png").getImage());
            }
        }

    }

    /**
     * Rellena el JLabel de la imagen final
     *
     * @param imgLabelFinal
     */
    private void rellenarImagenFinal(JLabel imgLabelFinal) {

        File doc = new File("img");

        doc.mkdirs();

        File file = new File("img/delete.png");

        if (file.exists()) {

            imgLabelFinal.setSize(200, 200);
            imgLabelFinal.setIcon(new ImageIcon(new ImageIcon(file.getPath()).getImage().getScaledInstance(imgLabelFinal.getWidth(),
                    imgLabelFinal.getHeight(), Image.SCALE_SMOOTH)));


        }

    }

    /**
     * Rellena la informacion del notario
     *
     * @param vistaAdmin
     * @param notario_id
     */
    public void rellenarPanelNotario(VistaAdmin vistaAdmin, Notario notario_id) {

        vistaAdmin.nombreNotariotextField.setText(notario_id.getNombre());
        vistaAdmin.numeroNotariotextField.setText(notario_id.getNumero_colegiado());
        vistaAdmin.paisNotarioTextField.setText(notario_id.getPais());
        vistaAdmin.ciudadNotariotextField.setText(notario_id.getCiudad());
        vistaAdmin.calleNotariotextField.setText(notario_id.getCalle());
        vistaAdmin.dniNotarioTextField.setText(notario_id.getDNI_CIF());

    }

    /**
     * Rellena la Tabla Testigos
     *
     * @param dtm
     * @param testigos
     */
    public void rellenarTablaTestigos(DefaultTableModel dtm, ArrayList<Testigos> testigos) {
        dtm.setRowCount(0);

        for (Testigos aux : testigos) {
            dtm.addRow(new String[]{aux.getId() + "", aux.getDNI_CIF(), aux.getNombre()});
        }


    }

    /**
     * Rellena la tabla Aportadores
     *
     * @param dtm
     * @param aportadors
     */
    public void rellenarTablaAportadores(DefaultTableModel dtm, ArrayList<Aportadores> aportadors) {
        dtm.setRowCount(0);

        for (Aportadores aux : aportadors) {
            dtm.addRow(new String[]{aux.getId() + "", aux.getDNI_CIF(), aux.getNombre(), aux.getCantidad(), aux.getMoneda()});
        }
    }

    /**
     * Rellena la tabla Accionistas
     *
     * @param dtm
     * @param accionistas_participadores
     */
    public void rellenarTablaAccinistas(DefaultTableModel dtm, ArrayList<AccionistasParticipadores> accionistas_participadores) {

        dtm.setRowCount(0);

        for (AccionistasParticipadores aux : accionistas_participadores) {
            dtm.addRow(new String[]{aux.getId() + "", aux.getDNI_CIF(), aux.getNombre(), aux.getPorcetnaje(), aux.getTotales(), aux.getTipo_moneda()});
        }


    }

    /**
     * Rellena el combo con los rangos
     *
     * @param rangoComboBox
     */
    private void rellenarComboBoxRango(JComboBox rangoComboBox) {

        rangoComboBox.removeAllItems();

        rangoComboBox.addItem(Controlador.adnimnistrador);
        rangoComboBox.addItem(Controlador.encargado);
        rangoComboBox.addItem(Controlador.trabajado);
        rangoComboBox.setSelectedIndex(-1);

    }

    /**
     * Rellena el combo de horas
     *
     * @param horasContratoComboBox
     */
    private void rellenarComboBoxHoras(JComboBox horasContratoComboBox) {

        horasContratoComboBox.removeAllItems();
        for (int i = 40; i > 0; i--) {
            horasContratoComboBox.addItem(i);
        }
        horasContratoComboBox.setSelectedIndex(-1);
    }

    /**
     * Rellena la tabla de contratos de los trabajadores
     *
     * @param dtm
     * @param contrato_trabajador
     */
    public void rellenarTablaContrato(DefaultTableModel dtm, ArrayList<ContratoTrabajador> contrato_trabajador) {

        dtm.setRowCount(0);

        for (ContratoTrabajador contratoTrabajador : contrato_trabajador) {
            dtm.addRow(new String[]{contratoTrabajador.getId() + "", contratoTrabajador.getFecha_inicio(), contratoTrabajador.getFecha_finalizacion(), contratoTrabajador.getFecha_despido()});
        }

    }

    /**
     * Agrega la escucha de eventos a los botones de la ventana
     *
     * @param vistaInicio
     * @param listener
     */
    public void setActionListener(VistaInicio vistaInicio, ActionListener listener) {

        vistaInicio.loginButton.addActionListener(listener);

    }

    /**
     * Agrega la escucha de eventos a los botones de la ventana
     *
     * @param vistaAdmin
     * @param listener
     */
    public void setActionListener(VistaAdmin vistaAdmin, ActionListener listener) {

        vistaAdmin.editarButton.addActionListener(listener);
        vistaAdmin.cancelarButton.addActionListener(listener);
        vistaAdmin.guardarButton.addActionListener(listener);
        vistaAdmin.nuevoButton.addActionListener(listener);
        vistaAdmin.guardarNotarioButton.addActionListener(listener);

        vistaAdmin.nuevoTestigoButton.addActionListener(listener);
        vistaAdmin.nuevoAccionistaButton.addActionListener(listener);
        vistaAdmin.nuevoAportadorButton.addActionListener(listener);
        vistaAdmin.nuevoTrabajadoresButton1.addActionListener(listener);
        vistaAdmin.nuevoContratoTrabajadorButton.addActionListener(listener);

        vistaAdmin.eliminarTestigoButton.addActionListener(listener);
        vistaAdmin.eliminarAportadorButton.addActionListener(listener);
        vistaAdmin.eliminarAccinistaButton.addActionListener(listener);
        vistaAdmin.eliminarTrabajadoresButton.addActionListener(listener);
        vistaAdmin.eliminarContratoTrabajadorButton.addActionListener(listener);

        vistaAdmin.verSalidasButton.addActionListener(listener);
        vistaAdmin.verEntradasButton.addActionListener(listener);

        vistaAdmin.registrarEntradaButton.addActionListener(listener);
        vistaAdmin.registrarSalidaButton.addActionListener(listener);

        vistaAdmin.imprimirDatosDeLaButton.addActionListener(listener);
        vistaAdmin.imprimirListaDeTrabajadoresButton.addActionListener(listener);
        vistaAdmin.logoButton.addActionListener(listener);

    }

    /**
     * Agrega la escucha de eventos a la ventana
     *
     * @param frame
     * @param listener
     */
    public void windowsLstener(JFrame frame, WindowListener listener) {

        frame.addWindowListener(listener);

    }

    /**
     * Controla la edicion de campos de la primera pestaña de VistaAdmin
     *
     * @param vistaAdmin
     * @param b
     */
    public void editarVistaAdminPanel1(VistaAdmin vistaAdmin, boolean b) {

        vistaAdmin.cifEmpresaTextField.setEditable(b);
        vistaAdmin.pagoNotarioEmpresaTextField.setEditable(b);
        vistaAdmin.nombreEmpresaTextField.setEditable(b);
        vistaAdmin.tipoEmpresaTextField.setEditable(b);
        vistaAdmin.capitalEmpresaTextField.setEditable(b);
        vistaAdmin.paisEmpresaTextField.setEditable(b);
        vistaAdmin.ciudadEmpresaTextField.setEditable(b);
        vistaAdmin.calleEmpresaTextField.setEditable(b);

        vistaAdmin.NombreTrabajadorTextField.setEditable(b);
        vistaAdmin.apellido1TrabajadorTextField.setEditable(b);
        vistaAdmin.apellido2TrabajadorTextField.setEditable(b);
        vistaAdmin.dniTrabajadorTextField.setEditable(b);
        vistaAdmin.ssTrabajadortextField.setEditable(b);
        vistaAdmin.nacionalidadTrabajadorTextField.setEditable(b);
        vistaAdmin.horarioTrabajadorTextField.setEditable(b);
        vistaAdmin.telefonoTrabajadorTextField.setEditable(b);
        vistaAdmin.emailTrabajadorTextField.setEditable(b);
        vistaAdmin.paisTrabajadorTextField.setEditable(b);
        vistaAdmin.ciudadTrabajadorTextField.setEditable(b);
        vistaAdmin.calleTrabajadorTextField.setEditable(b);

    }

    /**
     * Guarda los datos de la primera pestaña de la ventana VistaAdmin
     *
     * @param vistaAdmin
     * @param trabajador
     * @param empresa
     */
    public void guardarDatosVistaAdminP1(VistaAdmin vistaAdmin, Trabajador trabajador, Empresa empresa) {

        empresa.setCIF(vistaAdmin.cifEmpresaTextField.getText());
        empresa.setPago_notario(vistaAdmin.pagoNotarioEmpresaTextField.getText());
        empresa.setNombre_empresa(vistaAdmin.nombreEmpresaTextField.getText());
        empresa.setTipo_empresa(vistaAdmin.tipoEmpresaTextField.getText());
        empresa.setCapital_aportado(vistaAdmin.capitalEmpresaTextField.getText());
        empresa.setPais(vistaAdmin.paisEmpresaTextField.getText());
        empresa.setCiudad(vistaAdmin.ciudadEmpresaTextField.getText());
        empresa.setCalle(vistaAdmin.calleEmpresaTextField.getText());
        empresa.setFecha(extraerFecha(vistaAdmin.empresaDatePicker));

        trabajador.setNombre(vistaAdmin.NombreTrabajadorTextField.getText());
        trabajador.setApellido1(vistaAdmin.apellido1TrabajadorTextField.getText());
        trabajador.setApellido2(vistaAdmin.apellido2TrabajadorTextField.getText());
        trabajador.setDni_cif(vistaAdmin.dniTrabajadorTextField.getText());
        trabajador.setNumero_ss(vistaAdmin.ssTrabajadortextField.getText());
        trabajador.setNacionalidad(vistaAdmin.nacionalidadTrabajadorTextField.getText());
        trabajador.setHorario(vistaAdmin.horarioTrabajadorTextField.getText());
        trabajador.setTelefono(vistaAdmin.telefonoTrabajadorTextField.getText());
        trabajador.setEmail(vistaAdmin.emailTrabajadorTextField.getText());
        trabajador.setPais(vistaAdmin.paisTrabajadorTextField.getText());
        trabajador.setCiudad(vistaAdmin.ciudadTrabajadorTextField.getText());
        trabajador.setCalle(vistaAdmin.calleTrabajadorTextField.getText());
        trabajador.setFecha_nacimiento(extraerFecha(vistaAdmin.trabajadorDatePicker));

        if (vistaAdmin.hombreRadioButton.isSelected()) {
            trabajador.setGenero("true");
        } else if (vistaAdmin.hombreRadioButton.isSelected()) {
            trabajador.setGenero("flase");
        } else {
            trabajador.setGenero("null");
        }

    }

    /**
     * Encripta el objeto empresa con el rsa publica del servidor
     *
     * @param empresa
     * @param rsaServer
     * @return
     * @throws NoSuchPaddingException
     * @throws UnsupportedEncodingException
     * @throws NoSuchAlgorithmException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     * @throws NoSuchProviderException
     * @throws InvalidKeyException
     * @throws InvalidKeySpecException
     */
    public Empresa EncriptarEmpresa(Empresa empresa, RSA rsaServer) throws NoSuchPaddingException, UnsupportedEncodingException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, NoSuchProviderException, InvalidKeyException, InvalidKeySpecException {

        Empresa aux = new Empresa();

        aux.setId(empresa.getId());
        aux.setCIF(rsaServer.Encrypt(empresa.getCIF()));
        aux.setTexto(rsaServer.Encrypt(empresa.getTexto()));
        aux.setFecha(rsaServer.Encrypt(empresa.getFecha()));
        aux.setPago_notario(rsaServer.Encrypt(empresa.getPago_notario()));
        aux.setResumen_condiciones(rsaServer.Encrypt(empresa.getResumen_condiciones()));
        aux.setObservaciones(rsaServer.Encrypt(empresa.getObservaciones()));
        aux.setNombre_empresa(rsaServer.Encrypt(empresa.getNombre_empresa()));
        aux.setTipo_empresa(rsaServer.Encrypt(empresa.getTipo_empresa()));
        aux.setCapital_aportado(rsaServer.Encrypt(empresa.getCapital_aportado()));
        aux.setPais(rsaServer.Encrypt(empresa.getPais()));
        aux.setCiudad(rsaServer.Encrypt(empresa.getCiudad()));
        aux.setCalle(rsaServer.Encrypt(empresa.getCalle()));

        if (empresa.getNotario_id() != null) {
            aux.setNotario_id(EncriptarNotario(empresa.getNotario_id(), rsaServer));
        }
        /*
        for (Testigos testigos : empresa.getTestigos()) {
            aux.getTestigos().add(EncriptarTestigos(testigos, rsaServer));
        }

        for (AccionistasParticipadores accionistasParticipadores : empresa.getAccionistas_participadores()) {
            aux.getAccionistas_participadores().add(EncriptarAccionistasParticipadores(accionistasParticipadores, rsaServer));
        }

        for (Locales locale : empresa.getLocales()) {
            aux.getLocales().add(EncriptarLocales(locale, rsaServer));
        }

        for (Negocios negocios : empresa.getNegocio()) {
            aux.getNegocio().add(EncriptarNegocios(negocios, rsaServer));
        }

        for (Papeles papeles : empresa.getPapeles()) {
            aux.getPapeles().add(EncriptarPapeles(papeles, rsaServer));
        }

        for (Aportadores aportadores : empresa.getAportadors()) {
            aux.getAportadors().add(EncriptarAportadores(aportadores, rsaServer));
        }
*/
        return aux;
    }

    /**
     * Encripta el objeto Aportadores con el rsa publico del servidor
     *
     * @param aportadores
     * @param rsaServer
     * @throws NoSuchPaddingException
     * @throws UnsupportedEncodingException
     * @throws NoSuchAlgorithmException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     * @throws NoSuchProviderException
     * @throws InvalidKeyException
     * @throws InvalidKeySpecException
     */
    public Aportadores EncriptarAportadores(Aportadores aportadores, RSA rsaServer, boolean b) throws NoSuchPaddingException, UnsupportedEncodingException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, NoSuchProviderException, InvalidKeyException, InvalidKeySpecException {

        Aportadores aux = new Aportadores();

        aux.setId(aportadores.getId());
        aux.setDNI_CIF(rsaServer.Encrypt(aportadores.getDNI_CIF()));
        aux.setNombre(rsaServer.Encrypt(aportadores.getNombre()));
        aux.setCantidad(rsaServer.Encrypt(aportadores.getCantidad()));
        aux.setMoneda(rsaServer.Encrypt(aportadores.getMoneda()));

        if (b) {
            if (aportadores.getRepresentante_de().getId() != 0 && aportadores.getRepresentante_de().getId() != aportadores.getId()) {
                aux.setRepresentante_de(EncriptarAportadores(aportadores.getRepresentante_de(), rsaServer, false));
            }
        }

        return aux;
    }

    /**
     * Encripta el objeto papeles con el rsa publico del servidor
     *
     * @param papeles
     * @param rsaServer
     * @throws NoSuchPaddingException
     * @throws UnsupportedEncodingException
     * @throws NoSuchAlgorithmException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     * @throws NoSuchProviderException
     * @throws InvalidKeyException
     * @throws InvalidKeySpecException
     */
    public Papeles EncriptarPapeles(Papeles papeles, RSA rsaServer) throws NoSuchPaddingException, UnsupportedEncodingException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, NoSuchProviderException, InvalidKeyException, InvalidKeySpecException {

        Papeles aux = new Papeles();

        aux.setId(papeles.getId());
        aux.setNombre(rsaServer.Encrypt(papeles.getNombre()));
        aux.setNumero_referencia(rsaServer.Encrypt(papeles.getNumero_referencia()));
        aux.setDescripcion(rsaServer.Encrypt(papeles.getDescripcion()));
        aux.setObservaciones(rsaServer.Encrypt(papeles.getObservaciones()));
        aux.setRuta(rsaServer.Encrypt(papeles.getRuta()));
        aux.setFecha_guardado(rsaServer.Encrypt(papeles.getFecha_guardado()));
        aux.setFecha_papeles(rsaServer.Encrypt(papeles.getFecha_papeles()));

        return aux;
    }

    /**
     * Encripta el objeto Negocio con el rsa publico del Servidor
     *
     * @param negocio
     * @param rsaServer
     * @throws NoSuchPaddingException
     * @throws UnsupportedEncodingException
     * @throws NoSuchAlgorithmException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     * @throws NoSuchProviderException
     * @throws InvalidKeyException
     * @throws InvalidKeySpecException
     */
    public Negocios EncriptarNegocios(Negocios negocio, RSA rsaServer) throws NoSuchPaddingException, UnsupportedEncodingException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, NoSuchProviderException, InvalidKeyException, InvalidKeySpecException {

        Negocios aux = new Negocios();

        aux.setId(aux.getId());
        aux.setNombre_comercial(rsaServer.Encrypt(negocio.getNombre_comercial()));
        aux.setTipo(rsaServer.Encrypt(negocio.getTipo()));
        aux.setParticipacion(rsaServer.Encrypt(negocio.getParticipacion()));
        aux.setPais(rsaServer.Encrypt(negocio.getPais()));
        aux.setCiudad(rsaServer.Encrypt(negocio.getCiudad()));
        aux.setCalle(rsaServer.Encrypt(negocio.getCalle()));

        return aux;
    }

    /**
     * Encripta el objeto Locales con el rsa publico del servidor
     *
     * @param locale
     * @param rsaServer
     * @throws NoSuchPaddingException
     * @throws UnsupportedEncodingException
     * @throws NoSuchAlgorithmException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     * @throws NoSuchProviderException
     * @throws InvalidKeyException
     * @throws InvalidKeySpecException
     */
    public Locales EncriptarLocales(Locales locale, RSA rsaServer) throws NoSuchPaddingException, UnsupportedEncodingException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, NoSuchProviderException, InvalidKeyException, InvalidKeySpecException {
        Locales aux = new Locales();

        aux.setId(locale.getId());
        aux.setMetros_cuadrados(rsaServer.Encrypt(locale.getMetros_cuadrados()));
        aux.setObservaciones(rsaServer.Encrypt(locale.getObservaciones()));
        aux.setPais(rsaServer.Encrypt(locale.getPais()));
        aux.setCiudad(rsaServer.Encrypt(locale.getCiudad()));
        aux.setCalle(rsaServer.Encrypt(locale.getCalle()));

        return aux;
    }

    /**
     * Encripta el objeto AccionistasPaticipadores con el rsa publica del servidor
     *
     * @param accionistasParticipadores
     * @param rsaServer
     * @throws NoSuchPaddingException
     * @throws UnsupportedEncodingException
     * @throws NoSuchAlgorithmException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     * @throws NoSuchProviderException
     * @throws InvalidKeyException
     * @throws InvalidKeySpecException
     */
    public AccionistasParticipadores EncriptarAccionistasParticipadores(AccionistasParticipadores accionistasParticipadores, RSA rsaServer) throws NoSuchPaddingException, UnsupportedEncodingException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, NoSuchProviderException, InvalidKeyException, InvalidKeySpecException {

        AccionistasParticipadores aux = new AccionistasParticipadores();

        aux.setId(accionistasParticipadores.getId());
        aux.setDNI_CIF(rsaServer.Encrypt(accionistasParticipadores.getDNI_CIF()));
        aux.setNombre(rsaServer.Encrypt(accionistasParticipadores.getNombre()));
        aux.setPorcetnaje(rsaServer.Encrypt(accionistasParticipadores.getPorcetnaje()));
        aux.setTotales(rsaServer.Encrypt(accionistasParticipadores.getTotales()));
        aux.setTipo_moneda(rsaServer.Encrypt(accionistasParticipadores.getTipo_moneda()));

        return aux;
    }

    /**
     * Encripta el objeto testigos con el rsa publico del servidor
     *
     * @param testigos
     * @param rsaServer
     * @throws NoSuchPaddingException
     * @throws UnsupportedEncodingException
     * @throws NoSuchAlgorithmException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     * @throws NoSuchProviderException
     * @throws InvalidKeyException
     * @throws InvalidKeySpecException
     */
    public Testigos EncriptarTestigos(Testigos testigos, RSA rsaServer) throws NoSuchPaddingException, UnsupportedEncodingException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, NoSuchProviderException, InvalidKeyException, InvalidKeySpecException {

        Testigos aux = new Testigos();

        aux.setId(testigos.getId());
        aux.setDNI_CIF(rsaServer.Encrypt(testigos.getDNI_CIF()));
        aux.setNombre(rsaServer.Encrypt(testigos.getNombre()));

        return aux;

    }

    /**
     * Encripta el objeto notario con el rsa publico del servidor
     *
     * @param notario_id
     * @param rsaServer
     * @throws NoSuchPaddingException
     * @throws UnsupportedEncodingException
     * @throws NoSuchAlgorithmException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     * @throws NoSuchProviderException
     * @throws InvalidKeyException
     * @throws InvalidKeySpecException
     */
    public Notario EncriptarNotario(Notario notario_id, RSA rsaServer) throws NoSuchPaddingException, UnsupportedEncodingException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, NoSuchProviderException, InvalidKeyException, InvalidKeySpecException {

        Notario aux = new Notario();

        aux.setId(notario_id.getId());
        aux.setDNI_CIF(rsaServer.Encrypt(notario_id.getDNI_CIF()));
        aux.setNombre(rsaServer.Encrypt(notario_id.getNombre()));
        aux.setNumero_colegiado(rsaServer.Encrypt(notario_id.getNumero_colegiado()));
        aux.setPais(rsaServer.Encrypt(notario_id.getPais()));
        aux.setCiudad(rsaServer.Encrypt(notario_id.getCiudad()));
        aux.setCalle(rsaServer.Encrypt(notario_id.getCalle()));

        return aux;
    }

    /**
     * Encripta el elemento trabajador
     *
     * @param trabajador
     * @param rsaServer
     * @return
     * @throws NoSuchPaddingException
     * @throws UnsupportedEncodingException
     * @throws NoSuchAlgorithmException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     * @throws NoSuchProviderException
     * @throws InvalidKeyException
     * @throws InvalidKeySpecException
     */
    public Trabajador EncriptarTrabajador(Trabajador trabajador, RSA rsaServer) throws NoSuchPaddingException, UnsupportedEncodingException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, NoSuchProviderException, InvalidKeyException, InvalidKeySpecException {

        Trabajador aux = new Trabajador();

        aux.setId(trabajador.getId());
        aux.setDni_cif(rsaServer.Encrypt(trabajador.getDni_cif()));
        aux.setNumero_ss(rsaServer.Encrypt(trabajador.getNumero_ss()));
        aux.setNombre(rsaServer.Encrypt(trabajador.getNombre()));
        aux.setApellido1(rsaServer.Encrypt(trabajador.getApellido1()));
        aux.setApellido2(rsaServer.Encrypt(trabajador.getApellido2()));
        aux.setFecha_nacimiento(rsaServer.Encrypt(trabajador.getFecha_nacimiento()));
        aux.setGenero(rsaServer.Encrypt(trabajador.getGenero()));
        aux.setEstado(rsaServer.Encrypt(trabajador.getEstado()));
        aux.setNacionalidad(rsaServer.Encrypt(trabajador.getNacionalidad()));
        aux.setHorario(rsaServer.Encrypt(trabajador.getHorario()));
        aux.setTelefono(rsaServer.Encrypt(trabajador.getTelefono()));
        aux.setEmail(rsaServer.Encrypt(trabajador.getEmail()));
        aux.setPais(rsaServer.Encrypt(trabajador.getPais()));
        aux.setCiudad(rsaServer.Encrypt(trabajador.getCiudad()));
        aux.setCalle(rsaServer.Encrypt(trabajador.getCalle()));
        aux.setTipo_usuario(rsaServer.Encrypt(trabajador.getTipo_usuario()));

        return aux;
    }

    /**
     * Agrega los elementos de escucha de eventos a los objetos tipo table
     *
     * @param vistaAdmin
     * @param listener
     */
    public void addTableListener(VistaAdmin vistaAdmin, TableModelListener listener) {

        vistaAdmin.dtmContratoAdmin.addTableModelListener(listener);
        vistaAdmin.dtmAccinistas.addTableModelListener(listener);
        vistaAdmin.dtmTestigos.addTableModelListener(listener);
        vistaAdmin.dtmAportadores.addTableModelListener(listener);
        vistaAdmin.dtmTrabajadores.addTableModelListener(listener);
        vistaAdmin.dtmContratoTrabajadores.addTableModelListener(listener);

    }

    /**
     * Agrega el elemento de escucha de eventos a los objetos
     *
     * @param vistaAdmin
     * @param listener
     */
    public void addMouseListener(VistaAdmin vistaAdmin, MouseListener listener) {

        vistaAdmin.contratosAdminTable.addMouseListener(listener);
        vistaAdmin.trabajadoresTable.addMouseListener(listener);
        vistaAdmin.imgLabelFinal.addMouseListener(listener);

    }

    /**
     * Busca y debuelve el contrato seleccionado, en caso de no existir debuelve null
     *
     * @param contrato_trabajador
     * @param vistaAdmin
     * @param fila
     * @param colum
     * @return
     */
    public ContratoTrabajador selecionarContrato(ArrayList<ContratoTrabajador> contrato_trabajador, VistaAdmin vistaAdmin, int fila, int colum) {

        int idSelect = Integer.parseInt((String) vistaAdmin.contratosAdminTable.getValueAt(fila, colum));

        for (ContratoTrabajador c : contrato_trabajador) {

            if (idSelect == c.getId()) {
                return c;
            }

        }
        return null;
    }

    /**
     * Rellena los objetos del panel del contrato del administrador con el contrato seleccionado
     *
     * @param c
     * @param vistaAdmin
     */
    public void rellenarPanelContrato(ContratoTrabajador c, VistaAdmin vistaAdmin) {

        vistaAdmin.tipoDespidoTextField.setText(c.getTipo_despido());
        vistaAdmin.paisContratoTextField.setText(c.getPais());
        vistaAdmin.ciudadContratoTextField.setText(c.getCiudad());
        vistaAdmin.calleContratoTextField.setText(c.getCalle());
        vistaAdmin.suelodTextField.setText(c.getPago_diario());

        if (c.getRango().equalsIgnoreCase(Controlador.adnimnistrador)) {
            vistaAdmin.rangoComboBox.setSelectedIndex(0);
        } else if (c.getRango().equalsIgnoreCase(Controlador.encargado)) {
            vistaAdmin.rangoComboBox.setSelectedIndex(1);
        } else if (c.getRango().equalsIgnoreCase(Controlador.trabajado)) {
            vistaAdmin.rangoComboBox.setSelectedIndex(2);
        }

        if (!c.getHorario().equalsIgnoreCase("null")) {
            vistaAdmin.horasContratoComboBox.setSelectedIndex(40 - Integer.parseInt(c.getHorario()));
        }

        rellenarDatePicker(vistaAdmin.contratacionDatePicker, c.getFecha_inicio());
        rellenarDatePicker(vistaAdmin.fTeoricaDatePicker, c.getFecha_finalizacion());
        rellenarDatePicker(vistaAdmin.fRealDatePiker, c.getFecha_despido());

    }

    /**
     * Rellena el objeto DatePicker. En caso de recibir null lo deja en blanco
     *
     * @param datePicker
     * @param fecha_inicio
     */
    private void rellenarDatePicker(DatePicker datePicker, String fecha_inicio) {

        if (!fecha_inicio.equalsIgnoreCase("null")) {
            datePicker.setDate(LocalDate.parse(fecha_inicio));
        } else {
            datePicker.clear();
        }

    }

    /**
     * Encripta el objeto contraTrabajador
     *
     * @param selecionarContrato
     * @param rsaServer
     * @return
     * @throws NoSuchPaddingException
     * @throws UnsupportedEncodingException
     * @throws NoSuchAlgorithmException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     * @throws NoSuchProviderException
     * @throws InvalidKeyException
     * @throws InvalidKeySpecException
     */
    public ContratoTrabajador EncriptarContratoTrabajador(ContratoTrabajador selecionarContrato, RSA rsaServer) throws NoSuchPaddingException, UnsupportedEncodingException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, NoSuchProviderException, InvalidKeyException, InvalidKeySpecException {

        ContratoTrabajador aux = new ContratoTrabajador();

        aux.setId(selecionarContrato.getId());
        aux.setHorario(rsaServer.Encrypt(selecionarContrato.getHorario()));
        aux.setRango(rsaServer.Encrypt(selecionarContrato.getRango()));
        aux.setFecha_inicio(rsaServer.Encrypt(selecionarContrato.getFecha_inicio()));
        aux.setFecha_finalizacion(rsaServer.Encrypt(selecionarContrato.getFecha_finalizacion()));
        aux.setFecha_inicio(rsaServer.Encrypt(selecionarContrato.getFecha_inicio()));
        aux.setFecha_despido(rsaServer.Encrypt(selecionarContrato.getFecha_despido()));
        aux.setPago_diario(rsaServer.Encrypt(selecionarContrato.getPago_diario()));
        aux.setTipo_despido(rsaServer.Encrypt(selecionarContrato.getTipo_despido()));
        aux.setPais(rsaServer.Encrypt(selecionarContrato.getPais()));
        aux.setCiudad(rsaServer.Encrypt(selecionarContrato.getCiudad()));
        aux.setCalle(rsaServer.Encrypt(selecionarContrato.getCalle()));

        return aux;

    }

    /**
     * Guarda los datos introducidos por el ususairo en el objeto contrato trabajador seleccionado
     *
     * @param selecionarContrato
     * @param vistaAdmin
     * @return
     */
    public ContratoTrabajador guardarDatosContratoTrabajador(ContratoTrabajador selecionarContrato, VistaAdmin vistaAdmin) {

        if (vistaAdmin.horasContratoComboBox.getSelectedItem() != null) {
            selecionarContrato.setHorario(vistaAdmin.horasContratoComboBox.getSelectedItem().toString());
        }

        if (vistaAdmin.rangoComboBox.getSelectedItem() != null) {
            selecionarContrato.setRango(vistaAdmin.rangoComboBox.getSelectedItem().toString());
        }

        selecionarContrato.setFecha_inicio(extraerFecha(vistaAdmin.contratacionDatePicker));
        selecionarContrato.setFecha_finalizacion(extraerFecha(vistaAdmin.fTeoricaDatePicker));
        selecionarContrato.setPago_diario(vistaAdmin.suelodTextField.getText());
        selecionarContrato.setFecha_despido(extraerFecha(vistaAdmin.fRealDatePiker));
        selecionarContrato.setTipo_despido(vistaAdmin.tipoDespidoTextField.getText());
        selecionarContrato.setPais(vistaAdmin.paisContratoTextField.getText());
        selecionarContrato.setCiudad(vistaAdmin.ciudadContratoTextField.getText());
        selecionarContrato.setCalle(vistaAdmin.calleContratoTextField.getText());

        return selecionarContrato;
    }

    /**
     * Comprueba que los datos introducidos sean de tipo fecha, en caso contrario debuelve una cadena con la palabra null
     *
     * @param datePicker
     * @return
     */
    private String extraerFecha(DatePicker datePicker) {

        String aux = "null";

        try {
            aux = String.valueOf(datePicker.getDate());
        } catch (Exception e) {
            aux = aux;
            e.printStackTrace();
        }

        return aux;

    }

    /**
     * Agrega el elemento de escucha a los comboBox
     *
     * @param vistaAdmin
     * @param listener
     */
    public void addItemSelection(VistaAdmin vistaAdmin, ItemListener listener) {

        vistaAdmin.horasContratoComboBox.addItemListener(listener);
        vistaAdmin.rangoComboBox.addItemListener(listener);

    }

    /**
     * Guarda los datos del objeto notario, indicados por el usuario
     *
     * @param notario_id
     * @param vistaAdmin
     * @return
     */
    public Notario guardarDatosNotario(Notario notario_id, VistaAdmin vistaAdmin) {

        notario_id.setDNI_CIF(vistaAdmin.dniNotarioTextField.getText());
        notario_id.setNumero_colegiado(vistaAdmin.numeroNotariotextField.getText());
        notario_id.setNombre(vistaAdmin.nombreNotariotextField.getText());
        notario_id.setPais(vistaAdmin.paisNotarioTextField.getText());
        notario_id.setCiudad(vistaAdmin.ciudadNotariotextField.getText());
        notario_id.setCalle(vistaAdmin.calleNotariotextField.getText());

        return notario_id;
    }

    /**
     * Guardas los datos del objeto aportadores, indicados por el usuario
     *
     * @param aportadors
     * @param dtm
     * @param fila
     * @return
     */
    public Aportadores guardarDatosAportadores(Aportadores aportadors, DefaultTableModel dtm, int fila) {

        aportadors.setDNI_CIF(String.valueOf(dtm.getValueAt(fila, 1)));
        aportadors.setNombre(String.valueOf(dtm.getValueAt(fila, 2)));
        aportadors.setCantidad(String.valueOf(dtm.getValueAt(fila, 3)));
        aportadors.setMoneda(String.valueOf(dtm.getValueAt(fila, 4)));

        return aportadors;
    }

    /**
     * busca y debuelve le objeto aportadores indicado, en caso de no encontrarlo debuelve null
     *
     * @param aportadors
     * @param id
     * @return
     */
    public Aportadores selecionarAportador(ArrayList<Aportadores> aportadors, int id) {

        for (int i = 0; i < aportadors.size(); i++) {
            if (aportadors.get(i).getId() == id) {
                return aportadors.get(i);
            }
        }
        return null;
    }

    /**
     * busca y debuelve el objeto testigo, en caso de no encontrarlo debuelve null
     *
     * @param testigos
     * @param id
     * @return
     */
    public Testigos selecionarTestigo(ArrayList<Testigos> testigos, int id) {

        for (int i = 0; i < testigos.size(); i++) {
            if (testigos.get(i).getId() == id) {
                return testigos.get(i);
            }
        }
        return null;
    }

    /**
     * Guarda los datos modificados por el ususaio en el objeto testigos
     *
     * @param testigo
     * @param dtm
     * @param fila
     * @return
     */
    public Testigos guardarDatosTestigos(Testigos testigo, DefaultTableModel dtm, int fila) {

        testigo.setDNI_CIF(String.valueOf(dtm.getValueAt(fila, 1)));
        testigo.setNombre(String.valueOf(dtm.getValueAt(fila, 2)));

        return testigo;

    }

    /**
     * Busca y debuelve el objeto accionistasParticipadores, en caso de no existir debuelve null
     *
     * @param accionistas_participadores
     * @param id
     * @return
     */
    public AccionistasParticipadores selecionarAccionistasParticipadores(ArrayList<AccionistasParticipadores> accionistas_participadores, int id) {

        for (int i = 0; i < accionistas_participadores.size(); i++) {
            if (accionistas_participadores.get(i).getId() == id) {
                return accionistas_participadores.get(i);
            }
        }
        return null;

    }

    /**
     * Guarda el los datos en el objeto accionistasParticipadores
     *
     * @param accionistasParticipadores
     * @param dtm
     * @param fila
     * @return
     */
    public AccionistasParticipadores guardarAccionistasParticipadores(AccionistasParticipadores accionistasParticipadores, DefaultTableModel dtm, int fila) {

        accionistasParticipadores.setDNI_CIF(String.valueOf(dtm.getValueAt(fila, 1)));
        accionistasParticipadores.setNombre(String.valueOf(dtm.getValueAt(fila, 2)));
        accionistasParticipadores.setPorcetnaje(String.valueOf(dtm.getValueAt(fila, 3)));
        accionistasParticipadores.setTotales(String.valueOf(dtm.getValueAt(fila, 4)));
        accionistasParticipadores.setTipo_moneda(String.valueOf(dtm.getValueAt(fila, 5)));

        return accionistasParticipadores;
    }

    /**
     * Crea el array list de los trabajadores y lo rellena ocn los datos recibidos
     *
     * @param count
     * @param rsaCliente
     * @param gson
     * @param crypto
     * @param entrada
     * @param passAes
     * @return
     * @throws IOException
     * @throws IllegalBlockSizeException
     * @throws NoSuchPaddingException
     * @throws BadPaddingException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     */
    public ArrayList<Trabajador> recibirTrabajador(int count, RSA rsaCliente, Gson gson, Crypto crypto, DataInputStream entrada, String passAes) throws IOException, IllegalBlockSizeException, NoSuchPaddingException, BadPaddingException, NoSuchAlgorithmException, InvalidKeyException {

        ArrayList<Trabajador> trabajadors = new ArrayList<>();

        for (int i = 0; i < count; i++) {
            trabajadors.add(this.descrifrarTrabajador(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Trabajador.class), rsaCliente, gson, crypto, entrada, passAes, true));
        }

        return trabajadors;
    }

    /**
     * Busca y debuelve el trabajador seleccionado, en caso de no estar debuelve null
     *
     * @param trabajadors
     * @param id
     * @return
     */
    public Trabajador selecionarTrabajador(ArrayList<Trabajador> trabajadors, int id) {

        for (int i = 0; i < trabajadors.size(); i++) {
            if (trabajadors.get(i).getId() == id) {
                return trabajadors.get(i);
            }
        }
        return null;
    }

    /**
     * Guarda los datos del trabajador
     *
     * @param trabajador
     * @param dtm
     * @param fila
     * @param trabajadorAdmin
     * @return
     */
    public Trabajador guardarDatosTrabajador(Trabajador trabajador, DefaultTableModel dtm, int fila, Trabajador trabajadorAdmin) {


        trabajador.setDni_cif(String.valueOf(dtm.getValueAt(fila, 1)));
        trabajador.setNumero_ss(String.valueOf(dtm.getValueAt(fila, 2)));
        trabajador.setNombre(String.valueOf(dtm.getValueAt(fila, 3)));
        trabajador.setApellido1(String.valueOf(dtm.getValueAt(fila, 4)));
        trabajador.setApellido2(String.valueOf(dtm.getValueAt(fila, 5)));
        trabajador.setFecha_nacimiento(String.valueOf(dtm.getValueAt(fila, 6)));

        if (String.valueOf(dtm.getValueAt(fila, 7)).equalsIgnoreCase("Hombre")) {
            trabajador.setGenero("true");
        } else if (String.valueOf(dtm.getValueAt(fila, 7)).equalsIgnoreCase("Mujer")) {
            trabajador.setGenero("false");
        } else {
            trabajador.setGenero("null");
        }

        trabajador.setEstado(String.valueOf(dtm.getValueAt(fila, 8)));
        trabajador.setNacionalidad(String.valueOf(dtm.getValueAt(fila, 9)));
        trabajador.setHorario(String.valueOf(dtm.getValueAt(fila, 10)));
        trabajador.setTelefono(String.valueOf(dtm.getValueAt(fila, 11)));
        trabajador.setEmail(String.valueOf(dtm.getValueAt(fila, 12)));
        trabajador.setPais(String.valueOf(dtm.getValueAt(fila, 13)));
        trabajador.setCiudad(String.valueOf(dtm.getValueAt(fila, 14)));
        trabajador.setCalle(String.valueOf(dtm.getValueAt(fila, 15)));
        trabajador.setTipo_usuario(String.valueOf(dtm.getValueAt(fila, 16)));

        if (trabajador.getId() == trabajadorAdmin.getId()) {

            trabajadorAdmin.setDni_cif(String.valueOf(dtm.getValueAt(fila, 1)));
            trabajadorAdmin.setNumero_ss(String.valueOf(dtm.getValueAt(fila, 2)));
            trabajadorAdmin.setNombre(String.valueOf(dtm.getValueAt(fila, 3)));
            trabajadorAdmin.setApellido1(String.valueOf(dtm.getValueAt(fila, 4)));
            trabajadorAdmin.setApellido2(String.valueOf(dtm.getValueAt(fila, 5)));
            trabajadorAdmin.setFecha_nacimiento(String.valueOf(dtm.getValueAt(fila, 6)));
            trabajadorAdmin.setGenero(String.valueOf(dtm.getValueAt(fila, 7)));

            if (String.valueOf(dtm.getValueAt(fila, 7)).equalsIgnoreCase("Hombre")) {
                trabajador.setGenero("true");
            } else if (String.valueOf(dtm.getValueAt(fila, 7)).equalsIgnoreCase("Mujer")) {
                trabajador.setGenero("false");
            } else {
                trabajador.setGenero("null");
            }

            trabajadorAdmin.setNacionalidad(String.valueOf(dtm.getValueAt(fila, 9)));
            trabajadorAdmin.setHorario(String.valueOf(dtm.getValueAt(fila, 10)));
            trabajadorAdmin.setTelefono(String.valueOf(dtm.getValueAt(fila, 11)));
            trabajadorAdmin.setEmail(String.valueOf(dtm.getValueAt(fila, 12)));
            trabajadorAdmin.setPais(String.valueOf(dtm.getValueAt(fila, 13)));
            trabajadorAdmin.setCiudad(String.valueOf(dtm.getValueAt(fila, 14)));
            trabajadorAdmin.setCalle(String.valueOf(dtm.getValueAt(fila, 15)));
            trabajadorAdmin.setTipo_usuario(String.valueOf(dtm.getValueAt(fila, 16)));

        }

        return trabajador;
    }

    /**
     * Rellena la tabla Contrato trabajador
     *
     * @param selecionarTrabajador
     * @param dtm
     * @param contratosTrabajadorTable
     */
    public void rellenarTablaContratoTrabajador(Trabajador selecionarTrabajador, DefaultTableModel dtm, JTable contratosTrabajadorTable) {

        dtm.setRowCount(0);

        for (int i = 0; i < selecionarTrabajador.getContrato_trabajador().size(); i++) {

            TableColumn sportColumn = contratosTrabajadorTable.getColumnModel().getColumn(1);
            JComboBox comboBox = new JComboBox();
            this.rellenarComboBoxHoras(comboBox);
            sportColumn.setCellEditor(new DefaultCellEditor(comboBox));


            // Create our cell editor
            DefaultCellEditor datePickerCellEditorFI = new DatePickerTableEditor();
            // Set the number of mouse clicks needed to activate it.
            datePickerCellEditorFI.setClickCountToStart(1);
            // Set it for the appropriate table column.
            contratosTrabajadorTable.getColumnModel().getColumn(3).setCellEditor(datePickerCellEditorFI);

            // Create our cell editor
            DefaultCellEditor datePickerCellEditorFF = new DatePickerTableEditor();
            // Set the number of mouse clicks needed to activate it.
            datePickerCellEditorFF.setClickCountToStart(1);
            // Set it for the appropriate table column.
            contratosTrabajadorTable.getColumnModel().getColumn(4).setCellEditor(datePickerCellEditorFF);

            // Create our cell editor
            DefaultCellEditor datePickerCellEditorFD = new DatePickerTableEditor();
            // Set the number of mouse clicks needed to activate it.
            datePickerCellEditorFD.setClickCountToStart(1);
            // Set it for the appropriate table column.
            contratosTrabajadorTable.getColumnModel().getColumn(6).setCellEditor(datePickerCellEditorFD);


            dtm.addRow(new Object[]{selecionarTrabajador.getContrato_trabajador().get(i).getId() + "",
                    selecionarTrabajador.getContrato_trabajador().get(i).getHorario(),
                    selecionarTrabajador.getContrato_trabajador().get(i).getRango(),
                    selecionarTrabajador.getContrato_trabajador().get(i).getFecha_inicio(),
                    selecionarTrabajador.getContrato_trabajador().get(i).getFecha_finalizacion(),
                    selecionarTrabajador.getContrato_trabajador().get(i).getPago_diario(),
                    selecionarTrabajador.getContrato_trabajador().get(i).getFecha_despido(),
                    selecionarTrabajador.getContrato_trabajador().get(i).getTipo_despido(),
                    selecionarTrabajador.getContrato_trabajador().get(i).getPais(),
                    selecionarTrabajador.getContrato_trabajador().get(i).getCiudad(),
                    selecionarTrabajador.getContrato_trabajador().get(i).getCalle()});

        }

    }

    /**
     * Debuelve el trabajador seleccionado, en caso de no existir debuelve null
     *
     * @param trabajadors
     * @param id
     * @return
     */
    public ContratoTrabajador selecionarContratoTrabajo(ArrayList<Trabajador> trabajadors, int id) {

        for (int i = 0; i < trabajadors.size(); i++) {
            for (int o = 0; o < trabajadors.get(i).getContrato_trabajador().size(); o++) {
                if (trabajadors.get(i).getContrato_trabajador().get(o).getId() == id) {
                    return trabajadors.get(i).getContrato_trabajador().get(o);
                }
            }
        }
        return null;

    }

    /**
     * Guarda los datos del trabajador
     *
     * @param contratoTrabajo
     * @param dtm
     * @param fila
     * @param trabajador
     * @return
     */
    public ContratoTrabajador guardarDatosContratoTrabajador(ContratoTrabajador contratoTrabajo, DefaultTableModel dtm, int fila, Trabajador trabajador) {

        contratoTrabajo.setHorario(String.valueOf(dtm.getValueAt(fila, 1)));
        contratoTrabajo.setRango(String.valueOf(dtm.getValueAt(fila, 2)));
        contratoTrabajo.setFecha_inicio(String.valueOf(dtm.getValueAt(fila, 3)));
        contratoTrabajo.setFecha_finalizacion(String.valueOf(dtm.getValueAt(fila, 4)));
        contratoTrabajo.setPago_diario(String.valueOf(dtm.getValueAt(fila, 5)));
        contratoTrabajo.setFecha_despido(String.valueOf(dtm.getValueAt(fila, 6)));
        contratoTrabajo.setTipo_despido(String.valueOf(dtm.getValueAt(fila, 7)));
        contratoTrabajo.setPais(String.valueOf(dtm.getValueAt(fila, 8)));
        contratoTrabajo.setCiudad(String.valueOf(dtm.getValueAt(fila, 9)));
        contratoTrabajo.setCalle(String.valueOf(dtm.getValueAt(fila, 10)));

        for (int i = 0; i < trabajador.getContrato_trabajador().size(); i++) {
            if (trabajador.getContrato_trabajador().get(i).getId() == contratoTrabajo.getId()) {

                trabajador.getContrato_trabajador().get(i).setHorario(String.valueOf(dtm.getValueAt(fila, 1)));
                trabajador.getContrato_trabajador().get(i).setRango(String.valueOf(dtm.getValueAt(fila, 2)));
                trabajador.getContrato_trabajador().get(i).setFecha_inicio(String.valueOf(dtm.getValueAt(fila, 3)));
                trabajador.getContrato_trabajador().get(i).setFecha_finalizacion(String.valueOf(dtm.getValueAt(fila, 4)));
                trabajador.getContrato_trabajador().get(i).setPago_diario(String.valueOf(dtm.getValueAt(fila, 5)));
                trabajador.getContrato_trabajador().get(i).setFecha_despido(String.valueOf(dtm.getValueAt(fila, 6)));
                trabajador.getContrato_trabajador().get(i).setTipo_despido(String.valueOf(dtm.getValueAt(fila, 7)));
                trabajador.getContrato_trabajador().get(i).setPais(String.valueOf(dtm.getValueAt(fila, 8)));
                trabajador.getContrato_trabajador().get(i).setCiudad(String.valueOf(dtm.getValueAt(fila, 9)));
                trabajador.getContrato_trabajador().get(i).setCalle(String.valueOf(dtm.getValueAt(fila, 10)));
                break;
            }
        }

        return contratoTrabajo;

    }

    /**
     * Agrega los elemntos de escucha a los botones
     *
     * @param vistaAuxiliar
     * @param listener
     */
    public void setActionListener(VistaAuxiliar vistaAuxiliar, ActionListener listener) {

        vistaAuxiliar.eliminarButton.addActionListener(listener);

    }

    /**
     * Agrega los elementos de escucha la tabla
     *
     * @param vistaAuxiliar
     * @param listener
     */
    public void addTableListener(VistaAuxiliar vistaAuxiliar, TableModelListener listener) {

        vistaAuxiliar.dtm.addTableModelListener(listener);

    }

    /**
     * Rellena la tabla de la ventana vista Auxiliar
     *
     * @param vistaAuxiliar
     * @param entradas
     */
    public void rellenarTablaAuxiliar(VistaAuxiliar vistaAuxiliar, ArrayList<Entradas> entradas) {

        vistaAuxiliar.dtm.setRowCount(0);

        for (Entradas e : entradas) {
            vistaAuxiliar.dtm.addRow(new Object[]{e.getId(), e.getFecha(), e.getSalida()});
        }

    }

    /**
     * Rellena la tabla de la ventana vista Auxiliar
     *
     * @param vistaAuxiliar
     * @param salidas
     * @param b
     */
    public void rellenarTablaAuxiliar(VistaAuxiliar vistaAuxiliar, ArrayList<Salidas> salidas, boolean b) {

        vistaAuxiliar.dtm.setRowCount(0);

        for (Salidas e : salidas) {
            vistaAuxiliar.dtm.addRow(new Object[]{e.getId(), e.getFecha(), e.getSalida()});
        }

    }

    /**
     * Elimina el objeto trabajadors seleccionado
     *
     * @param trabajadors
     * @param opc
     */
    public void eliminarTrabajador(ArrayList<Trabajador> trabajadors, int opc) {


        for (int i = 0; i < trabajadors.size(); i++) {
            if (trabajadors.get(i).getId() == opc) {
                trabajadors.remove(i);
            }
        }
    }

    /**
     * Envia la imagen seleccionado al servidor para que la guarde
     *
     * @param logo
     * @param password
     * @param socket
     * @throws IOException
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     * @throws NoSuchProviderException
     * @throws InvalidKeyException
     * @throws InvalidKeySpecException
     */
    public void enviarImagen(File logo, String password, Socket socket) throws IOException, NoSuchPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, NoSuchProviderException, InvalidKeyException, InvalidKeySpecException {

        byte[] mybytearray = new byte[(int) logo.length()];
        BufferedInputStream bis = new BufferedInputStream(new FileInputStream(logo));
        bis.read(mybytearray, 0, mybytearray.length);
        Socket s2 = new Socket(socket.getInetAddress(),(socket.getPort()+1));
        OutputStream os = s2.getOutputStream();
        os.write(mybytearray, 0, mybytearray.length);

        os.flush();
        os.close();
        bis.close();
        s2.close();


      /*
      // lee el archivo que se dea enviar
        InputStream is = new BufferedInputStream(new FileInputStream(logo));
      // crea un cifrado
        Cipher cipher = Cipher.getInstance("DESede/ECB/PKCS7Padding");
        cipher.init(Cipher.ENCRYPT_MODE, new Crypto().crearClave(password));
      // Crea un archivo auxilar con la información cifrada
        CipherOutputStream out = new CipherOutputStream(new FileOutputStream(new File("logoAux.png")), cipher);
      // rellena el archivo auxiliar
        byte[] buffer = new byte[8192];
        int count;
        while ((count = is.read(buffer)) > 0) {
            out.write(buffer, 0, count);
        }
      */

    }

    /**
     * Guarda el objeto en la ubicacion por defecto con el nombre por defecto
     *
     * @param logo
     */
    public void guardarLogo(File logo) {

        File destino = new File("img/logo.png");

        try {
            InputStream in = new FileInputStream(logo);
            OutputStream out = new FileOutputStream(destino);

            byte[] buf = new byte[1024];
            int len;

            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }

            in.close();
            out.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    /**
     * Envia el id del objeto papeles seleccionado
     *
     * @param papeles
     * @param salida
     * @param rsaServer
     * @param crypto
     * @param passAes
     * @param gson
     * @throws NoSuchPaddingException
     * @throws IOException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     */
    public void enviarIdPapelesLogo(ArrayList<Papeles> papeles, DataOutputStream salida, RSA rsaServer, Crypto crypto, String passAes, Gson gson) throws NoSuchPaddingException, IOException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, InvalidKeyException {

        int id = 0;

        for (Papeles p : papeles) {
            if (p.getNombre().equalsIgnoreCase("logo")) {
                id = p.getId();
                break;
            }
        }
        salida.writeUTF(crypto.encriptar(gson.toJson(id), passAes));
    }

    /**
     * Agrega los elementos de a los botones de vistaEncargado
     *
     * @param vistaEncargado
     * @param listener
     */
    public void setActionListener(VistaEncargado vistaEncargado, ActionListener listener) {
        vistaEncargado.entradaButton.addActionListener(listener);
        vistaEncargado.salidaButton.addActionListener(listener);
    }

    /**
     * Agrega los elementos de escucha de eventos a las tablas
     *
     * @param vistaEncargado
     * @param listener
     */
    public void addTableListener(VistaEncargado vistaEncargado, TableModelListener listener) {
        vistaEncargado.dtmTrabajador.addTableModelListener(listener);
        vistaEncargado.dtmContratoTrabajadoresTrabajador.addTableModelListener(listener);
    }

    /**
     * Rellena los elementos de la ventana vistaEncargado
     *
     * @param vistaEncargado
     * @param trabajadors
     */
    public void rellenarVistaEncargado(VistaEncargado vistaEncargado, ArrayList<Trabajador> trabajadors) {

        rellenarTablaTrabajadores(vistaEncargado.dtmTrabajador, trabajadors, vistaEncargado.encargdorTable);

    }

    /**
     * Rellena la tabla encargdorTable
     *
     * @param dtm
     * @param trabajadors
     * @param encargdorTable
     */
    private void rellenarTablaTrabajadores(DefaultTableModel dtm, ArrayList<Trabajador> trabajadors, JTable encargdorTable) {

        dtm.setRowCount(0);
        for (Trabajador aux : trabajadors) {
            TableColumn sexColumn = encargdorTable.getColumnModel().getColumn(7);
            JComboBox comboBox1 = new JComboBox();
            comboBox1.addItem("Hombre");
            comboBox1.addItem("Mujer");
            comboBox1.addItem("ND");
            sexColumn.setCellEditor(new DefaultCellEditor(comboBox1));

            String genero = "";

            if (aux.getGenero().equalsIgnoreCase("True")) {
                genero = "Hombre";
            } else if (aux.getGenero().equalsIgnoreCase("False")) {
                genero = "Mujer";
            } else {
                genero = "ND";
            }

            TableColumn userTypeColumn = encargdorTable.getColumnModel().getColumn(16);
            JComboBox comboBox = new JComboBox();
            comboBox.addItem(Controlador.adnimnistrador);
            comboBox.addItem(Controlador.encargado);
            comboBox.addItem(Controlador.trabajado);
            userTypeColumn.setCellEditor(new DefaultCellEditor(comboBox));

            String tipoUsuario = "";

            if (aux.getTipo_usuario().equalsIgnoreCase(Controlador.adnimnistrador)) {
                tipoUsuario = Controlador.adnimnistrador;
            } else if (aux.getTipo_usuario().equalsIgnoreCase(Controlador.encargado)) {
                tipoUsuario = Controlador.encargado;
            } else if (aux.getTipo_usuario().equalsIgnoreCase(Controlador.trabajado)) {
                tipoUsuario = Controlador.trabajado;
            } else {
                tipoUsuario = userTypeColumn.getHeaderValue().toString();
            }

            // Create our cell editor
            DefaultCellEditor datePickerCellEditor = new DatePickerTableEditor();
            // Set the number of mouse clicks needed to activate it.
            datePickerCellEditor.setClickCountToStart(2);
            // Set it for the appropriate table column.
            encargdorTable.getColumnModel().getColumn(6).setCellEditor(datePickerCellEditor);

            dtm.addRow(new Object[]{aux.getId() + "", aux.getDni_cif(), aux.getNumero_ss(), aux.getNombre(), aux.getApellido1(), aux.getApellido2(), aux.getFecha_nacimiento(), genero, aux.getEstado(), aux.getNacionalidad(), aux.getHorario(), aux.getTelefono(), aux.getEmail(), aux.getPais(), aux.getCiudad(), aux.getCalle(), tipoUsuario});

        }
    }

    /**
     * Agrega los elementos de escucha de eventos del raton a los elementos
     *
     * @param vistaEncargado
     * @param listener
     */
    public void setMouseListener(VistaEncargado vistaEncargado, MouseListener listener) {

        vistaEncargado.encargdorTable.addMouseListener(listener);

    }

    /**
     * Rellena los elementos visuales de la ventana
     *
     * @param vistaTrabajador
     * @param trabajador
     */
    public void rellenarVistaTrabajador(VistaTrabajador vistaTrabajador, Trabajador trabajador) {

        vistaTrabajador.nombreTextField.setText(trabajador.getNombre());
        vistaTrabajador.apellido1TextField.setText(trabajador.getApellido1());
        vistaTrabajador.apellido2TextField.setText(trabajador.getApellido2());

    }

    /**
     * Agrega las escuchas de eventos a los botones
     *
     * @param vistaTrabajador
     * @param listener
     */
    public void setActionListener(VistaTrabajador vistaTrabajador, ActionListener listener) {

        vistaTrabajador.entrarButton.addActionListener(listener);
        vistaTrabajador.salidaButton.addActionListener(listener);

    }
}