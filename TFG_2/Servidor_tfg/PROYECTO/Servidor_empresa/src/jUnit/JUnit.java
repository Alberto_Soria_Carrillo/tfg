package jUnit;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import conexion.Conexion;

class JUnit {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}
	
	@Test
    void conectar() {
        Conexion conexion = new Conexion(true, "localhost", "3306", "empresa", "root", "Montessori2020");

        conexion.conectar();

        assertTrue(conexion != null);

        if (conexion != null) {
            conexion.desconectar();
        }

    }

    @Test
    void desconectar() throws SQLException {

    	Conexion conexion = new Conexion(true, "localhost", "3306", "empresa", "root", "Montessori2020");

        conexion.conectar();

        assertTrue(conexion != null);

        if (conexion != null) {
            conexion.desconectar();

        }
        try {
            assertTrue(conexion.getConnection().isClosed());
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Test
    void buscarPS() throws SQLException {

        try {
        	Conexion conexion = new Conexion(true, "localhost", "3306", "empresa", "root", "Montessori2020");

            conexion.conectar();

            String query = "select * from trabajadores";

            ResultSet resultSet = conexion.buscarPS(query);


            assertTrue(resultSet!=null);

            if (conexion != null) {
                conexion.desconectar();

            }

            assertTrue(conexion.getConnection().isClosed());
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }



    @Test
    void insertarPS() {

        try {
        	Conexion conexion = new Conexion(true, "localhost", "3306", "empresa", "root", "Montessori2020");

            conexion.conectar();

            String query = "insert into name_pass_user (nombre, pass)\n" +
                    "values ('Junit', 'Junit')";

            conexion.insertarPS(query);

            query = "select * from name_pass_user where nombre = 'Junit' and pass = 'Junit'";

            ResultSet resultSet = conexion.buscarPS(query);


            assertTrue(resultSet!=null);

            if (conexion != null) {
                conexion.desconectar();

            }

            assertTrue(conexion.getConnection().isClosed());
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Test
    void deletPS() {

        try {
        	Conexion conexion = new Conexion(true, "localhost", "3306", "empresa", "root", "Montessori2020");

            conexion.conectar();

            String query = "delete from name_pass_user where nombre = 'Junit' and pass = 'Junit'";

            conexion.deletPS(query);

            query = "select * from name_pass_user where nombre = 'Junit' and pass = 'Junit'";

            ResultSet resultSet = conexion.buscarPS(query);

            int count = -1;

            while (resultSet.next()){
                count++;
            }

            assertTrue(count == -1);

            if (conexion != null) {
                conexion.desconectar();

            }

            assertTrue(conexion.getConnection().isClosed());
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

}
