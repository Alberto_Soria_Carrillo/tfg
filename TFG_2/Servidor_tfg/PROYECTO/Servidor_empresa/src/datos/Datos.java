package datos;

/**
 * Clase Datos, guarda la información necesaria para el programa
 * 
 * @author Alberto Soria Carrillo
 *
 */
public class Datos {
	
	
	// Atributos
	public String user; // nombre del usuario de la BBDD
	public String pass; // contraseña del usuario de la BBDD
	
	public static final String adnimnistrador = "Administrador";
	public static final String encargado = "Encargado";
	public static final String trabajador = "Trabajador";

}
