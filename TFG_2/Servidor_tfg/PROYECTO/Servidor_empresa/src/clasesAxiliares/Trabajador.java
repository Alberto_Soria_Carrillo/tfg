package clasesAxiliares;

import java.util.ArrayList;

/**
 * Clase Trabajador. Guarda la estructura del objeto Trabajador de la BBDD
 * @author Alberto Soria Carrillo
 *
 */
public class Trabajador {
	// Atributos
	private int id;
	private String dni_cif;
	private String numero_ss;
	private String nombre;
	private String apellido1;
	private String apellido2;
	private String fecha_nacimiento;
	private String genero;
	private String estado;
	private String nacionalidad;
	private String horario;
	private String telefono;
	private String email;
	private String pais;
	private String ciudad;
	private String calle;
	private String tipo_usuario;
	private int user_pass;

	private Trabajador resposable;
	
	private ArrayList<ContratoTrabajador> contrato_trabajador;
	private ArrayList<Salidas> salida;
	private ArrayList<Entradas> entrada;
	private ArrayList<Bajas> baja;
	private ArrayList<AdelantoTabajador> adelanto_trabajador;

	// Contructor
	public Trabajador() {
	}

	public Trabajador(int id, String dni_cif, String numero_ss, String nombre, String apellido1, String apellido2,
			String fecha_nacimiento, String genero, String estado, String nacionalidad, String horario, String telefono,
			String email, String pais, String ciudad, String calle, String tipo_usuario, Trabajador resposable,
			int user_pass) {
		super();
		this.id = id;
		this.dni_cif = dni_cif;
		this.numero_ss = numero_ss;
		this.nombre = nombre;
		this.apellido1 = apellido1;
		this.apellido2 = apellido2;
		this.fecha_nacimiento = fecha_nacimiento;
		this.genero = genero;
		this.estado = estado;
		this.nacionalidad = nacionalidad;
		this.horario = horario;
		this.telefono = telefono;
		this.email = email;
		this.pais = pais;
		this.ciudad = ciudad;
		this.calle = calle;
		this.tipo_usuario = tipo_usuario;
		this.resposable = resposable;
		this.user_pass = user_pass;

	}

	public Trabajador(int id, String dni_cif, String numero_ss, String nombre, String apellido1, String apellido2,
			String fecha_nacimiento, String genero, String estado, String nacionalidad, String horario, String telefono,
			String email, String pais, String ciudad, String calle, String tipo_usuario, Trabajador resposable) {
		super();
		this.id = id;
		this.dni_cif = dni_cif;
		this.numero_ss = numero_ss;
		this.nombre = nombre;
		this.apellido1 = apellido1;
		this.apellido2 = apellido2;
		this.fecha_nacimiento = fecha_nacimiento;
		this.genero = genero;
		this.estado = estado;
		this.nacionalidad = nacionalidad;
		this.horario = horario;
		this.telefono = telefono;
		this.email = email;
		this.pais = pais;
		this.ciudad = ciudad;
		this.calle = calle;
		this.tipo_usuario = tipo_usuario;
		this.resposable = resposable;
		this.user_pass = 0;

	}

	// Getters and Setters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDni_cif() {
		return dni_cif;
	}

	public void setDni_cif(String dni_cif) {
		this.dni_cif = dni_cif;
	}

	public String getNumero_ss() {
		return numero_ss;
	}

	public void setNumero_ss(String numero_ss) {
		this.numero_ss = numero_ss;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido1() {
		return apellido1;
	}

	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}

	public String getApellido2() {
		return apellido2;
	}

	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}

	public String getFecha_nacimiento() {
		return fecha_nacimiento;
	}

	public void setFecha_nacimiento(String fecha_nacimiento) {
		this.fecha_nacimiento = fecha_nacimiento;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getNacionalidad() {
		return nacionalidad;
	}

	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}

	public String getHorario() {
		return horario;
	}

	public void setHorario(String horario) {
		this.horario = horario;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getTipo_usuario() {
		return tipo_usuario;
	}

	public void setTipo_usuario(String tipo_usuario) {
		this.tipo_usuario = tipo_usuario;
	}

	public Trabajador getResposable() {
		return resposable;
	}

	public void setResposable(Trabajador resposable) {
		this.resposable = resposable;
	}

	public int getUser_pass() {
		return user_pass;
	}

	public void setUser_pass(int user_pass) {
		this.user_pass = user_pass;
	}

	public ArrayList<Salidas> getSalida() {
		return salida;
	}

	public void setSalida(ArrayList<Salidas> salida) {
		this.salida = salida;
	}

	public ArrayList<Entradas> getEntrada() {
		return entrada;
	}

	public void setEntrada(ArrayList<Entradas> entrada) {
		this.entrada = entrada;
	}

	public ArrayList<Bajas> getBaja() {
		return baja;
	}

	public void setBaja(ArrayList<Bajas> baja) {
		this.baja = baja;
	}

	public ArrayList<AdelantoTabajador> getAdelanto_trabajador() {
		return adelanto_trabajador;
	}

	public void setAdelanto_trabajador(ArrayList<AdelantoTabajador> adelanto_trabajador) {
		this.adelanto_trabajador = adelanto_trabajador;
	}

	public ArrayList<ContratoTrabajador> getContrato_trabajador() {
		return contrato_trabajador;
	}

	public void setContrato_trabajador(ArrayList<ContratoTrabajador> contrato_trabajador) {
		this.contrato_trabajador = contrato_trabajador;
	}

	@Override
	public String toString() {
		return "Trabajador{" +
				"\nid=" + id +
				"\n, dni_cif='" + dni_cif + '\'' +
				"\n, numero_ss='" + numero_ss + '\'' +
				"\n, nombre='" + nombre + '\'' +
				"\n, apellido1='" + apellido1 + '\'' +
				"\n, apellido2='" + apellido2 + '\'' +
				"\n, fecha_nacimiento='" + fecha_nacimiento + '\'' +
				"\n, genero='" + genero + '\'' +
				"\n, estado='" + estado + '\'' +
				"\n, nacionalidad='" + nacionalidad + '\'' +
				"\n, horario='" + horario + '\'' +
				"\n, telefono='" + telefono + '\'' +
				"\n, email='" + email + '\'' +
				"\n, pais='" + pais + '\'' +
				"\n, ciudad='" + ciudad + '\'' +
				"\n, calle='" + calle + '\'' +
				"\n, tipo_usuario='" + tipo_usuario + '\'' +
				"\n, user_pass=" + user_pass +
				"\n, resposable=" + resposable +
				"\n, contrato_trabajador=" + contrato_trabajador +
				"\n, salida=" + salida +
				"\n, entrada=" + entrada +
				"\n, baja=" + baja +
				"\n, adelanto_trabajador=" + adelanto_trabajador +
				'}';
	}
	
}
