package clasesAxiliares;

/**
 * Clase Locales. Guarda la estructura del objeto Locales de la BBDD
 * @author Alberto Soria Carrillo
 *
 */
public class Locales {

	// Atributos
	private int id;
	private String metros_cuadrados;
	private String observaciones;
	private String pais;
	private String ciudad;
	private String calle;
	
	// Constructor
	public Locales(int id, String metros_cuadrados, String observaciones, String pais, String ciudad, String calle) {
		super();
		this.id = id;
		this.metros_cuadrados = metros_cuadrados;
		this.observaciones = observaciones;
		this.pais = pais;
		this.ciudad = ciudad;
		this.calle = calle;
	}
	
	public Locales() {}

	// Getters and Setters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMetros_cuadrados() {
		return metros_cuadrados;
	}

	public void setMetros_cuadrados(String metros_cuadrados) {
		this.metros_cuadrados = metros_cuadrados;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}
	
	
}
