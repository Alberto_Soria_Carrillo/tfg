package clasesAxiliares;

/**
 * Clase Testigos. Guarda la estructura del objeto Testigos de la BBDD
 * @author Alberto Soria Carrillo
 *
 */
public class Testigos {
	
	// Atributo
	private String DNI_CIF;
	private int id;
	private String nombre;
	private Empresa empresa_id;
	
	// Constructor
	public Testigos(String dNI_CIF, int id, String nombre, Empresa empresa_id) {
		super();
		DNI_CIF = dNI_CIF;
		this.id = id;
		this.nombre = nombre;
		this.empresa_id = empresa_id;
	}

	public Testigos() {}

	// Getters and Setters
	public String getDNI_CIF() {
		return DNI_CIF;
	}

	public void setDNI_CIF(String dNI_CIF) {
		DNI_CIF = dNI_CIF;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Empresa getEmpresa_id() {
		return empresa_id;
	}

	public void setEmpresa_id(Empresa empresa_id) {
		this.empresa_id = empresa_id;
	}
	
	
	
	
}
