package clasesAxiliares;

/**
 * Clase Negocios. Guarda la estructura del objeto Negocios de la BBDD
 * @author Alberto Soria Carrillo
 *
 */
public class Negocios {
	
	// Atributos
	private int id;
	private String nombre_comercial;
	private String tipo;
	private String participacion;
	private String pais;
	private String ciudad;
	private String calle;
	
	// Constructor
	public Negocios(int id, String nombre_comercial, String tipo, String participacion, String pais, String ciudad,
			String calle) {
		super();
		this.id = id;
		this.nombre_comercial = nombre_comercial;
		this.tipo = tipo;
		this.participacion = participacion;
		this.pais = pais;
		this.ciudad = ciudad;
		this.calle = calle;
	}
	
	public Negocios() {}

	// Getters and Setters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre_comercial() {
		return nombre_comercial;
	}

	public void setNombre_comercial(String nombre_comercial) {
		this.nombre_comercial = nombre_comercial;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getParticipacion() {
		return participacion;
	}

	public void setParticipacion(String participacion) {
		this.participacion = participacion;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}
	
	

}
