package clasesAxiliares;

import java.util.ArrayList;

/**
 * Clase Empresa. Guarda la estructura del objeto Empresa de la BBDD
 * @author Alberto Soria Carrillo
 *
 */
public class Empresa {

	// Atributos
	private String CIF;
	private int id;
	private String texto;
	private String fecha;
	private String pago_notario;
	private String resumen_condiciones;
	private String observaciones;
	private String nombre_empresa;
	private String tipo_empresa;
	private String capital_aportado;
	private String pais;
	private String ciudad;
	private String calle;

	private Notario notario_id;
	
	private ArrayList<AccionistasParticipadores> accionistas_participadores;
	private ArrayList<Locales> locales;
	private ArrayList<Negocios> negocio;
	private ArrayList<Papeles> papeles;
	private ArrayList<Testigos> testigos;
	private ArrayList<Aportadores> aportadors;
	private ArrayList<Trabajador> trabajadores;
	
	// Constructores
	public Empresa(String cIF, int id, String texto, String fecha, String pago_notario, String resumen_condiciones,
			String observaciones, String nombre_empresa, String tipo_empresa, String capital_aportado, String pais,
			String ciudad, String calle, Notario notario_id,
			ArrayList<AccionistasParticipadores> accionistas_participadores, ArrayList<Locales> locales,
			ArrayList<Negocios> negocio, ArrayList<Papeles> papeles, ArrayList<Testigos> testigos,
			ArrayList<Aportadores> aportadors, ArrayList<Trabajador> trabajadores) {
		super();
		this.CIF = cIF;
		this.id = id;
		this.texto = texto;
		this.fecha = fecha;
		this.pago_notario = pago_notario;
		this.resumen_condiciones = resumen_condiciones;
		this.observaciones = observaciones;
		this.nombre_empresa = nombre_empresa;
		this.tipo_empresa = tipo_empresa;
		this.capital_aportado = capital_aportado;
		this.pais = pais;
		this.ciudad = ciudad;
		this.calle = calle;
		this.notario_id = notario_id;
		this.accionistas_participadores = accionistas_participadores;
		this.locales = locales;
		this.negocio = negocio;
		this.papeles = papeles;
		this.testigos = testigos;
		this.aportadors = aportadors;
		this.trabajadores = trabajadores;
	}
	
	public Empresa() {}

	// Getters and Setters
	public String getCIF() {
		return CIF;
	}

	public void setCIF(String cIF) {
		CIF = cIF;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getPago_notario() {
		return pago_notario;
	}

	public void setPago_notario(String pago_notario) {
		this.pago_notario = pago_notario;
	}

	public String getResumen_condiciones() {
		return resumen_condiciones;
	}

	public void setResumen_condiciones(String resumen_condiciones) {
		this.resumen_condiciones = resumen_condiciones;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getNombre_empresa() {
		return nombre_empresa;
	}

	public void setNombre_empresa(String nombre_empresa) {
		this.nombre_empresa = nombre_empresa;
	}

	public String getTipo_empresa() {
		return tipo_empresa;
	}

	public void setTipo_empresa(String tipo_empresa) {
		this.tipo_empresa = tipo_empresa;
	}

	public String getCapital_aportado() {
		return capital_aportado;
	}

	public void setCapital_aportado(String capital_aportado) {
		this.capital_aportado = capital_aportado;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public Notario getNotario_id() {
		return notario_id;
	}

	public void setNotario_id(Notario notario_id) {
		this.notario_id = notario_id;
	}

	public ArrayList<AccionistasParticipadores> getAccionistas_participadores() {
		return accionistas_participadores;
	}

	public void setAccionistas_participadores(ArrayList<AccionistasParticipadores> accionistas_participadores) {
		this.accionistas_participadores = accionistas_participadores;
	}

	public ArrayList<Locales> getLocales() {
		return locales;
	}

	public void setLocales(ArrayList<Locales> locales) {
		this.locales = locales;
	}

	public ArrayList<Negocios> getNegocio() {
		return negocio;
	}

	public void setNegocio(ArrayList<Negocios> negocio) {
		this.negocio = negocio;
	}

	public ArrayList<Papeles> getPapeles() {
		return papeles;
	}

	public void setPapeles(ArrayList<Papeles> papeles) {
		this.papeles = papeles;
	}

	public ArrayList<Testigos> getTestigos() {
		return testigos;
	}

	public void setTestigos(ArrayList<Testigos> testigos) {
		this.testigos = testigos;
	}

	public ArrayList<Aportadores> getAportadors() {
		return aportadors;
	}

	public void setAportadors(ArrayList<Aportadores> aportadors) {
		this.aportadors = aportadors;
	}

	public ArrayList<Trabajador> getTrabajadores() {
		return trabajadores;
	}

	public void setTrabajadores(ArrayList<Trabajador> trabajadores) {
		this.trabajadores = trabajadores;
	}
}