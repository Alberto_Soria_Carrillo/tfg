package clasesAxiliares;

/**
 * Clase Aportadores. Guarda la estructura del objeto Aportadores de la BBDD
 * @author Alberto Soria Carrillo
 *
 */
public class Aportadores {
	
	// Atributos
	private String DNI_CIF;
	private int id;
	private String nombre;
	private String cantidad;
	private String moneda;
	private Empresa empresa_id;
	private Aportadores representante_de;
	
	// Constructores
	public Aportadores(String dNI_CIF, int id, String nombre, String cantidad, String moneda, Empresa empresa_id,
			Aportadores representante_de) {
		super();
		DNI_CIF = dNI_CIF;
		this.id = id;
		this.nombre = nombre;
		this.cantidad = cantidad;
		this.moneda = moneda;
		this.empresa_id = empresa_id;
		this.representante_de = representante_de;
	}
	
	public Aportadores() {}

	// Getters and Setters
	public String getDNI_CIF() {
		return DNI_CIF;
	}

	public void setDNI_CIF(String dNI_CIF) {
		DNI_CIF = dNI_CIF;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCantidad() {
		return cantidad;
	}

	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public Empresa getEmpresa_id() {
		return empresa_id;
	}

	public void setEmpresa_id(Empresa empresa_id) {
		this.empresa_id = empresa_id;
	}

	public Aportadores getRepresentante_de() {
		return representante_de;
	}

	public void setRepresentante_de(Aportadores representante_de) {
		this.representante_de = representante_de;
	}
	
	
}
