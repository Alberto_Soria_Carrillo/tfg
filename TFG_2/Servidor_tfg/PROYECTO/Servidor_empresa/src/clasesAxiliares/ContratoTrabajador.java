package clasesAxiliares;

import java.util.ArrayList;

/**
 * Clase ContratoTrabajador. Guarda la estructura del objeto ContratoTrabajador de la BBDD
 * 
 * @author Alberto Soria Carrillo
 *
 */
public class ContratoTrabajador {
	
	// Atributos
	private int id;
	private String horario;
	private String rango;
	private String fecha_inicio;
	private String fecha_finalizacion;
	private String pago_diario;
	private String fecha_despido;
	private String tipo_despido;
	private String pais;
	private String ciudad;
	private String calle;

	private Trabajador trabajador;
	private Empresa empresa;
	
	private ArrayList<AnexosContrato> anexos_contrato;

	// Constructor
	public ContratoTrabajador(int id, String horario, String rango, String fecha_inicio, String fecha_finalizacion,
			String pago_diario, String fecha_despido, String tipo_despido, String pais, String ciudad, String calle,
			Trabajador trabajador, Empresa empresa, ArrayList<AnexosContrato> anexos_contrato) {
		super();
		this.id = id;
		this.horario = horario;
		this.rango = rango;
		this.fecha_inicio = fecha_inicio;
		this.fecha_finalizacion = fecha_finalizacion;
		this.pago_diario = pago_diario;
		this.fecha_despido = fecha_despido;
		this.tipo_despido = tipo_despido;
		this.pais = pais;
		this.ciudad = ciudad;
		this.calle = calle;
		this.trabajador = trabajador;
		this.empresa = empresa;
		this.anexos_contrato = anexos_contrato;
	}
	
	public ContratoTrabajador() {}

	// Getters and Setteres
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getHorario() {
		return horario;
	}

	public void setHorario(String horario) {
		this.horario = horario;
	}

	public String getRango() {
		return rango;
	}

	public void setRango(String rango) {
		this.rango = rango;
	}

	public String getFecha_inicio() {
		return fecha_inicio;
	}

	public void setFecha_inicio(String fecha_inicio) {
		this.fecha_inicio = fecha_inicio;
	}

	public String getFecha_finalizacion() {
		return fecha_finalizacion;
	}

	public void setFecha_finalizacion(String fecha_finalizacion) {
		this.fecha_finalizacion = fecha_finalizacion;
	}

	public String getPago_diario() {
		return pago_diario;
	}

	public void setPago_diario(String pago_diario) {
		this.pago_diario = pago_diario;
	}

	public String getFecha_despido() {
		return fecha_despido;
	}

	public void setFecha_despido(String fecha_despido) {
		this.fecha_despido = fecha_despido;
	}

	public String getTipo_despido() {
		return tipo_despido;
	}

	public void setTipo_despido(String tipo_despido) {
		this.tipo_despido = tipo_despido;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public Trabajador getTrabajador() {
		return trabajador;
	}

	public void setTrabajador(Trabajador trabajador) {
		this.trabajador = trabajador;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public ArrayList<AnexosContrato> getAnexos_contrato() {
		return anexos_contrato;
	}

	public void setAnexos_contrato(ArrayList<AnexosContrato> anexos_contrato) {
		this.anexos_contrato = anexos_contrato;
	}

	@Override
	public String toString() {
		return "ContratoTrabajador [id=" + id + ", horario=" + horario + ", rango=" + rango + ", fecha_inicio="
				+ fecha_inicio + ", fecha_finalizacion=" + fecha_finalizacion + ", pago_diario=" + pago_diario
				+ ", fecha_despido=" + fecha_despido + ", tipo_despido=" + tipo_despido + ", pais=" + pais + ", ciudad="
				+ ciudad + ", calle=" + calle + ", trabajador=" + trabajador + ", empresa=" + empresa
				+ ", anexos_contrato=" + anexos_contrato + "]";
	}	
}
