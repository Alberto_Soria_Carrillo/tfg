package clasesAxiliares;

/**
 * Clase Salidas. Guarda la estructura del objeto Salidas de la BBDD
 * @author Alberto Soria Carrillo
 *
 */
public class Salidas {
	
	// Atributos
	private int id;
	private String fecha;
	private String salida;
	private int trabajador;
	
	// Constructor
	public Salidas(int id, String fecha, String salida, int trabajador) {
		super();
		this.id = id;
		this.fecha = fecha;
		this.salida = salida;
		this.trabajador = trabajador;
	}
	
	public Salidas() {}

	// Getters and Setters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getSalida() {
		return salida;
	}

	public void setSalida(String salida) {
		this.salida = salida;
	}

	public int getTrabajador() {
		return trabajador;
	}

	public void setTrabajador(int trabajador) {
		this.trabajador = trabajador;
	}
	
	
	
	
}
