package clasesAxiliares;

/**
 * Clase Notario. Guarda la estructura del objeto Notario de la BBDD
 * @author Alberto Soria Carrillo
 *
 */
public class Notario {
	
	// Atributos
	private String DNI_CIF;
	private int id;
	private String nombre;
	private String numero_colegiado;
	private String pais;
	private String ciudad;
	private String calle;
	
	// Constructor
	public Notario(String dNI_CIF, int id, String nombre, String numero_colegiado, String pais, String ciudad,
			String calle) {
		super();
		DNI_CIF = dNI_CIF;
		this.id = id;
		this.nombre = nombre;
		this.numero_colegiado = numero_colegiado;
		this.pais = pais;
		this.ciudad = ciudad;
		this.calle = calle;
	}

	public Notario() {}

	// Getters and Setters
	public String getDNI_CIF() {
		return DNI_CIF;
	}

	public void setDNI_CIF(String dNI_CIF) {
		DNI_CIF = dNI_CIF;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNumero_colegiado() {
		return numero_colegiado;
	}

	public void setNumero_colegiado(String numero_colegiado) {
		this.numero_colegiado = numero_colegiado;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}
	
	
	
}
