package clasesAxiliares;

/**
 * Clase Entradas. Guarda la estructura del objeto Entradas de la BBDD
 * @author Alberto Soria Carrillo
 *
 */
public class Entradas {
	
	// Atributos
	private int id;
	private String fecha;
	private String salida;
	private int trabajador;
	
	// Constructor
	public Entradas(int id, String fecha, String salida, int trabajador) {
		super();
		this.id = id;
		this.fecha = fecha;
		this.salida = salida;
		this.trabajador = trabajador;
	}
	
	public Entradas() {}

	// Getters and Setters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getSalida() {
		return salida;
	}

	public void setSalida(String salida) {
		this.salida = salida;
	}

	public int getTrabajador() {
		return trabajador;
	}

	public void setTrabajador(int trabajador) {
		this.trabajador = trabajador;
	}
	
	

}
