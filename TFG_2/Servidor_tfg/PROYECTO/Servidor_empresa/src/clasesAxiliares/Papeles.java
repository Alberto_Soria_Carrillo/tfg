package clasesAxiliares;

/**
 * Clase Papeles. Guarda la estructura del objeto Papeles de la BBDD
 * @author Alberto Soria Carrillo
 *
 */
public class Papeles {
	
	// Atributos
	private int id;
	private String nombre;
	private String numero_referencia;
	private String descripcion;
	private String observaciones;
	private String ruta;
	private String fecha_guardado;
	private String fecha_papeles;
	private Empresa empresa_id;
	
	// Constructor
	public Papeles(int id, String nombre, String numero_referencia, String descripcion, String observaciones,
			String ruta, String fecha_guardado, String fecha_papeles, Empresa empresa_id) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.numero_referencia = numero_referencia;
		this.descripcion = descripcion;
		this.observaciones = observaciones;
		this.ruta = ruta;
		this.fecha_guardado = fecha_guardado;
		this.fecha_papeles = fecha_papeles;
		this.empresa_id = empresa_id;
	}

	public Papeles() {}

	// Getters and Setters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNumero_referencia() {
		return numero_referencia;
	}

	public void setNumero_referencia(String numero_referencia) {
		this.numero_referencia = numero_referencia;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getRuta() {
		return ruta;
	}

	public void setRuta(String ruta) {
		this.ruta = ruta;
	}

	public String getFecha_guardado() {
		return fecha_guardado;
	}

	public void setFecha_guardado(String fecha_guardado) {
		this.fecha_guardado = fecha_guardado;
	}

	public String getFecha_papeles() {
		return fecha_papeles;
	}

	public void setFecha_papeles(String fecha_papeles) {
		this.fecha_papeles = fecha_papeles;
	}

	public Empresa getEmpresa_id() {
		return empresa_id;
	}

	public void setEmpresa_id(Empresa empresa_id) {
		this.empresa_id = empresa_id;
	}
	
	
	
}
