package clasesAxiliares;

/**
 * Clase NamePassUser. Guarda la estructura del objeto NamePassUser de la BBDD
 * @author Alberto Soria Carrillo
 *
 */
public class NamePassUser {
	
	// Atributos
	private int id;
	private String nombre;
	private String pass;
	
	// Constructores
	public NamePassUser(int id, String nombre, String pass) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.pass = pass;
	}

	public NamePassUser() {}
	
	// Getters and Setters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}
	
	
	
}
