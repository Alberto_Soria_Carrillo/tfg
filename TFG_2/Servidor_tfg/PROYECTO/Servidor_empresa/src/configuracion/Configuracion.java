package configuracion;

import java.io.BufferedReader;
import java.io.Console;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Scanner;
import java.util.stream.Stream;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import conexion.Conexion;
import datos.Datos;
import rsa.RSA;
import servidor.Server;

/**
 * Clase Configuracion, se ocupa de las tareas de configuracion iniciales del
 * servidor
 * 
 * @author Alberto Soria Carrillo
 *
 */
public class Configuracion {

	private static Scanner in = new Scanner(System.in); // entrada de datos por teclado
	private static JFileChooser fileChooser; // ventana de busqueda de archivos
	private static JFileChooser fileChooser2; // ventana de busqueda de archivos
	private static String urlPriv = "";
	private static String urlPub = "";

	/**
	 * Realiza la peticion de los datos al usuario
	 * 
	 * @return
	 * @throws Throwable
	 */
	public static Conexion conexionBBDD(Datos datos) throws Throwable {

		System.out.println("- Datos de conexion de la Base de Datos -\n");

//		System.out.print("Indica la direccion ip de la Base de Datos: ");
//		String ip = in.nextLine();
//
//		System.out.print("Indica el puerto de conexion con la Base de Datos: ");
//		String puerto = in.nextLine();
//
//		System.out.print("Indica el nombre de la Base de Datos: ");
//		String nombre = in.nextLine();
//
//		System.out.print("Indica el nombre del usuario de la Base de Datos: ");
//		String usuario = in.nextLine();
//
//		System.out.print("Indica la contrase�a de la Base de Datos: ");
//		String passwd = in.nextLine();
//
//		return new Conexion(true, ip, puerto, nombre, usuario, passwd);

		Console cons = System.console();
		Configuracion confi = new Configuracion();

		try {
			String ip = confi.readLine("Indica la direccion ip de la Base de Datos: ");

			String puerto = confi.readLine("Indica el puerto de conexion con la Base de Datos: ");

			String nombre = confi.readLine("Indica el nombre de la Base de Datos: ");

			String usuario = confi.readLine("Indica el nombre del usuario de la Base de Datos: ");

			char[] passwd = confi.readPassword("Indica la contrase�a de la Base de Datos: ");

			datos.user = usuario;
			datos.pass = new String(passwd);

			return new Conexion(true, ip, puerto, nombre, usuario, new String(passwd));
		} catch (IOException e) {
			return null;
		}

	}

	/**
	 * Realiza la lectura de daos introducidos por consola
	 * 
	 * @param format
	 * @param args
	 * @return
	 * @throws IOException
	 */
	public String readLine(String format, Object... args) throws IOException {
		if (System.console() != null) {
			return System.console().readLine(format, args);
		}
		System.out.print(String.format(format, args));
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		return reader.readLine();
	}

	/**
	 * Realiza la lectura de contrase�as introducidas por consola
	 * 
	 * @param format
	 * @param args
	 * @return
	 * @throws IOException
	 */
	public char[] readPassword(String format, Object... args) throws IOException {
		if (System.console() != null)
			return System.console().readPassword(format, args);
		return this.readLine(format, args).toCharArray();
	}

	/**
	 * Realiza la configuracion de los sockets
	 * 
	 * @param conexionBBDD
	 * @param datos
	 * @return
	 */
	public static Server socketsBBDD(Conexion conexionBBDD, RSA rsa, Datos datos) {

		int puerto = 0;

		System.out.println("\n- Datos del Socket de conexion del cliente -\n");

		boolean aBoolean = true;

		// bucle de control. Controla que se introduce un numero valido
		while (aBoolean) {

			try {
				System.out.print("Indica el puerto de conexion de los clientes: ");
				puerto = in.nextInt();

				if (puerto < 65000 && puerto > 0) {
					aBoolean = false;
				} else {
					System.out.println("\nLos rangos permitidos son de 0 a 65000\n");
				}

			} catch (Exception e) {
				System.err.println("\nDebe ser un numero entero\n");
				in.nextLine();
			}

		}

		return new Server(puerto, conexionBBDD, rsa, datos);
	}

	/**
	 * Configura los archivo RSA que se usaran en el servidor
	 * 
	 * @return
	 * @throws IOException
	 * @throws InvalidKeySpecException
	 * @throws NoSuchAlgorithmException
	 * @throws BadPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws NoSuchPaddingException
	 * @throws InvalidKeyException
	 */
	public static RSA rsaBBDD() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException,
			InvalidKeyException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {

		RSA rsa = new RSA();
		

		File privada = new File("rsa\\rsa.pri");
		File publica = new File("rsa\\rsa.pub");

		System.out.println("\n- Configuracion de los archivos de cifrado -\n");

		if (privada.exists() && publica.exists()) {
			urlPriv = privada.getAbsolutePath();
			urlPub = publica.getAbsolutePath();
			System.out.println("Configuracion automatica terminada");
		} else {

			String respuesta = "";

			do {
				System.out.println(
						"No se han encontado los archivos. �Desea generarlos? (\"SI\" para generarlos, \"No\" para buscar los archivos de manera manual)");
				respuesta = in.nextLine();

			} while (!respuesta.equalsIgnoreCase("si") && !respuesta.equalsIgnoreCase("no"));

			if (respuesta.equalsIgnoreCase("si")) {
				// Generala clave
				rsa.genKeyPair(4096);
				// LAs guarda en la ubicacion por defecto
				rsa.saveToDiskPrivateKey(privada.getAbsolutePath());
				rsa.saveToDiskPublicKey(publica.getAbsolutePath());
				// Asigna la ubicacion por defecto a las variables tipo cadena que se usan para
				// abrir los archivos
				urlPriv = privada.getAbsolutePath();
				urlPub = publica.getAbsolutePath();
			}

			if (respuesta.equalsIgnoreCase("no")) {
				// busqueda manual de los archivos
				buscarArchivosRSA();
			}
		}

		rsa.openFromDiskPrivateKey(urlPriv);
		rsa.openFromDiskPublicKey(urlPub);

		return rsa;
	}

	/**
	 * Permite al usario indicar una ruta diferente a la predefinida en el programa
	 * para los archivos RSA
	 * 
	 * @param urlPriv
	 * @param urlPub
	 */
	private static void buscarArchivosRSA() {
		fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);

		System.out.println("\n- Configuraci�n del cifrado RSA -\n");

		System.out.println("\tSeleccione la clave Privada");
		fileChooser.setFileFilter(new FileNameExtensionFilter("", "pri"));
		if (fileChooser.showDialog(null, "Seleccione la clave Privada") == JFileChooser.APPROVE_OPTION) {
			urlPriv = fileChooser.getSelectedFile().getAbsolutePath();
		}

		fileChooser2 = new JFileChooser();
		fileChooser2.setFileSelectionMode(JFileChooser.FILES_ONLY);
		
		System.out.println("\tSeleccione la clave Publica");
		fileChooser2.setFileFilter(new FileNameExtensionFilter("", "pub"));
		if (fileChooser2.showDialog(null, "Seleccione la clave Publica") == JFileChooser.APPROVE_OPTION) {
			urlPub = fileChooser2.getSelectedFile().getAbsolutePath();
		}		

	}

	/**
	 * Lee los archivos .pri y .pub de la ruta indicada
	 * 
	 * @param string
	 * @throws IOException
	 */
	private static void readFiles(String string) throws IOException {
		try (Stream<Path> paths = Files.walk(Paths.get(string))) {
			paths.filter(Files::isRegularFile).filter(path -> path.toString().endsWith(".pri"))
					.forEach(System.out::println);
			paths.filter(Files::isRegularFile).filter(path -> path.toString().endsWith(".pub"))
					.forEach(System.out::println);
		}

	}

	/**
	 * Crea las carpetas necesarias para guardar los archivos. En caso de existir no
	 * realiza ninguna acci�n
	 */
	public static void createFolders() {
		File rsaFolder = new File("rsa");
		rsaFolder.mkdirs();

		File imgFolder = new File("img");
		imgFolder.mkdirs();

		File paperFolder = new File("paper");
		paperFolder.mkdirs();

	}

}
