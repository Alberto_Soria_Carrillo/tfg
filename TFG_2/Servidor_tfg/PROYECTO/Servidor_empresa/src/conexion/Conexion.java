package conexion;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import javax.swing.JOptionPane;

/**
 * Clase Conexion. Encargada de establecer la conexion con la base de datos.
 *
 * @author Alberto Soria Carrillo
 */
public class Conexion {

	// Atributos
	private Connection connection;
	private PreparedStatement preparedStatement;
	private ResultSet resultSet;
	private CallableStatement callableStatement; // Para invocar procedimientos
	private boolean versionJDBC = true; // Para la version antigua false, para la nueva true
	private String ipBBDD;
	private String puertoBBDD;
	private String nombreBBDD;
	private String usuarioBBDD;
	private String passBBDD;
	private String textBBDD = null; // guarda la ruta al archivo txt con las sentencias sql de la BBDD
	private String sepradaorTextBBDD; // simbilo usado en el metodo split() para indicar la separacion entre
										// sentencias sql
	private String[][] remplazarTextBBDD = null; // guarda las parte que se quieren remplazar del texto

	// Constructor

	/**
	 * Este contructor crea la clase Conexion con los atributos basicos y con la
	 * version mas moderna del JDBC
	 * 
	 * @param ipBBDD
	 * @param puertoBBDD
	 * @param nombreBBDD
	 * @param usuarioBBDD
	 * @param passBBDD
	 */
	public Conexion(String ipBBDD, String puertoBBDD, String nombreBBDD, String usuarioBBDD, String passBBDD) {

		this.ipBBDD = ipBBDD;
		this.puertoBBDD = puertoBBDD;
		this.nombreBBDD = nombreBBDD;
		this.usuarioBBDD = usuarioBBDD;
		this.passBBDD = passBBDD;

	}

	/**
	 * Este contructor crea la clase Conexion con los atributos basicos y con la
	 * version indicada del JDBC (true la moderna, false la antigua)
	 * 
	 * @param versionJDBC
	 * @param ipBBDD
	 * @param puertoBBDD
	 * @param nombreBBDD
	 * @param usuarioBBDD
	 * @param passBBDD
	 */
	public Conexion(boolean versionJDBC, String ipBBDD, String puertoBBDD, String nombreBBDD, String usuarioBBDD,
			String passBBDD) {

		this.versionJDBC = versionJDBC;
		this.ipBBDD = ipBBDD;
		this.puertoBBDD = puertoBBDD;
		this.nombreBBDD = nombreBBDD;
		this.usuarioBBDD = usuarioBBDD;
		this.passBBDD = passBBDD;

	}

	/**
	 * Este contructor crea la clase Conexion con los atributos basicos, con la
	 * version indicada del JDBC (true la moderna, false la antigua) y con la ruta
	 * al txt de la BBDD con el separador usado entre sentencias SQL
	 * 
	 * @param versionJDBC
	 * @param ipBBDD
	 * @param puertoBBDD
	 * @param nombreBBDD
	 * @param usuarioBBDD
	 * @param passBBDD
	 * @param textBBDD
	 * @param separadorTextBBDD
	 */
	public Conexion(boolean versionJDBC, String ipBBDD, String puertoBBDD, String nombreBBDD, String usuarioBBDD,
			String passBBDD, String textBBDD, String separadorTextBBDD) {

		this.versionJDBC = versionJDBC;
		this.ipBBDD = ipBBDD;
		this.puertoBBDD = puertoBBDD;
		this.nombreBBDD = nombreBBDD;
		this.usuarioBBDD = usuarioBBDD;
		this.passBBDD = passBBDD;
		this.textBBDD = textBBDD;
		this.sepradaorTextBBDD = separadorTextBBDD;

	}

	/**
	 * Este contructor crea la clase Conexion con los atributos basicos, con la
	 * version indicada del JDBC (true la moderna, false la antigua), con la ruta al
	 * txt de la BBDD con el separador usado entre sentencias SQL y las palabras a
	 * remplazar ([cantidad de palabras a remplazar][posicion 0 para la palabra que
	 * se busca, posicion 1 para el remplazo])
	 * 
	 * @param versionJDBC
	 * @param ipBBDD
	 * @param puertoBBDD
	 * @param nombreBBDD
	 * @param usuarioBBDD
	 * @param passBBDD
	 * @param textBBDD
	 * @param separadorTextBBDD
	 * @param remplazarTextBBDD
	 */
	public Conexion(boolean versionJDBC, String ipBBDD, String puertoBBDD, String nombreBBDD, String usuarioBBDD,
			String passBBDD, String textBBDD, String separadorTextBBDD, String[][] remplazarTextBBDD) {

		this.versionJDBC = versionJDBC;
		this.ipBBDD = ipBBDD;
		this.puertoBBDD = puertoBBDD;
		this.nombreBBDD = nombreBBDD;
		this.usuarioBBDD = usuarioBBDD;
		this.passBBDD = passBBDD;
		this.textBBDD = textBBDD;
		this.sepradaorTextBBDD = separadorTextBBDD;
		this.remplazarTextBBDD = remplazarTextBBDD;

	}

	// Getters and Setters
	public Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	public PreparedStatement getPreparedStatement() {
		return preparedStatement;
	}

	public void setPreparedStatement(PreparedStatement preparedStatement) {
		this.preparedStatement = preparedStatement;
	}

	public ResultSet getResultSet() {
		return resultSet;
	}

	public void setResultSet(ResultSet resultSet) {
		this.resultSet = resultSet;
	}

	public CallableStatement getCallableStatement() {
		return callableStatement;
	}

	public void setCallableStatement(CallableStatement callableStatement) {
		this.callableStatement = callableStatement;
	}

	public boolean isVersionJDBC() {
		return versionJDBC;
	}

	public void setVersionJDBC(boolean versionJDBC) {
		this.versionJDBC = versionJDBC;
	}

	public String getIpBBDD() {
		return ipBBDD;
	}

	public void setIpBBDD(String ipBBDD) {
		this.ipBBDD = ipBBDD;
	}

	public String getPuertoBBDD() {
		return puertoBBDD;
	}

	public void setPuertoBBDD(String puertoBBDD) {
		this.puertoBBDD = puertoBBDD;
	}

	public String getNombreBBDD() {
		return nombreBBDD;
	}

	public void setNombreBBDD(String nombreBBDD) {
		this.nombreBBDD = nombreBBDD;
	}

	public String getUsuarioBBDD() {
		return usuarioBBDD;
	}

	public void setUsuarioBBDD(String usuarioBBDD) {
		this.usuarioBBDD = usuarioBBDD;
	}

	public String getPassBBDD() {
		return passBBDD;
	}

	public void setPassBBDD(String passBBDD) {
		this.passBBDD = passBBDD;
	}

	public String getTextBBDD() {
		return textBBDD;
	}

	public void setTextBBDD(String textBBDD) {
		this.textBBDD = textBBDD;
	}

	public String getSepradaorTextBBDD() {
		return sepradaorTextBBDD;
	}

	public void setSepradaorTextBBDD(String sepradaorTextBBDD) {
		this.sepradaorTextBBDD = sepradaorTextBBDD;
	}

	public String[][] getRemplazarTextBBDD() {
		return remplazarTextBBDD;
	}

	public void setRemplazarTextBBDD(String[][] remplazarTextBBDD) {
		this.remplazarTextBBDD = remplazarTextBBDD;
	}

	/**
	 * Encargado de la conexion
	 */
	public void conectar() {
		try {
			// Crea la url
			String url = "jdbc:mysql://" + this.ipBBDD + ":" + this.puertoBBDD + "/" + this.nombreBBDD
					+ "?serverTimezone=UTC";

			// Selecciona el controlador
			if (versionJDBC) {
				Class.forName("com.mysql.cj.jdbc.Driver"); // controlador nuevo
			} else {
				Class.forName("com.mysql.jdbc.Driver"); // controlador antiguo deprecado
			}

			// crea la conexion con la BBDD
			this.connection = DriverManager.getConnection(url, this.usuarioBBDD, this.passBBDD);

		} catch (SQLException | ClassNotFoundException e) {

			if (this.textBBDD != null) {
				// Si existe el archivo txt con los parametros de la BBDD, la crea
				createBBDD();
			} else {
				// Advierte del fallo de conexion
				JOptionPane.showMessageDialog(null, "Fallo en la conexión");
				e.printStackTrace();
			}

		}
	}

	/**
	 * Crea la base de datos
	 */
	private void createBBDD() {
		try {
			// crea la url
			String url = "jdbc:mysql://" + this.ipBBDD + ":" + this.puertoBBDD + "/?serverTimezone=UTC";

			// Selecciona el controlador para conectar con la BBDD
			if (versionJDBC) {
				Class.forName("com.mysql.cj.jdbc.Driver"); // controlador nuevo
			} else {
				Class.forName("com.mysql.jdbc.Driver"); // controlador antiguo deprecado
			}

			// crea la conexion con la BBDD
			this.connection = DriverManager.getConnection(url, this.usuarioBBDD, this.passBBDD);

			// Abre el archivo con las sentencias SQL
			BufferedReader br = new BufferedReader(new FileReader(this.textBBDD));

			// Lee el archivo con las sentencias SQL y lo guarda en memoria
			String BBDD = "";
			String linea;
			while ((linea = br.readLine()) != null) {

				// comprueba si se debe remplazar alguna parte del texto
				if (this.remplazarTextBBDD != null) {

					// remplaza las partes indicadas
					for (int i = 0; i < this.remplazarTextBBDD.length; i++) {

						linea.replaceAll(this.remplazarTextBBDD[i][0], this.remplazarTextBBDD[i][1]);

					}

				}

				BBDD = BBDD + linea;

			}

			br.close();
			
			// Separa el archivo en memoria por sentencias
			String[] querys = BBDD.split(this.sepradaorTextBBDD);

			// Ejecuta las sentencias en la BBDD
			for (int i = 0; i < querys.length; i++) {
				insertarPS(querys[i]);
			}

		} catch (SQLException | ClassNotFoundException | FileNotFoundException e) {
			JOptionPane.showMessageDialog(null, "Fallo en la creación de la Base de Datos");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Encargado de la desconexion
	 */
	public void desconectar() {
		try {
			this.connection.close();
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Fallo en la desconexión");
			e.printStackTrace();
		}
	}

	/**
	 * Realiza la busqueda en la BBDD
	 * 
	 * @param query
	 * @return
	 * @throws SQLException
	 */
	public ResultSet buscarPS(String query) throws SQLException {
		return this.connection.prepareStatement(query).executeQuery();
	}

	/**
	 * Realiza la query sql indicada
	 *
	 * @param query
	 * @throws SQLException
	 */
	public ResultSet buscarCS(String query) throws SQLException {
		return this.connection.prepareCall(query).executeQuery();
	}

	/**
	 * Realiza las peticions de inserción en la BBDD
	 * 
	 * @param query
	 * @throws SQLException
	 */
	public void insertarPS(String query) throws SQLException {
		this.connection.prepareStatement(query).executeUpdate();
	}

	/**
	 * Realiza las peticion de eliminación de las BBDD
	 * 
	 * @param query
	 * @throws SQLException
	 */
	public void deletPS(String query) throws SQLException {
		this.connection.prepareStatement(query).execute();
	}

	/**
	 * Realiza las peticion es actualizacón en la BBDD
	 * 
	 * @param query
	 * @throws SQLException
	 */
	public void updatePS(String query) throws SQLException {
		this.connection.prepareStatement(query).executeUpdate();
	}
}
