package servidor;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import clasesAxiliares.AccionistasParticipadores;
import clasesAxiliares.AdelantoTabajador;
import clasesAxiliares.AnexosContrato;
import clasesAxiliares.Aportadores;
import clasesAxiliares.Bajas;
import clasesAxiliares.ContratoTrabajador;
import clasesAxiliares.Empresa;
import clasesAxiliares.Entradas;
import clasesAxiliares.Locales;
import clasesAxiliares.NamePassUser;
import clasesAxiliares.Negocios;
import clasesAxiliares.Notario;
import clasesAxiliares.Papeles;
import clasesAxiliares.Salidas;
import clasesAxiliares.Testigos;
import clasesAxiliares.Trabajador;
import conexion.Conexion;
import datos.Datos;
import libreriaAlberto.Crypto;
import rsa.RSA;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.joda.time.LocalDateTime;

/**
 * Clase ServidorHilo, la cual se ocupa de la resolucion del envio y recepcion
 * de la informacion entre el cliente y el servicor y el servidor y la BBDD
 *
 * @author Alberto Soria Carrillo
 */
public class ServidorHilo extends Thread {
	// Atributos
	private final static String passAes = "Montessori2020-2021";
	private Socket socket;
	private DataOutputStream salida;
	private DataInputStream entrada;
	private Gson gson;
	private Crypto crypto;
	private int idUser;
	private Conexion conexion;
	private RSA rsaBBDD; // cifrado de la BBDD
	private RSA rsaServer;// cifrado temporal de al conexion para los paquetes recibidos por el servidor
	private RSA rsaClient;// cifrado temporal de al conexion para los paquetes recibidos por el cliente

	// Atributos auxiliares. Estos atributos guardan la informacion recibida desde
	// la BBDD para acelerar las busquedas
	private Empresa empresa = null; // datos de la empresa en esta sesion
	private Trabajador trabajador = null; // datos del trabajador en esta sesion
	private String tipo_trabajador; // tipo de trabajador
	private String responsable; // responsable del trabajador
	private String notario_id; // id del notario de la empresa
	private ArrayList<String> representante_de = new ArrayList<>(); // id del representante del aportador
	private boolean bucle = true; // controla el buccle de las opciones de usuario
	private ServerSocket ss;

	// Constructor
	public ServidorHilo(Socket socket, int idUsers, Conexion conexion, RSA rsa) throws InvalidKeyException,
			NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException {
		this.socket = socket;
		this.gson = new Gson(); // se encarga del formato JSON a los datos
		this.crypto = new Crypto();// Encripta y desencripta la informacion entre el servidor y el cliente
		this.idUser = idUsers; // id de sesion
		this.conexion = conexion;
		this.rsaBBDD = rsa;
		this.rsaClient = new RSA();
		this.rsaServer = new RSA();
		rsaServer.genKeyPair(4096);
		this.ss = new ServerSocket((this.socket.getLocalPort() + 1));

		try {

			salida = new DataOutputStream(this.socket.getOutputStream());
			entrada = new DataInputStream(this.socket.getInputStream());

		} catch (IOException ex) {
			Logger.getLogger(ServidorHilo.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	@Override
	public void run() {
		try {
			intercambioClaves();
			// Comprueba que el usuario existe y si existe inicia el bucle de acciones
			if (verificacionUsario()) {
				switch (tipo_trabajador) {
				case Datos.adnimnistrador:
					rellenarEmpresa();
					rellenarArrayTrabajadores();
					bucleAdminstrador();
					break;
				case Datos.encargado:
					rellenarArrayTrabajadores();
					bucleEncargado();
					break;
				case Datos.trabajador:
					bucleTrabajador();
					break;
				}
			}
		} catch (Exception e) {
			try {
				this.entrada.close();
				this.salida.close();
				this.socket.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

	/**
	 * Rellena el ArrayList de los trabajadores y lo envia al cliente de manera fraccionada junto con el resto de la información de las tablas conectadas
	 * 
	 * @throws InvalidKeyException
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws IOException
	 * @throws InvalidKeySpecException
	 * @throws NoSuchProviderException
	 * @throws SQLException
	 */
	private void rellenarArrayTrabajadores() throws InvalidKeyException, UnsupportedEncodingException,
			NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
			IOException, InvalidKeySpecException, NoSuchProviderException, SQLException {
		// Objeto trabajador

		String query = "";

		// selecciona el tipo de sentencia
		if (Datos.adnimnistrador.equalsIgnoreCase(this.tipo_trabajador)) {
			query = "select trabajadores.* from trabajadores, contratos_trabajadores where trabajadores.id = contratos_trabajadores.trabajador and contratos_trabajadores.empresa = "
					+ empresa.getId() + " group by trabajadores.id";
		}
		if (Datos.encargado.equalsIgnoreCase(this.tipo_trabajador)) {
			query = "select trabajadores.* from trabajadores,contratos_trabajadores where trabajadores.id = contratos_trabajadores.trabajador and contratos_trabajadores.empresa = "
					+ "(select contratos_trabajadores.empresa from trabajadores,contratos_trabajadores where trabajadores.id = contratos_trabajadores.trabajador and trabajadores.id = "
					+ trabajador.getId() + " limit 1) group by trabajadores.id;";
		}

		// relllena el Array
		ArrayList<Trabajador> tra = rellenarArrayListTrabajador(conexion.buscarPS(query));

		// envia la cantidad de ciclos de envio que necesitara para enviar toda la información
		salida.writeUTF(crypto.encriptar(gson.toJson(tra.size()), passAes));

		// envia el trabajador, y busca y envia el resto de la informacion relacionada del mismo de la BBDD
		for (Trabajador trabajador : tra) {
			salida.writeUTF(crypto.encriptar(gson.toJson(trabajador), passAes));
			// Objeto adelatoTrabajador
			query = "select * from adelanto_trabajador where adelanto_trabajador.trabajador_id = " + trabajador.getId();
			trabajador.setAdelanto_trabajador(rellenarAdelantoTrabajador(conexion.buscarPS(query)));
			salida.writeUTF(crypto.encriptar(gson.toJson(trabajador.getAdelanto_trabajador().size()), passAes));
			for (int i = 0; i < trabajador.getAdelanto_trabajador().size(); i++) {
				salida.writeUTF(crypto.encriptar(gson.toJson(trabajador.getAdelanto_trabajador().get(i)), passAes));
			}

			// objeto salida
			query = "select * from salidas where trabajador = " + trabajador.getId();
			trabajador.setSalida(rellenarSalidas(conexion.buscarPS(query)));
			salida.writeUTF(crypto.encriptar(gson.toJson(trabajador.getSalida().size()), passAes));
			for (int i = 0; i < trabajador.getSalida().size(); i++) {
				salida.writeUTF(crypto.encriptar(gson.toJson(trabajador.getSalida().get(i)), passAes));
			}

			// objeto entrada
			query = "select * from entradas where trabajador = " + trabajador.getId();
			trabajador.setEntrada(rellenarEntradas(conexion.buscarPS(query)));
			salida.writeUTF(crypto.encriptar(gson.toJson(trabajador.getEntrada().size()), passAes));
			for (int i = 0; i < trabajador.getEntrada().size(); i++) {
				salida.writeUTF(crypto.encriptar(gson.toJson(trabajador.getEntrada().get(i)), passAes));
			}

			// obeto baja
			query = "select * from bajas where trabajador = " + trabajador.getId();
			trabajador.setBaja(rellenarBajas(conexion.buscarPS(query)));
			salida.writeUTF(crypto.encriptar(gson.toJson(trabajador.getBaja().size()), passAes));
			for (int i = 0; i < trabajador.getBaja().size(); i++) {
				salida.writeUTF(crypto.encriptar(gson.toJson(trabajador.getBaja().get(i)), passAes));
			}

			// objeto contratoTrabajador
			query = "select * from contratos_trabajadores where trabajador = " + trabajador.getId();
			trabajador.setContrato_trabajador(rellenarContratoTrabajador(conexion.buscarPS(query)));
			salida.writeUTF(crypto.encriptar(gson.toJson(trabajador.getContrato_trabajador().size()), passAes));
			for (int i = 0; i < trabajador.getContrato_trabajador().size(); i++) {
				salida.writeUTF(crypto.encriptar(gson.toJson(trabajador.getContrato_trabajador().get(i)), passAes));

				query = "select * from anexos_contratos where contrato_id = "
						+ trabajador.getContrato_trabajador().get(i).getId();
				trabajador.getContrato_trabajador().get(i)
						.setAnexos_contrato(rellenarAnexosContratoTrabajador(conexion.buscarPS(query)));
				salida.writeUTF(crypto.encriptar(
						gson.toJson(trabajador.getContrato_trabajador().get(i).getAnexos_contrato().size()), passAes));

				for (int o = 0; o < trabajador.getContrato_trabajador().get(i).getAnexos_contrato().size(); o++) {
					salida.writeUTF(crypto.encriptar(
							gson.toJson(trabajador.getContrato_trabajador().get(i).getAnexos_contrato().get(o)),
							passAes));
				}

			}

			query = "select * from trabajadores where id = " + responsable;
			trabajador.setResposable(rellenarTrabajador(conexion.buscarPS(query)));

			salida.writeUTF(crypto.encriptar(gson.toJson(trabajador.getResposable()), passAes));

		}

	}

	/**
	 * Crea, rellena y debuelve el arrayList con la informacion de los trabajadores
	 * 
	 * @param buscarPS
	 * @return
	 * @throws SQLException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws InvalidKeySpecException
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchProviderException
	 */
	private ArrayList<Trabajador> rellenarArrayListTrabajador(ResultSet buscarPS) throws SQLException,
			InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException,
			BadPaddingException, InvalidKeySpecException, UnsupportedEncodingException, NoSuchProviderException {
		ArrayList<Trabajador> tra = new ArrayList<>();

		while (buscarPS.next()) {

			Trabajador trabajador = new Trabajador();

			trabajador.setId(buscarPS.getInt(1));
			trabajador.setDni_cif(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(2))));
			trabajador.setNumero_ss(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(3))));
			trabajador.setNombre(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(4))));
			trabajador.setApellido1(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(5))));
			trabajador.setApellido2(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(6))));
			trabajador.setFecha_nacimiento(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(7))));
			trabajador.setGenero(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(8))));
			trabajador.setEstado(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(9))));
			trabajador.setNacionalidad(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(10))));
			trabajador.setHorario(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(11))));
			trabajador.setTelefono(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(12))));
			trabajador.setEmail(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(13))));
			trabajador.setPais(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(14))));
			trabajador.setCiudad(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(15))));
			trabajador.setCalle(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(16))));
			trabajador.setTipo_usuario(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(17))));
			// responsable = rsaBBDD.Decrypt(buscarPS.getString(18));

			tra.add(trabajador);

		}

		buscarPS.close();

		return tra;
	}

	/**
	 * Bucle para recepcion de las peticiones del usuario de tipo trabajdor
	 * 
	 * @throws InvalidKeyException
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws InvalidKeySpecException
	 * @throws NoSuchProviderException
	 * @throws IOException
	 * @throws SQLException
	 */
	private void bucleTrabajador() throws InvalidKeyException, UnsupportedEncodingException, NoSuchAlgorithmException,
			NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException,
			NoSuchProviderException, IOException, SQLException {
		String query = "";

		while (bucle) {

			int x = (gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Integer.class));

			switch (x) {
			case 22:
				this.insertarEntrada(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Integer.class));
				query = "select * from entradas where id = (select max(id) from entradas)";
				salida.writeUTF(
						crypto.encriptar(gson.toJson(this.rellenarEntradas(conexion.buscarPS(query)).get(0)), passAes));
				break;
			case 23:
				this.insertarSalidas(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Integer.class));
				query = "select * from salidas where id = (select max(id) from salidas)";
				salida.writeUTF(
						crypto.encriptar(gson.toJson(this.rellenarSalidas(conexion.buscarPS(query)).get(0)), passAes));
				break;
			case 99:
				try {
					this.entrada.close();
					this.salida.close();
					this.socket.close();
					bucle = false;
				} catch (IOException e1) {

					e1.printStackTrace();
				}

				break;
			default:
				break;

			}
		}

	}

	/**
	 * Bucle para recepcion de las peticiones del usuario de tipo encargado
	 * 
	 * @throws JsonSyntaxException
	 * @throws InvalidKeyException
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws IOException
	 * @throws InvalidKeySpecException
	 * @throws NoSuchProviderException
	 * @throws SQLException
	 */
	private void bucleEncargado() throws JsonSyntaxException, InvalidKeyException, UnsupportedEncodingException,
			NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
			IOException, InvalidKeySpecException, NoSuchProviderException, SQLException {

		String query = "";

		while (bucle) {

			switch (gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Integer.class)) {
			case 1:
				this.updateTrabajador(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Trabajador.class));
				break;
			case 3:
				this.updateContratoTrabajador(
						gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), ContratoTrabajador.class));
				break;
			case 22:
				this.insertarEntrada(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Integer.class));
				query = "select * from entradas where id = (select max(id) from entradas)";
				salida.writeUTF(
						crypto.encriptar(gson.toJson(this.rellenarEntradas(conexion.buscarPS(query)).get(0)), passAes));
				break;
			case 23:
				this.insertarSalidas(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Integer.class));
				query = "select * from salidas where id = (select max(id) from salidas)";
				salida.writeUTF(
						crypto.encriptar(gson.toJson(this.rellenarSalidas(conexion.buscarPS(query)).get(0)), passAes));
				break;
			case 99:
				try {
					this.entrada.close();
					this.salida.close();
					this.socket.close();
					bucle = false;
				} catch (IOException e1) {
					e1.printStackTrace();
				}

				break;
			default:
				break;
			}
		}

	}

	/**
	 * Bucle para recepcion de las peticiones del usuario de tipo administrador
	 * 
	 * @throws JsonSyntaxException
	 * @throws InvalidKeyException
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws IOException
	 * @throws InvalidKeySpecException
	 * @throws NoSuchProviderException
	 * @throws SQLException
	 */
	private void bucleAdminstrador() throws JsonSyntaxException, InvalidKeyException, UnsupportedEncodingException,
			NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
			IOException, InvalidKeySpecException, NoSuchProviderException, SQLException {

		while (bucle) {

			switch (gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Integer.class)) {

			case 1:
				this.updateTrabajador(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Trabajador.class));
				break;
			case 2:
				this.updateEmpresa(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Empresa.class));
				break;
			case 3:
				this.updateContratoTrabajador(
						gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), ContratoTrabajador.class));
				break;
			case 4:
				this.insertContratoTrabajador(
						gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), ContratoTrabajador.class));

				String query = "select * from contratos_trabajadores where id = (select max(id) from contratos_trabajadores) limit 1";
				ContratoTrabajador contrato = rellenarContratoTrabajador(conexion.buscarPS(query)).get(0);
				salida.writeUTF(crypto.encriptar(gson.toJson(contrato), passAes));

				query = "select * from anexos_contratos where contrato_id = " + contrato.getId();
				contrato.setAnexos_contrato(rellenarAnexosContratoTrabajador(conexion.buscarPS(query)));
				salida.writeUTF(crypto.encriptar(gson.toJson(contrato.getAnexos_contrato().size()), passAes));

				for (int o = 0; o < contrato.getAnexos_contrato().size(); o++) {
					salida.writeUTF(crypto.encriptar(gson.toJson(contrato.getAnexos_contrato().get(o)), passAes));
				}
				break;
			case 5:
				this.selectOptNotario(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Notario.class));
				break;
			case 6:
				salida.writeUTF(
						crypto.encriptar(gson.toJson(this.insertAccionesParticipadores(empresa.getId())), passAes));
				break;
			case 7:
				salida.writeUTF(crypto.encriptar(gson.toJson(this.insertAportadores(empresa.getId())), passAes));
				break;
			case 8:
				salida.writeUTF(crypto.encriptar(gson.toJson(this.insertTstigos(empresa.getId())), passAes));
				break;
			case 9:
				this.deleteAccionistasParticipadores(
						gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Integer.class));
				break;
			case 10:
				this.deleteAportadores(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Integer.class));
				break;
			case 11:
				this.deleteTestigos(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Integer.class));
				break;
			case 12:
				this.updateAportador(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Aportadores.class));
				break;
			case 13:
				this.updateTestigo(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Testigos.class));
				break;
			case 14:
				this.updateAccionistas(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes),
						AccionistasParticipadores.class));
				break;
			case 15:
				this.insertTrabajador(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Integer.class));

				query = "select * from trabajadores where id = (select max(id) from trabajadores)";
				Trabajador tra = this.rellenarTrabajador(conexion.buscarPS(query));
				salida.writeUTF(crypto.encriptar(gson.toJson(tra), passAes));
				rellenarNuevoTrabajador(tra);
				break;
			case 16:

				int a = gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Integer.class);
				int b = gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Integer.class);

				agregarContratoTrabajadorVacio(a, b);

				query = "select * from contratos_trabajadores where id = (select max(id) from contratos_trabajadores)";
				ContratoTrabajador ct = this.rellenarContratoTrabajador(conexion.buscarPS(query)).get(0);
				salida.writeUTF(crypto.encriptar(gson.toJson(ct), passAes));

				query = "select * from anexos_contratos where contrato_id = " + ct.getId();
				ArrayList<AnexosContrato> ac = this.rellenarAnexosContratoTrabajador(conexion.buscarCS(query));
				salida.writeUTF(crypto.encriptar(gson.toJson(ac.size()), passAes));
				for (AnexosContrato aux : ac) {
					salida.writeUTF(crypto.encriptar(gson.toJson(aux), passAes));
				}

				break;
			case 17:

				query = "select * from entradas where trabajador = "
						+ gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Integer.class);
				ArrayList<Entradas> entradas = this.rellenarEntradas(conexion.buscarPS(query));

				salida.writeUTF(crypto.encriptar(gson.toJson(entradas.size()), passAes));

				for (Entradas e : entradas) {
					salida.writeUTF(crypto.encriptar(gson.toJson(e), passAes));
				}

				break;

			case 18:

				query = "select * from salidas where trabajador = "
						+ gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Integer.class);
				ArrayList<Salidas> salidas = this.rellenarSalidas(conexion.buscarPS(query));

				salida.writeUTF(crypto.encriptar(gson.toJson(salidas.size()), passAes));

				for (Salidas e : salidas) {
					salida.writeUTF(crypto.encriptar(gson.toJson(e), passAes));
				}

				break;
			case 19:

				String tabla = gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), String.class);
				int id = gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Integer.class);

				query = "delete from " + tabla + " where id = " + id;
				conexion.deletPS(query);

				break;
			case 20:
				this.eliminarTrabajador(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Integer.class),
						rsaServer
								.Decrypt(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), String.class)));
				break;
			case 21:
				this.eliminarContrato(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Integer.class));
				break;
			case 22:
				this.insertarEntrada(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Integer.class));
				query = "select * from entradas where id = (select max(id) from entradas)";
				salida.writeUTF(
						crypto.encriptar(gson.toJson(this.rellenarEntradas(conexion.buscarPS(query)).get(0)), passAes));
				break;
			case 23:
				this.insertarSalidas(gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Integer.class));
				query = "select * from salidas where id = (select max(id) from salidas)";
				salida.writeUTF(
						crypto.encriptar(gson.toJson(this.rellenarSalidas(conexion.buscarPS(query)).get(0)), passAes));
				break;
			case 24:
				this.recibirImagen();
				break;
			case 25:
				String ruta = gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), String.class);
				this.enviarImagen(ruta);
				break;
			case 99:
				try {
					this.entrada.close();
					this.salida.close();
					this.socket.close();
					bucle = false;
				} catch (IOException e1) {
					e1.printStackTrace();
				}

				break;
			default:
				break;

			}

		}

	}

	/**
	 * Envia un aimagen al cliente
	 * 
	 * P.D. para Vicente: dado que no hemos dado encriptaciond de imagenes, he dejado el metodo comentado. Pero aqui lo tienes, que conste
	 * 
	 * @param ruta
	 * @throws IOException
	 */
	private void enviarImagen(String ruta) throws IOException {
		
		File logo = new File(ruta);

		byte[] mybytearray = new byte[(int) logo.length()];
        BufferedInputStream bis = new BufferedInputStream(new FileInputStream(logo));
        bis.read(mybytearray, 0, mybytearray.length);
        Socket s2 = new Socket(socket.getInetAddress(),(socket.getPort()+2));
        OutputStream os = s2.getOutputStream();
        os.write(mybytearray, 0, mybytearray.length);

        os.flush();
        os.close();
        bis.close();
        s2.close();

	}

	/**
	 * Recibe una imagen y la guarda en la carpeta por defecto
	 * 
	 * P.D. para Vicente: dado que no hemos dado encriptaciond de imagenes, he dejado el metodo comentado. Pero aqui lo tienes, que conste
	 * 
	 * @throws IOException
	 * @throws SQLException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws InvalidKeySpecException
	 * @throws NoSuchProviderException
	 */
	private void recibirImagen()
			throws IOException, SQLException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException,
			IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException, NoSuchProviderException {

		 

		File file = null;
		String rutaFinal = "";

		int count = 0;

		while (true) {
			rutaFinal = "img/Prueba(" + count + ").png";

			file = new File(rutaFinal);

			if (!file.exists()) {
				break;
			}
			count++;

		}
		
		Socket s2 = ss.accept();
		OutputStream output = new FileOutputStream(file.getPath());
		InputStream in = s2.getInputStream();
		
		int bytesRead;
		byte[] buffer = new byte[1024];
		while ((bytesRead = in.read(buffer)) != -1) {
			output.write(buffer, 0, bytesRead);
		}
		
		in.close();
		s2.close();
		output.close();
		
		
		int i = gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Integer.class);
		String query = "delete from papeles where id = " + i;
		if (i > 0) {
			conexion.deletPS(query);
		}
		query = "insert into papeles(nombre,ruta,fecha_guardado,empresa_id) values('" + rsaBBDD.Encrypt("logo") + "','"
				+ rsaBBDD.Encrypt(file.getAbsolutePath()) + "','" + rsaBBDD.Encrypt(String.valueOf(LocalDateTime.now()))
				+ "'," + empresa.getId() + ")";
		conexion.insertarPS(query);

	}

	/**
	 * Inserta una salida en la BBDD
	 * 
	 * @param i
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws InvalidKeySpecException
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchProviderException
	 * @throws SQLException
	 */
	private void insertarSalidas(int i) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException,
			IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException, UnsupportedEncodingException,
			NoSuchProviderException, SQLException {
		String query = "insert into salidas(trabajador,fecha,salida) values( '" + i + "', '"
				+ rsaBBDD.Encrypt(String.valueOf(LocalDate.now())) + "', '"
				+ rsaBBDD.Encrypt(String.valueOf(LocalTime.now())) + "')";
		conexion.deletPS(query);
	}

	/**
	 * inserta una entrada en la BBDD
	 * 
	 * @param i
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws InvalidKeySpecException
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchProviderException
	 * @throws SQLException
	 */
	private void insertarEntrada(int i) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException,
			IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException, UnsupportedEncodingException,
			NoSuchProviderException, SQLException {
		String query = "insert into entradas(trabajador,fecha,entrada) values( '" + i + "', '"
				+ rsaBBDD.Encrypt(String.valueOf(LocalDate.now())) + "', '"
				+ rsaBBDD.Encrypt(String.valueOf(LocalTime.now())) + "')";
		conexion.deletPS(query);
	}

	/**
	 * Elimina un contrato de un trabajador, con sus anexos, de la BBDD
	 * 
	 * @param i
	 * @throws SQLException
	 */
	private void eliminarContrato(int i) throws SQLException {
		String query = "delete from anexos_contratos where contrato_id = (select id from contratos_trabajadores where id = "
				+ i + ")";
		conexion.deletPS(query);
		query = "delete from contratos_trabajadores where id = " + i;
		conexion.deletPS(query);

	}

	/**
	 * Elimina un trabajador de la BBDD
	 * 
	 * @param i
	 * @param string
	 * @throws SQLException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws InvalidKeySpecException
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchProviderException
	 */
	private void eliminarTrabajador(int i, String string) throws SQLException, InvalidKeyException,
			NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
			InvalidKeySpecException, UnsupportedEncodingException, NoSuchProviderException {

		eliminarAdelantoTrabajador(i);
		eliminarSalidas(i);
		eliminarEntradas(i);
		eliminarBajas(i);

		agregarFechaDespidoAlContrato(i, string);

		String query = "select user_pass from trabajadores where id = " + i;
		int idUserPass = this.maxId(conexion.buscarPS(query));
		eliminarTrabajador(i);
		eliminarNamePassUser(idUserPass);

	}

	/**
	 * Elimina un trabajador de la BBDD
	 * 
	 * @param i
	 * @throws SQLException
	 */
	private void eliminarTrabajador(int i) throws SQLException {
		String query = "delete from trabajadores where id = " + i;
		conexion.deletPS(query);
	}

	/**
	 * Agrega la fecha de despido al contrato
	 * 
	 * @param i
	 * @param string
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws InvalidKeySpecException
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchProviderException
	 * @throws SQLException
	 */
	private void agregarFechaDespidoAlContrato(int i, String string) throws InvalidKeyException,
			NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
			InvalidKeySpecException, UnsupportedEncodingException, NoSuchProviderException, SQLException {
		String query = "update contratos_trabajadores set fecha_despido = '"
				+ rsaBBDD.Encrypt(String.valueOf(LocalDate.now())) + "', " + "tipo_despido = '"
				+ rsaBBDD.Encrypt(string) + "', trabajador = " + null + " where trabajador = " + i;
		conexion.updatePS(query);
	}

	/**
	 * Elimina ua baja de la BBDD
	 * 
	 * @param i
	 * @throws SQLException
	 */
	private void eliminarBajas(int i) throws SQLException {
		String query = "delete from bajas where trabajador = " + i;
		conexion.deletPS(query);
	}

	/**
	 * Elimina una entrada de la BBDD
	 * 
	 * @param i
	 * @throws SQLException
	 */
	private void eliminarEntradas(int i) throws SQLException {
		String query = "delete from entradas where trabajador = " + i;
		conexion.deletPS(query);
	}

	/**
	 * Elimina una salida de la BBDD
	 * 
	 * @param i
	 * @throws SQLException
	 */
	private void eliminarSalidas(int i) throws SQLException {
		String query = "delete from salidas where trabajador = " + i;
		conexion.deletPS(query);
	}

	/**
	 * elimina un adelanto de la BBDD
	 * 
	 * @param i
	 * @throws SQLException
	 */
	private void eliminarAdelantoTrabajador(int i) throws SQLException {
		String query = "delete from adelanto_trabajador where trabajador_id = " + i;
		conexion.deletPS(query);
	}

	/**
	 * Elimina las credenciales de acceso del usuario
	 * 
	 * @param i
	 * @throws SQLException
	 */
	private void eliminarNamePassUser(int i) throws SQLException {

		String query = "delete from name_pass_user where id = (select user_pass from trabajadores where id = " + i
				+ " )";
		conexion.deletPS(query);

	}

	/**
	 * Inserta un trabajador en la BBDD e invoca el metodo agregarContratoTrabajadorVacio, para agregar el 1º contrato
	 * 
	 * @param idEmpresa
	 * @throws JsonSyntaxException
	 * @throws InvalidKeyException
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws IOException
	 * @throws InvalidKeySpecException
	 * @throws NoSuchProviderException
	 * @throws SQLException
	 */
	private void insertTrabajador(int idEmpresa) throws JsonSyntaxException, InvalidKeyException,
			UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException,
			BadPaddingException, IOException, InvalidKeySpecException, NoSuchProviderException, SQLException {

		// recibe los datoa de inicio de sesion
		NamePassUser npu = gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), NamePassUser.class);
		// crea el usuario en la tabla name_pass_user
		String query = "insert into name_pass_user(nombre,pass) values('"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(npu.getNombre())) + "', '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(npu.getPass())) + "')";
		conexion.insertarPS(query);
		// busca el id que la BBDD le a dado
		query = "select max(id) from name_pass_user";
		int idNPU = this.maxId(conexion.buscarPS(query));
		// crea un trabajador en la tabla trabajadores y lo anexa al creado en la tabla
		// name_pass_user
		query = "insert into trabajadores(user_pass,tipo_usuario) values(" + idNPU + ", '"
				+ rsaBBDD.Encrypt(Datos.trabajador) + "')";
		conexion.insertarPS(query);
		// busca el id que le dio la BBDD
		query = "select max(id) from trabajadores";
		int idTrabajador = this.maxId(conexion.buscarPS(query));

		agregarContratoTrabajadorVacio(idTrabajador, idEmpresa);

	}

	/**
	 * Crea y agrega un contrato a la BBDD, indicando la empresa y el trabajador al que pertenece
	 * 
	 * @param idTrabajador
	 * @param idEmpresa
	 * @throws SQLException
	 */
	private void agregarContratoTrabajadorVacio(int idTrabajador, int idEmpresa) throws SQLException {
		// crea un contrato base y le agreega el trabajador y la empresa a quien
		// pertenece. Esto se hace al entender que no puede existir un trabajdor sin
		// contrato
		String query = "insert into contratos_trabajadores(trabajador, empresa) values(" + idTrabajador + ", "
				+ idEmpresa + ")";
		conexion.insertarPS(query);

	}

	/**
	 * Actualiza un accionista de la BBDD
	 * 
	 * @param aux
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws InvalidKeySpecException
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchProviderException
	 * @throws SQLException
	 */
	private void updateAccionistas(AccionistasParticipadores aux) throws InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException,
			UnsupportedEncodingException, NoSuchProviderException, SQLException {

		String update = "Update accionistas_participadores set DNI_CIF = '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getDNI_CIF())) + "', nombre = '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getNombre())) + "', porcetnaje = '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getPorcetnaje())) + "', totales = '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getTotales())) + "', tipo_moneda = '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getTipo_moneda())) + "' where id = " + aux.getId();
		conexion.insertarPS(update);

	}

	/**
	 * Actualiza un testigo en la BBDD
	 * 
	 * @param aux
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws InvalidKeySpecException
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchProviderException
	 * @throws SQLException
	 */
	private void updateTestigo(Testigos aux) throws InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException,
			UnsupportedEncodingException, NoSuchProviderException, SQLException {

		String query = "Update testigos set DNI_CIF = '" + rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getDNI_CIF()))
				+ "', nombre = '" + rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getNombre())) + "' where id = " + aux.getId();
		conexion.insertarPS(query);

	}

	/**
	 * Actualiza un aportaor en la BBDD
	 * 
	 * @param aux
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws InvalidKeySpecException
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchProviderException
	 * @throws SQLException
	 */
	private void updateAportador(Aportadores aux) throws InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException,
			UnsupportedEncodingException, NoSuchProviderException, SQLException {

		String query = "Update aportadores set DNI_CIF = '" + rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getDNI_CIF()))
				+ "', nombre = '" + rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getNombre())) + "', cantidad = '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getCantidad())) + "', moneda = '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getMoneda())) + "' where id = " + aux.getId();

		conexion.insertarPS(query);

	}

	/**
	 * Elimina un testigo de la BBDD
	 * 
	 * @param i
	 * @throws SQLException
	 */
	private void deleteTestigos(int i) throws SQLException {

		String query = "delete from testigos where id = " + i;

		conexion.deletPS(query);

	}

	/**
	 * Elimina un aportador de la BBDD
	 * 
	 * @param i
	 * @throws SQLException
	 */
	private void deleteAportadores(int i) throws SQLException {

		String query = "delete from aportadores where id = " + i;

		conexion.deletPS(query);

	}

	/**
	 * Elimina un accionista de la BBDD
	 * 
	 * @param i
	 * @throws SQLException
	 */
	private void deleteAccionistasParticipadores(int i) throws SQLException {

		String query = "delete from accionistas_participadores where id = " + i;

		conexion.deletPS(query);

	}

	/**
	 * Inserta un nuevo testigo en la BBDD y lo debuelve
	 * 
	 * @param id
	 * @return
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws InvalidKeySpecException
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchProviderException
	 * @throws SQLException
	 */
	private Testigos insertTstigos(int id) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException,
			IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException, UnsupportedEncodingException,
			NoSuchProviderException, SQLException {

		Testigos testigo = new Testigos();

		String query = "insert into testigos(empresa_id) values(" + empresa.getId() + ")";

		conexion.insertarPS(query);

		query = "select * from testigos where id = (select max(id) from testigos)";

		testigo = this.rellenarTestigos(conexion.buscarPS(query)).get(0);

		return testigo;
	}

	/**
	 * Inserta un nuevo aportador en la BBDD y lo debuelve
	 * 
	 * @param id
	 * @return
	 * @throws SQLException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws InvalidKeySpecException
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchProviderException
	 */
	private Aportadores insertAportadores(int id) throws SQLException, InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException,
			UnsupportedEncodingException, NoSuchProviderException {

		Aportadores apor = new Aportadores();

		String query = "insert into aportadores(empresa_id) values(" + empresa.getId() + ")";

		conexion.insertarPS(query);

		query = "select * from aportadores where id = (select max(id) from aportadores)";

		apor = this.rellenarAportadores(conexion.buscarPS(query)).get(0);

		return apor;
	}

	/**
	 * Inserta un nuevo accionista participador en la BBDD y lo debuelve
	 * 
	 * @param id
	 * @return
	 * @throws SQLException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws InvalidKeySpecException
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchProviderException
	 */
	private AccionistasParticipadores insertAccionesParticipadores(int id) throws SQLException, InvalidKeyException,
			NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
			InvalidKeySpecException, UnsupportedEncodingException, NoSuchProviderException {

		AccionistasParticipadores accPar = new AccionistasParticipadores();

		String query = "insert into accionistas_participadores(empresa_id) values(" + empresa.getId() + ")";

		conexion.insertarPS(query);

		query = "select * from accionistas_participadores where id = (select max(id) from accionistas_participadores)";

		accPar = this.rellenarAccParti(conexion.buscarPS(query)).get(0);

		return accPar;
	}

	/**
	 * Controla la accion a realizar en la BBDD sobre la tabla notarios
	 * 
	 * @param aux
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws InvalidKeySpecException
	 * @throws NoSuchProviderException
	 * @throws SQLException
	 * @throws JsonSyntaxException
	 * @throws IOException
	 */
	private void selectOptNotario(Notario aux) throws InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException,
			NoSuchProviderException, SQLException, JsonSyntaxException, IOException {

		if (aux.getId() != 0) {
			this.updateNotario(aux);
		} else {
			this.insetNotario(aux, gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), Integer.class));
		}

	}

	/**
	 * actualiza el notario
	 * 
	 * @param aux
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws InvalidKeySpecException
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchProviderException
	 * @throws SQLException
	 */
	private void updateNotario(Notario aux) throws InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException,
			UnsupportedEncodingException, NoSuchProviderException, SQLException {

		String query = "update notarios set DNI_CIF = '" + rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getDNI_CIF()))
				+ "', nombre = '" + rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getNombre())) + "', numero_colegiado = '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getNumero_colegiado())) + "', pais = '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getPais())) + "', ciudad = '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getCiudad())) + "', calle = '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getCalle())) + "'";

		conexion.insertarPS(query);

	}

	/**
	 * Inserta un nuevo notario en la BBDD y lo enlaza con la empresa
	 * 
	 * @param aux
	 * @param i
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws InvalidKeySpecException
	 * @throws NoSuchProviderException
	 * @throws SQLException
	 * @throws IOException
	 */
	private void insetNotario(Notario aux, int i)
			throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException,
			BadPaddingException, InvalidKeySpecException, NoSuchProviderException, SQLException, IOException {

		String query = "insert into notarios(DNI_CIF, nombre, numero_colegiado, pais, ciudad, calle)" + "values ('"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getDNI_CIF())) + "', '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getNombre())) + "', '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getNumero_colegiado())) + "', '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getPais())) + "', '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getCiudad())) + "', '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getCalle())) + "')";
		conexion.insertarPS(query);

		query = "select max(id) from notarios";
		int idNotario = maxId(conexion.buscarPS(query));
		salida.writeUTF(crypto.encriptar(gson.toJson(idNotario), passAes));

		query = "update contrato_empresa set notario_id = " + idNotario + " where id = " + i;
		conexion.insertarPS(query);

	}

	/**
	 * Debuelve el valor numerico de la consulta+
	 * 
	 * @param buscarPS
	 * @return
	 * @throws SQLException
	 */
	private int maxId(ResultSet buscarPS) throws SQLException {

		int num = 0;

		while (buscarPS.next()) {

			num = buscarPS.getInt(1);

		}

		return num;
	}

	/*
	 * Inserta un nuevo contrato de trabajador
	 */
	private void insertContratoTrabajador(ContratoTrabajador aux) throws SQLException, InvalidKeyException,
			NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
			InvalidKeySpecException, UnsupportedEncodingException, NoSuchProviderException {

		String quere = "insert into contratos_trabajadores(horario, rango, fecha_inicio, fecha_finalizacion, pago_diario, fecha_despido, tipo_despido, pais, ciudad, calle, trabajador, empresa) "
				+ "values ('" + rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getHorario())) + "', '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getRango())) + "', '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getFecha_inicio())) + "', '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getFecha_finalizacion())) + "', '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getPago_diario())) + "', '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getFecha_despido())) + "', '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getTipo_despido())) + "', '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getPais())) + "', '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getCiudad())) + "', '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getCalle())) + "', " + trabajador.getId() + ", "
				+ empresa.getId() + ")";

		conexion.insertarPS(quere);
	}

	/**
	 * Actualiza los datos guardados en la BBDD del contrato del trabajador
	 * 
	 * @param aux
	 * @throws SQLException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws InvalidKeySpecException
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchProviderException
	 */
	private void updateContratoTrabajador(ContratoTrabajador aux) throws SQLException, InvalidKeyException,
			NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
			InvalidKeySpecException, UnsupportedEncodingException, NoSuchProviderException {

		String query = "UPDATE contratos_trabajadores SET horario = '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getHorario())) + "', rango = '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getRango())) + "', fecha_inicio = '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getFecha_inicio())) + "', fecha_finalizacion = '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getFecha_finalizacion())) + "', pago_diario = '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getPago_diario())) + "', fecha_despido = '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getFecha_despido())) + "', tipo_despido = '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getTipo_despido())) + "', pais = '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getPais())) + "', ciudad = '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getCiudad())) + "', calle = '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getCalle())) + "' WHERE id = " + aux.getId();

		conexion.insertarPS(query);

	}

	/**
	 * Actualiza los datos guardados en la BBDD de la empresa
	 * 
	 * @param aux
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws InvalidKeySpecException
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchProviderException
	 * @throws SQLException
	 */
	private void updateEmpresa(Empresa aux) throws InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException,
			UnsupportedEncodingException, NoSuchProviderException, SQLException {
		String query = "UPDATE contrato_empresa SET CIF = '" + rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getCIF()))
				+ "', texto = '" + rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getTexto())) + "', fecha = '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getFecha())) + "', pago_notario = '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getPago_notario())) + "', resumen_condiciones = '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getResumen_condiciones())) + "', observaciones = '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getObservaciones())) + "', nombre_empresa = '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getNombre_empresa())) + "', tipo_empresa = '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getTipo_empresa())) + "', capital_aportado = '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getCapital_aportado())) + "', pais = '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getPais())) + "', ciudad = '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getCiudad())) + "', calle = '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getCalle())) + "'  where id = " + aux.getId();

		conexion.insertarPS(query);

	}

	/**
	 * Actualiza el trabajador en la BBDD
	 * 
	 * @param aux
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws InvalidKeySpecException
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchProviderException
	 * @throws SQLException
	 */
	private void updateTrabajador(Trabajador aux) throws InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException,
			UnsupportedEncodingException, NoSuchProviderException, SQLException {

		String query = "UPDATE trabajadores SET dni_cif = '" + rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getDni_cif()))
				+ "', numero_ss = '" + rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getNumero_ss())) + "', nombre = '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getNombre())) + "', apellido1 = '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getApellido1())) + "', apellido2 = '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getApellido2())) + "', fecha_nacimiento = '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getFecha_nacimiento())) + "', genero = '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getGenero())) + "', estado = '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getEstado())) + "', nacionalidad = '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getNacionalidad())) + "', horario = '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getHorario())) + "', telefono = '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getTelefono())) + "', email = '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getEmail())) + "', pais = '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getPais())) + "', ciudad = '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getCiudad())) + "', calle = '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getCalle())) + "', tipo_usuario = '"
				+ rsaBBDD.Encrypt(rsaServer.Decrypt(aux.getTipo_usuario())) + "' where id = " + aux.getId();

		conexion.insertarPS(query);

	}

	/**
	 * Rellena los datos de la empresa
	 * 
	 * @throws SQLException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws InvalidKeySpecException
	 * @throws NoSuchProviderException
	 * @throws IOException
	 */
	private void rellenarEmpresa() throws SQLException, InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException,
			NoSuchProviderException, IOException {
		empresa = new Empresa();

		String query = "select contrato_empresa.* from contrato_empresa, contratos_trabajadores, trabajadores "
				+ "where contrato_empresa.id = contratos_trabajadores.empresa "
				+ "and contratos_trabajadores.trabajador = trabajadores.id "
				+ "and contratos_trabajadores.id = (select max(contratos_trabajadores.id) from contratos_trabajadores where trabajador = "
				+ trabajador.getId() + ")";
		rellenarDatosEmpresa(conexion.buscarPS(query));
		salida.writeUTF(crypto.encriptar(gson.toJson(empresa), passAes));

		// objeto notario
		query = "select * from notarios where id = " + this.notario_id;
		empresa.setNotario_id(rellenarNotario(conexion.buscarPS(query)));
		salida.writeUTF(crypto.encriptar(gson.toJson(empresa.getNotario_id()), passAes));

		// objeto testigos
		query = "select * from testigos where empresa_id = " + empresa.getId();
		empresa.setTestigos(rellenarTestigos(conexion.buscarPS(query)));
		salida.writeUTF(crypto.encriptar(gson.toJson(empresa.getTestigos().size()), passAes));

		for (Testigos testigo : empresa.getTestigos()) {
			salida.writeUTF(crypto.encriptar(gson.toJson(testigo), passAes));
		}

		// objeto accionistas participadores
		query = "select * from accionistas_participadores where empresa_id = " + empresa.getId();
		empresa.setAccionistas_participadores(rellenarAccParti(conexion.buscarPS(query)));
		salida.writeUTF(crypto.encriptar(gson.toJson(empresa.getAccionistas_participadores().size()), passAes));

		for (AccionistasParticipadores acc : empresa.getAccionistas_participadores()) {
			salida.writeUTF(crypto.encriptar(gson.toJson(acc), passAes));
		}

		// objetos local
		query = "select locales.* from locales, local_empresa where locales.id = local_empresa.local_id and local_empresa.empresa_id = "
				+ empresa.getId();
		empresa.setLocales(rellenarLocales(conexion.buscarPS(query)));
		salida.writeUTF(crypto.encriptar(gson.toJson(empresa.getLocales().size()), passAes));

		for (Locales local : empresa.getLocales()) {
			salida.writeUTF(crypto.encriptar(gson.toJson(local), passAes));
		}

		// objeto negocio
		query = "select negocios.* from negocios, negocio_empresa where negocios.id = negocio_empresa.negocio_id and negocio_empresa.empresa_id = "
				+ empresa.getId();
		empresa.setNegocio(rellenarNegocios(conexion.buscarPS(query)));
		salida.writeUTF(crypto.encriptar(gson.toJson(empresa.getNegocio().size()), passAes));
		for (Negocios negocio : empresa.getNegocio()) {
			salida.writeUTF(crypto.encriptar(gson.toJson(negocio), passAes));
		}

		// onjeto aportadores
		query = "select * from aportadores where empresa_id = " + empresa.getId();
		empresa.setAportadors(rellenarAportadores(conexion.buscarPS(query)));
		for (int i = 0; i < empresa.getAportadors().size(); i++) {

			query = "select * from aportadores where empresa_id = " + representante_de.get(i);
			empresa.getAportadors().get(i).setRepresentante_de(rellenarRepresentanteDe(conexion.buscarPS(query)));

		}

		salida.writeUTF(crypto.encriptar(gson.toJson(empresa.getAportadors().size()), passAes));
		for (Aportadores aportador : empresa.getAportadors()) {
			salida.writeUTF(crypto.encriptar(gson.toJson(aportador), passAes));
		}

		// objeto papeles
		query = "select * from papeles where empresa_id = " + empresa.getId();
		empresa.setPapeles(rellenarPapeles(conexion.buscarPS(query)));
		salida.writeUTF(crypto.encriptar(gson.toJson(empresa.getPapeles().size()), passAes));
		for (Papeles papel : empresa.getPapeles()) {
			salida.writeUTF(crypto.encriptar(gson.toJson(papel), passAes));
		}

	}

	/**
	 * rellena el objeto de representante
	 * 
	 * @param buscarPS
	 * @return
	 * @throws SQLException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws InvalidKeySpecException
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchProviderException
	 */
	private Aportadores rellenarRepresentanteDe(ResultSet buscarPS) throws SQLException, InvalidKeyException,
			NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
			InvalidKeySpecException, UnsupportedEncodingException, NoSuchProviderException {
		Aportadores aux = new Aportadores();

		while (buscarPS.next()) {

			aux.setDNI_CIF(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(1))));
			aux.setId(buscarPS.getInt(2));
			aux.setNombre(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(3))));
			aux.setCantidad(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(5))));
		}

		return aux;
	}

	/**
	 * Rellena el arraylist de aportadores
	 * 
	 * @param buscarPS
	 * @return
	 * @throws SQLException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws InvalidKeySpecException
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchProviderException
	 */
	private ArrayList<Aportadores> rellenarAportadores(ResultSet buscarPS) throws SQLException, InvalidKeyException,
			NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
			InvalidKeySpecException, UnsupportedEncodingException, NoSuchProviderException {
		ArrayList<Aportadores> aportadores = new ArrayList<Aportadores>();

		while (buscarPS.next()) {
			Aportadores aux = new Aportadores();

			aux.setDNI_CIF(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(1))));
			aux.setId(buscarPS.getInt(2));
			aux.setNombre(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(3))));
			representante_de.add(buscarPS.getString(4));
			aux.setCantidad(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(5))));
			aux.setMoneda(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(6))));

			aportadores.add(aux);

		}

		return aportadores;
	}

	/**
	 * Agrega los datos al arraylist de los papeles
	 * 
	 * @param buscarPS
	 * @return
	 * @throws SQLException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws InvalidKeySpecException
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchProviderException
	 */
	private ArrayList<Papeles> rellenarPapeles(ResultSet buscarPS) throws SQLException, InvalidKeyException,
			NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
			InvalidKeySpecException, UnsupportedEncodingException, NoSuchProviderException {

		ArrayList<Papeles> papeles = new ArrayList<Papeles>();

		while (buscarPS.next()) {

			Papeles aux = new Papeles();

			aux.setId(buscarPS.getInt(1));
			aux.setNombre(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(2))));
			aux.setNumero_referencia(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(3))));
			aux.setDescripcion(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(4))));
			aux.setObservaciones(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(5))));
			aux.setRuta(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(6))));
			aux.setFecha_guardado(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(7))));
			aux.setFecha_papeles(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(8))));

			papeles.add(aux);

		}

		return papeles;
	}

	/**
	 * Genera un arraylist de negocios con la informacion de BBDD
	 * 
	 * @param buscarPS
	 * @return
	 * @throws SQLException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws InvalidKeySpecException
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchProviderException
	 */
	private ArrayList<Negocios> rellenarNegocios(ResultSet buscarPS) throws SQLException, InvalidKeyException,
			NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
			InvalidKeySpecException, UnsupportedEncodingException, NoSuchProviderException {
		ArrayList<Negocios> negocios = new ArrayList<Negocios>();

		while (buscarPS.next()) {

			Negocios aux = new Negocios();

			aux.setId(buscarPS.getInt(1));
			aux.setNombre_comercial(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(2))));
			aux.setTipo(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(3))));
			aux.setParticipacion(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(4))));
			aux.setPais(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(5))));
			aux.setCiudad(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(6))));
			aux.setCalle(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(7))));

			negocios.add(aux);

		}

		return negocios;
	}

	/**
	 * Rellena el arrayList de locales
	 * 
	 * @param buscarPS
	 * @return
	 * @throws SQLException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws InvalidKeySpecException
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchProviderException
	 */
	private ArrayList<Locales> rellenarLocales(ResultSet buscarPS) throws SQLException, InvalidKeyException,
			NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
			InvalidKeySpecException, UnsupportedEncodingException, NoSuchProviderException {

		ArrayList<Locales> locales = new ArrayList<Locales>();

		while (buscarPS.next()) {

			Locales aux = new Locales();

			aux.setId(buscarPS.getInt(1));
			aux.setMetros_cuadrados(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(2))));
			aux.setObservaciones(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(3))));
			aux.setPais(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(4))));
			aux.setCiudad(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(5))));
			aux.setCalle(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(6))));

			locales.add(aux);

		}

		return locales;
	}

	/**
	 * Rellena la arrayList de AccionistasParticipadores
	 * 
	 * @param buscarPS
	 * @return
	 * @throws SQLException
	 * @throws NoSuchProviderException
	 * @throws UnsupportedEncodingException
	 * @throws InvalidKeySpecException
	 * @throws BadPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws NoSuchPaddingException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 */
	private ArrayList<AccionistasParticipadores> rellenarAccParti(ResultSet buscarPS) throws SQLException,
			InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException,
			BadPaddingException, InvalidKeySpecException, UnsupportedEncodingException, NoSuchProviderException {

		ArrayList<AccionistasParticipadores> accPar = new ArrayList<AccionistasParticipadores>();

		while (buscarPS.next()) {

			AccionistasParticipadores aux = new AccionistasParticipadores();

			aux.setDNI_CIF(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(1))));
			aux.setId(buscarPS.getInt(2));
			aux.setNombre(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(3))));
			aux.setPorcetnaje(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(4))));
			aux.setTotales(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(5))));
			aux.setTipo_moneda(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(6))));

			accPar.add(aux);
		}

		return accPar;
	}

	/**
	 * Rellena el arraylist de testigos
	 * 
	 * @param buscarPS
	 * @return
	 * @throws SQLException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws InvalidKeySpecException
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchProviderException
	 */
	private ArrayList<Testigos> rellenarTestigos(ResultSet buscarPS) throws SQLException, InvalidKeyException,
			NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
			InvalidKeySpecException, UnsupportedEncodingException, NoSuchProviderException {

		ArrayList<Testigos> testigos = new ArrayList<Testigos>();

		while (buscarPS.next()) {

			Testigos aux = new Testigos();

			aux.setDNI_CIF(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(1))));
			aux.setId(buscarPS.getInt(2));
			aux.setNombre(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(3))));

			testigos.add(aux);
		}

		buscarPS.close();

		return testigos;
	}

	/**
	 * Agrega los datos del notario a la empresa
	 * 
	 * @param buscarPS
	 * @return
	 * @throws SQLException
	 * @throws NoSuchProviderException
	 * @throws UnsupportedEncodingException
	 * @throws InvalidKeySpecException
	 * @throws BadPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws NoSuchPaddingException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 */
	private Notario rellenarNotario(ResultSet buscarPS) throws SQLException, InvalidKeyException,
			NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
			InvalidKeySpecException, UnsupportedEncodingException, NoSuchProviderException {
		Notario notario = new Notario();

		while (buscarPS.next()) {

			notario.setDNI_CIF(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(1))));
			notario.setId(buscarPS.getInt(2));
			notario.setNombre(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(3))));
			notario.setNumero_colegiado(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(4))));
			notario.setPais(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(5))));
			notario.setCiudad(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(6))));
			notario.setCalle(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(7))));

		}

		buscarPS.close();

		return notario;
	}

	/**
	 * Introdce los datos de la BBDD al objeto empresa
	 * 
	 * @param resultSet
	 * @param buscarPS
	 * @throws SQLException
	 * @throws NoSuchProviderException
	 * @throws UnsupportedEncodingException
	 * @throws InvalidKeySpecException
	 * @throws BadPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws NoSuchPaddingException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 */
	private void rellenarDatosEmpresa(ResultSet resultSet) throws SQLException, InvalidKeyException,
			NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
			InvalidKeySpecException, UnsupportedEncodingException, NoSuchProviderException {

		while (resultSet.next()) {

			empresa.setCIF(rsaClient.Encrypt(rsaBBDD.Decrypt(resultSet.getString(1))));
			empresa.setId(resultSet.getInt(2));
			empresa.setTexto(rsaClient.Encrypt(rsaBBDD.Decrypt(resultSet.getString(3))));
			empresa.setFecha(rsaClient.Encrypt(rsaBBDD.Decrypt(resultSet.getString(4))));
			empresa.setPago_notario(rsaClient.Encrypt(rsaBBDD.Decrypt(resultSet.getString(5))));
			empresa.setResumen_condiciones(rsaClient.Encrypt(rsaBBDD.Decrypt(resultSet.getString(6))));
			empresa.setObservaciones(rsaClient.Encrypt(rsaBBDD.Decrypt(resultSet.getString(7))));
			empresa.setNombre_empresa(rsaClient.Encrypt(rsaBBDD.Decrypt(resultSet.getString(8))));
			empresa.setTipo_empresa(rsaClient.Encrypt(rsaBBDD.Decrypt(resultSet.getString(9))));
			empresa.setCapital_aportado(rsaClient.Encrypt(rsaBBDD.Decrypt(resultSet.getString(10))));
			empresa.setPais(rsaClient.Encrypt(rsaBBDD.Decrypt(resultSet.getString(11))));
			empresa.setCiudad(rsaClient.Encrypt(rsaBBDD.Decrypt(resultSet.getString(12))));
			empresa.setCalle(rsaClient.Encrypt(rsaBBDD.Decrypt(resultSet.getString(13))));

			this.notario_id = resultSet.getInt(14) + "";

		}

		resultSet.close();

	}

	/**
	 * Verifica la existencia del usuario en la BBDD
	 * 
	 * @throws SQLException
	 * @throws IOException
	 * @throws BadPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws NoSuchPaddingException
	 * @throws NoSuchAlgorithmException
	 * @throws UnsupportedEncodingException
	 * @throws InvalidKeyException
	 * @throws JsonSyntaxException
	 * @throws NoSuchProviderException
	 * @throws InvalidKeySpecException
	 */
	private boolean verificacionUsario() throws SQLException, JsonSyntaxException, InvalidKeyException,
			UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException,
			BadPaddingException, IOException, InvalidKeySpecException, NoSuchProviderException {

		String query = "select * from name_pass_user";

		return contrasteRecividoBBDD(listaUsuarios(conexion.buscarPS(query)));

	}

	/**
	 * Contrasta los datos de la BBDD con los recibidos por el usuario y debuelve la
	 * respuesta
	 * 
	 * Comprueba la existencia del usuario en la tabla de name_pass_user y debuelve
	 * un objeto trabajador. En caso de que no exista el usuario debuelve un objeto
	 * trabajador vacio, si existe debuelve un objeto trabajador con los datos del
	 * trabajador
	 * 
	 * @param listaUsuarios
	 * @throws IOException
	 * @throws BadPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws NoSuchPaddingException
	 * @throws NoSuchAlgorithmException
	 * @throws UnsupportedEncodingException
	 * @throws InvalidKeyException
	 * @throws JsonSyntaxException
	 * @throws NoSuchProviderException
	 * @throws InvalidKeySpecException
	 * @throws SQLException
	 */
	private boolean contrasteRecividoBBDD(ArrayList<NamePassUser> listaUsuarios)
			throws JsonSyntaxException, InvalidKeyException, UnsupportedEncodingException, NoSuchAlgorithmException,
			NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException,
			InvalidKeySpecException, NoSuchProviderException, SQLException {

		boolean aBoolean = false;

		NamePassUser cliente = gson.fromJson(crypto.desencriptar(entrada.readUTF(), passAes), NamePassUser.class);

		String nombre = rsaServer.Decrypt(cliente.getNombre());
		String pass = rsaServer.Decrypt(cliente.getPass());

		for (int i = 0; i < listaUsuarios.size(); i++) {

			if (rsaBBDD.Decrypt(listaUsuarios.get(i).getNombre()).equals(nombre)
					&& rsaBBDD.Decrypt(listaUsuarios.get(i).getPass()).equals(pass)) {

				trabajador = agregarTrabajador(listaUsuarios.get(i).getId());

				aBoolean = true;
				break;
			}
		}

		if (!aBoolean) {
			trabajador = null;
			salida.writeUTF(crypto.encriptar(gson.toJson(trabajador), passAes));
		}

		return aBoolean;
	}

	/**
	 * Agrega la informacion del trabajador
	 * 
	 * @param idNamePassUser
	 * 
	 * @return
	 * @throws SQLException
	 * @throws @throws                  IllegalBlockSizeException
	 * @throws NoSuchPaddingException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 * @throws BadPaddingException
	 * @throws NoSuchProviderException
	 * @throws InvalidKeySpecException
	 * @throws IOException
	 */
	private Trabajador agregarTrabajador(int idNamePassUser) throws SQLException, InvalidKeyException,
			NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
			InvalidKeySpecException, NoSuchProviderException, IOException {

		// Objeto trabajador
		String query = "select * from trabajadores where user_pass = " + idNamePassUser;
		trabajador = rellenarTrabajador(conexion.buscarPS(query));
		salida.writeUTF(crypto.encriptar(gson.toJson(trabajador), passAes));

		rellenarObjetoTrabajador();

		return trabajador;
	}

	/**
	 * Rellena el objeto trabajador y lo envia de manera fraccionada al cliente
	 * 
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws InvalidKeySpecException
	 * @throws NoSuchProviderException
	 * @throws SQLException
	 * @throws IOException
	 */
	private void rellenarObjetoTrabajador()
			throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException,
			BadPaddingException, InvalidKeySpecException, NoSuchProviderException, SQLException, IOException {
		// Objeto adelatoTrabajador
		String query = "select * from adelanto_trabajador where adelanto_trabajador.trabajador_id = "
				+ trabajador.getId();
		trabajador.setAdelanto_trabajador(rellenarAdelantoTrabajador(conexion.buscarPS(query)));
		salida.writeUTF(crypto.encriptar(gson.toJson(trabajador.getAdelanto_trabajador().size()), passAes));
		for (int i = 0; i < trabajador.getAdelanto_trabajador().size(); i++) {
			salida.writeUTF(crypto.encriptar(gson.toJson(trabajador.getAdelanto_trabajador().get(i)), passAes));
		}

		// objeto salida
		query = "select * from salidas where trabajador = " + trabajador.getId();
		trabajador.setSalida(rellenarSalidas(conexion.buscarPS(query)));
		salida.writeUTF(crypto.encriptar(gson.toJson(trabajador.getSalida().size()), passAes));
		for (int i = 0; i < trabajador.getSalida().size(); i++) {
			salida.writeUTF(crypto.encriptar(gson.toJson(trabajador.getSalida().get(i)), passAes));
		}

		// objeto entrada
		query = "select * from entradas where trabajador = " + trabajador.getId();
		trabajador.setEntrada(rellenarEntradas(conexion.buscarPS(query)));
		salida.writeUTF(crypto.encriptar(gson.toJson(trabajador.getEntrada().size()), passAes));
		for (int i = 0; i < trabajador.getEntrada().size(); i++) {
			salida.writeUTF(crypto.encriptar(gson.toJson(trabajador.getEntrada().get(i)), passAes));
		}

		// obeto baja
		query = "select * from bajas where trabajador = " + trabajador.getId();
		trabajador.setBaja(rellenarBajas(conexion.buscarPS(query)));
		salida.writeUTF(crypto.encriptar(gson.toJson(trabajador.getBaja().size()), passAes));
		for (int i = 0; i < trabajador.getBaja().size(); i++) {
			salida.writeUTF(crypto.encriptar(gson.toJson(trabajador.getBaja().get(i)), passAes));
		}

		// objeto contratoTrabajador
		query = "select * from contratos_trabajadores where trabajador = " + trabajador.getId();
		trabajador.setContrato_trabajador(rellenarContratoTrabajador(conexion.buscarPS(query)));
		salida.writeUTF(crypto.encriptar(gson.toJson(trabajador.getContrato_trabajador().size()), passAes));
		for (int i = 0; i < trabajador.getContrato_trabajador().size(); i++) {
			salida.writeUTF(crypto.encriptar(gson.toJson(trabajador.getContrato_trabajador().get(i)), passAes));

			query = "select * from anexos_contratos where contrato_id = "
					+ trabajador.getContrato_trabajador().get(i).getId();
			trabajador.getContrato_trabajador().get(i)
					.setAnexos_contrato(rellenarAnexosContratoTrabajador(conexion.buscarPS(query)));
			salida.writeUTF(crypto.encriptar(
					gson.toJson(trabajador.getContrato_trabajador().get(i).getAnexos_contrato().size()), passAes));

			for (int o = 0; o < trabajador.getContrato_trabajador().get(i).getAnexos_contrato().size(); o++) {
				salida.writeUTF(crypto.encriptar(
						gson.toJson(trabajador.getContrato_trabajador().get(i).getAnexos_contrato().get(o)), passAes));
			}

		}

		query = "select * from trabajadores where id = " + responsable;
		trabajador.setResposable(rellenarTrabajador(conexion.buscarPS(query)));

		salida.writeUTF(crypto.encriptar(gson.toJson(trabajador.getResposable()), passAes));

	}

	private void rellenarNuevoTrabajador(Trabajador trabajador)
			throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException,
			BadPaddingException, InvalidKeySpecException, NoSuchProviderException, SQLException, IOException {
		// Objeto adelatoTrabajador
		String query = "select * from adelanto_trabajador where adelanto_trabajador.trabajador_id = "
				+ trabajador.getId();
		trabajador.setAdelanto_trabajador(rellenarAdelantoTrabajador(conexion.buscarPS(query)));
		salida.writeUTF(crypto.encriptar(gson.toJson(trabajador.getAdelanto_trabajador().size()), passAes));
		for (int i = 0; i < trabajador.getAdelanto_trabajador().size(); i++) {
			salida.writeUTF(crypto.encriptar(gson.toJson(trabajador.getAdelanto_trabajador().get(i)), passAes));
		}

		// objeto salida
		query = "select * from salidas where trabajador = " + trabajador.getId();
		trabajador.setSalida(rellenarSalidas(conexion.buscarPS(query)));
		salida.writeUTF(crypto.encriptar(gson.toJson(trabajador.getSalida().size()), passAes));
		for (int i = 0; i < trabajador.getSalida().size(); i++) {
			salida.writeUTF(crypto.encriptar(gson.toJson(trabajador.getSalida().get(i)), passAes));
		}

		// objeto entrada
		query = "select * from entradas where trabajador = " + trabajador.getId();
		trabajador.setEntrada(rellenarEntradas(conexion.buscarPS(query)));
		salida.writeUTF(crypto.encriptar(gson.toJson(trabajador.getEntrada().size()), passAes));
		for (int i = 0; i < trabajador.getEntrada().size(); i++) {
			salida.writeUTF(crypto.encriptar(gson.toJson(trabajador.getEntrada().get(i)), passAes));
		}

		// obeto baja
		query = "select * from bajas where trabajador = " + trabajador.getId();
		trabajador.setBaja(rellenarBajas(conexion.buscarPS(query)));
		salida.writeUTF(crypto.encriptar(gson.toJson(trabajador.getBaja().size()), passAes));
		for (int i = 0; i < trabajador.getBaja().size(); i++) {
			salida.writeUTF(crypto.encriptar(gson.toJson(trabajador.getBaja().get(i)), passAes));
		}

		// objeto contratoTrabajador
		query = "select * from contratos_trabajadores where trabajador = " + trabajador.getId();
		trabajador.setContrato_trabajador(rellenarContratoTrabajador(conexion.buscarPS(query)));
		salida.writeUTF(crypto.encriptar(gson.toJson(trabajador.getContrato_trabajador().size()), passAes));
		for (int i = 0; i < trabajador.getContrato_trabajador().size(); i++) {
			salida.writeUTF(crypto.encriptar(gson.toJson(trabajador.getContrato_trabajador().get(i)), passAes));

			query = "select * from anexos_contratos where contrato_id = "
					+ trabajador.getContrato_trabajador().get(i).getId();
			trabajador.getContrato_trabajador().get(i)
					.setAnexos_contrato(rellenarAnexosContratoTrabajador(conexion.buscarPS(query)));
			salida.writeUTF(crypto.encriptar(
					gson.toJson(trabajador.getContrato_trabajador().get(i).getAnexos_contrato().size()), passAes));

			for (int o = 0; o < trabajador.getContrato_trabajador().get(i).getAnexos_contrato().size(); o++) {
				salida.writeUTF(crypto.encriptar(
						gson.toJson(trabajador.getContrato_trabajador().get(i).getAnexos_contrato().get(o)), passAes));
			}

		}

		query = "select * from trabajadores where id = " + responsable;
		trabajador.setResposable(rellenarTrabajador(conexion.buscarPS(query)));

		salida.writeUTF(crypto.encriptar(gson.toJson(trabajador.getResposable()), passAes));

	}

	/**
	 * Rellena el arraylist de anexo de contratos
	 * 
	 * @param buscarPS
	 * @return
	 * @throws SQLException
	 * @throws NoSuchProviderException
	 * @throws UnsupportedEncodingException
	 * @throws InvalidKeySpecException
	 * @throws BadPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws NoSuchPaddingException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 */
	private ArrayList<AnexosContrato> rellenarAnexosContratoTrabajador(ResultSet buscarPS) throws InvalidKeyException,
			NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
			InvalidKeySpecException, UnsupportedEncodingException, NoSuchProviderException, SQLException {
		ArrayList<AnexosContrato> anexosContrato = new ArrayList<AnexosContrato>();

		while (buscarPS.next()) {

			AnexosContrato aux = new AnexosContrato();

			aux.setId(buscarPS.getInt(1));
			aux.setTexto(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(2))));
			aux.setDescripcion(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(3))));
			aux.setObserbaciones(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(4))));

			anexosContrato.add(aux);

		}

		buscarPS.close();

		return anexosContrato;
	}

	/**
	 * Rellena el arraylist de contratos
	 * 
	 * @param buscarPS
	 * @return
	 * @throws SQLException
	 * @throws NoSuchProviderException
	 * @throws UnsupportedEncodingException
	 * @throws InvalidKeySpecException
	 * @throws BadPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws NoSuchPaddingException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 */
	private ArrayList<ContratoTrabajador> rellenarContratoTrabajador(ResultSet buscarPS) throws InvalidKeyException,
			NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
			InvalidKeySpecException, UnsupportedEncodingException, NoSuchProviderException, SQLException {

		ArrayList<ContratoTrabajador> contratoTrabajador = new ArrayList<ContratoTrabajador>();

		while (buscarPS.next()) {

			ContratoTrabajador aux = new ContratoTrabajador();

			aux.setId(buscarPS.getInt(1));
			aux.setHorario(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(2))));
			aux.setRango(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(3))));
			aux.setFecha_inicio(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(4))));
			aux.setFecha_finalizacion(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(5))));
			aux.setPago_diario(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(6))));
			aux.setFecha_despido(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(7))));
			aux.setTipo_despido(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(8))));
			aux.setPais(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(9))));
			aux.setCiudad(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(10))));
			aux.setCalle(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(11))));

			contratoTrabajador.add(aux);

		}

		buscarPS.close();

		return contratoTrabajador;
	}

	/**
	 * Rellena el arraylist de bajas
	 * 
	 * @param buscarPS
	 * @return
	 * @throws SQLException
	 * @throws NoSuchProviderException
	 * @throws UnsupportedEncodingException
	 * @throws InvalidKeySpecException
	 * @throws BadPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws NoSuchPaddingException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 */
	private ArrayList<Bajas> rellenarBajas(ResultSet buscarPS) throws InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException,
			UnsupportedEncodingException, NoSuchProviderException, SQLException {

		ArrayList<Bajas> bajas = new ArrayList<Bajas>();

		while (buscarPS.next()) {

			Bajas aux = new Bajas();

			aux.setId(buscarPS.getInt(1));
			aux.setTipo(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(2))));
			aux.setMutua(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(3))));
			aux.setExpediente(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(4))));
			aux.setNumero_policia(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(5))));
			aux.setDescripcion(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(6))));
			aux.setObservacion(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(7))));
			aux.setFecha_incio(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(8))));
			aux.setFecha_final(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(9))));

			bajas.add(aux);

		}

		buscarPS.close();

		return bajas;
	}

	/**
	 * Rellena el arraylist de entradas
	 * 
	 * @param buscarPS
	 * @return
	 * @throws SQLException
	 * @throws NoSuchProviderException
	 * @throws UnsupportedEncodingException
	 * @throws InvalidKeySpecException
	 * @throws BadPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws NoSuchPaddingException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 */
	private ArrayList<Entradas> rellenarEntradas(ResultSet buscarPS) throws InvalidKeyException,
			NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
			InvalidKeySpecException, UnsupportedEncodingException, NoSuchProviderException, SQLException {

		ArrayList<Entradas> entradas = new ArrayList<Entradas>();

		while (buscarPS.next()) {

			Entradas aux = new Entradas();

			aux.setId(buscarPS.getInt(1));
			aux.setFecha(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(2))));
			aux.setSalida(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(3))));

			entradas.add(aux);

		}

		buscarPS.close();

		return entradas;
	}

	/**
	 * Rellena el arraylist de salidas
	 * 
	 * @param buscarPS
	 * @return
	 * @throws SQLException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws InvalidKeySpecException
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchProviderException
	 */
	private ArrayList<Salidas> rellenarSalidas(ResultSet buscarPS) throws SQLException, InvalidKeyException,
			NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
			InvalidKeySpecException, UnsupportedEncodingException, NoSuchProviderException {

		ArrayList<Salidas> salidas = new ArrayList<Salidas>();

		while (buscarPS.next()) {

			Salidas aux = new Salidas();

			aux.setId(buscarPS.getInt(1));
			aux.setFecha(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(2))));
			aux.setSalida(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(3))));

			salidas.add(aux);

		}

		buscarPS.close();

		return salidas;
	}

	/**
	 * Rellena el objeto adelanto trabajadores
	 * 
	 * @param buscarPS
	 * @return
	 * @throws SQLException
	 * @throws NoSuchProviderException
	 * @throws UnsupportedEncodingException
	 * @throws InvalidKeySpecException
	 * @throws BadPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws NoSuchPaddingException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 */
	private ArrayList<AdelantoTabajador> rellenarAdelantoTrabajador(ResultSet buscarPS) throws SQLException,
			InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException,
			BadPaddingException, InvalidKeySpecException, UnsupportedEncodingException, NoSuchProviderException {

		ArrayList<AdelantoTabajador> array = new ArrayList<AdelantoTabajador>();

		while (buscarPS.next()) {

			AdelantoTabajador aux = new AdelantoTabajador();

			aux.setId(buscarPS.getInt(1));
			aux.setCantidad(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(3))));
			aux.setFecha(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(4))));

			array.add(aux);

		}

		buscarPS.close();

		return array;
	}

	/**
	 * Crea el objeto trabajador y rellena los datos del mismo
	 * 
	 * @param buscarPS
	 * @return
	 * @throws SQLException
	 * @throws BadPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws NoSuchPaddingException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 * @throws NoSuchProviderException
	 * @throws UnsupportedEncodingException
	 * @throws InvalidKeySpecException
	 */
	private Trabajador rellenarTrabajador(ResultSet buscarPS) throws SQLException, InvalidKeyException,
			NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
			InvalidKeySpecException, UnsupportedEncodingException, NoSuchProviderException {

		Trabajador trabajador = new Trabajador();
		trabajador.setId(0);

		while (buscarPS.next()) {

			trabajador.setId(buscarPS.getInt(1));
			trabajador.setDni_cif(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(2))));
			trabajador.setNumero_ss(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(3))));
			trabajador.setNombre(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(4))));
			trabajador.setApellido1(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(5))));
			trabajador.setApellido2(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(6))));
			trabajador.setFecha_nacimiento(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(7))));
			trabajador.setGenero(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(8))));
			trabajador.setEstado(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(9))));
			trabajador.setNacionalidad(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(10))));
			trabajador.setHorario(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(11))));
			trabajador.setTelefono(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(12))));
			trabajador.setEmail(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(13))));
			trabajador.setPais(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(14))));
			trabajador.setCiudad(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(15))));
			trabajador.setCalle(rsaClient.Encrypt(rsaBBDD.Decrypt(buscarPS.getString(16))));
			tipo_trabajador = rsaBBDD.Decrypt(buscarPS.getString(17));
			trabajador.setTipo_usuario(rsaClient.Encrypt(tipo_trabajador));
			responsable = rsaBBDD.Decrypt(buscarPS.getString(18));

		}

		buscarPS.close();

		return trabajador;
	}

	/**
	 * Crea un Array list con los usuarios de la BBDD
	 * 
	 * @param buscarPS
	 * @return
	 * @throws SQLException
	 */

	private ArrayList<NamePassUser> listaUsuarios(ResultSet buscarPS) throws SQLException {

		ArrayList<NamePassUser> namePassUser = new ArrayList<NamePassUser>();

		while (buscarPS.next()) {
			NamePassUser npu = new NamePassUser(buscarPS.getInt(1), buscarPS.getString(2), buscarPS.getString(3));

			namePassUser.add(npu);
		}

		buscarPS.close();

		return namePassUser;
	}

	/**
	 * Realiza la configuracion e intercambio de las claves publicas entre el
	 * cliente y el servidor
	 * 
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 * @throws JsonSyntaxException
	 * @throws InvalidKeyException
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws IOException
	 */
	private void intercambioClaves() throws NoSuchAlgorithmException, InvalidKeySpecException, JsonSyntaxException,
			InvalidKeyException, UnsupportedEncodingException, NoSuchPaddingException, IllegalBlockSizeException,
			BadPaddingException, IOException {
		// recibe la clave publica del ciente y la guarda en la variable
		rsaClient.setPublicKeyString(crypto.desencriptar(entrada.readUTF(), passAes));
		// envio la clave publica al cliente
		salida.writeUTF(crypto.encriptar(rsaServer.getPublicKeyString(), passAes));
	}
}