package servidor;

import java.io.Console;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.swing.JOptionPane;

import conexion.Conexion;
import configuracion.Configuracion;
import datos.Datos;
import rsa.RSA;

/**
 * Clase Opciones. Contiene las acciones que puede realizar el servidor una vez esta arrancado
 * 
 * @author Alberto Soria Carrillo
 *
 */
public class Opciones extends Thread {

	// Atributos
	private Conexion conexion;
	private RSA rsa;
	private Datos datos;
	private Server servidor;

	// Constructor
	public Opciones(Conexion conexion, RSA rsa, Datos datos) {
		this.conexion = conexion;
		this.rsa = rsa;
		this.datos = datos;
	}

	public Opciones() throws InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException, Throwable {
		Datos datos = new Datos();
		servidor = Configuracion.socketsBBDD(Configuracion.conexionBBDD(datos), Configuracion.rsaBBDD(), datos);
		this.conexion = servidor.conexion;
		this.rsa = servidor.rsa;
		this.datos = servidor.datos;
		servidor.start();
		
		this.sleep(1000);
		
	}

	@Override
	public void run() {

		System.out.println("\n\n****************************************************************\n");
		
		while (true) {

			System.out.println("Menu de opciones del servidor\n");

			System.out
					.println("\t1. - Agregar un nuevo usuario a la BBDD. El usuario generado es de tipo administrador");
			System.out.println("\t2. - Bloquear el servidor");
			System.out.println("\t3. - Cerrar el servidor");

			try {
				opcionesServidor(datos, conexion);
			} catch (IOException | SQLException | InvalidKeyException | NoSuchAlgorithmException
					| NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException | InvalidKeySpecException
					| NoSuchProviderException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	/**
	 * Realiza las acciones del men� de opciones del servidor
	 * 
	 * @param datos
	 * @param conexion
	 * @throws IOException
	 * @throws SQLException
	 * @throws NoSuchProviderException
	 * @throws InvalidKeySpecException
	 * @throws BadPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws NoSuchPaddingException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 */
	public void opcionesServidor(Datos datos, Conexion conexion)
			throws IOException, SQLException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException,
			IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException, NoSuchProviderException {

		Scanner in = new Scanner(System.in);

		System.out.print("\n\tEscoge una opci�n: ");
		switch (in.nextLine()) {
		case "1":
			// Crea un nuevo usuario en la BBDD de tipo administrador
			System.out.println("\n\n- Creaci�n de usuario de tipo " + Datos.adnimnistrador + "\n");
			System.out.println("Indica el nombre: ");
			String nombre = in.nextLine();
			System.out.println("Indica la contrase�a: ");
			String pass = in.nextLine();
			System.out.println("Vuelve a escribir la contrase�a: ");
			String pass2 = in.nextLine();

			if (!nombre.equalsIgnoreCase("") && !pass.equalsIgnoreCase("") && pass.equalsIgnoreCase(pass2)) {
				if (JOptionPane.showConfirmDialog(null, "�Est�s seguro que quieres crear el nuevo usuario?",
						"Creaci�n de usuario", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_NO_OPTION) {

					crearUsuario(nombre, pass);
					System.out.println("\n- Usuario creado -\n");

				} else {
					System.err.println("\n- Creaci�n de usuario cancelada -\n");
				}
			} else {
				System.out.println("Datos incorrectos");
			}

			break;
		case "2":
			// Bloquea el servidor
			String usuario = "";
			String contra = "";

			Console cons = System.console();
			Configuracion confi = new Configuracion();

			do {
				usuario = confi.readLine("--> Usuario: ");
				contra = new String(confi.readPassword("--> Contrase�a: "));
			} while (!usuario.equalsIgnoreCase(datos.user) || !contra.equalsIgnoreCase(datos.pass));

			break;
		case "3":
			// cierra el servidor						
			System.out.println("Adios");
			conexion.desconectar();
			System.exit(0);

			break;
		default:
			System.err.println("\\n\\tOpcion no valida");
			break;

		}

	}

	/**
	 * Crea un registro nuevo en la tabla name_pass_user de la base de datos y lo
	 * enlaza con un registro nuevo en la tabla trabajadores
	 * 
	 * @param nombre
	 * @param pass
	 * @throws SQLException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws InvalidKeySpecException
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchProviderException
	 */
	private void crearUsuario(String nombre, String pass) throws SQLException, InvalidKeyException,
			NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
			InvalidKeySpecException, UnsupportedEncodingException, NoSuchProviderException {

		// inserta el nuevo usuario en la tabla usuarios
		String query = "";
		query = "insert into name_pass_user(nombre, pass) " + "values ('" + rsa.Encrypt(nombre) + "', '"
				+ rsa.Encrypt(pass) + "')";
		conexion.insertarPS(query);

		// busca el id del usuario y y crea un nuevo s trabajador en la tabla trabajadores enlazandolo con el usuario
		query = "Select max(id) from name_pass_user";
		query = "insert into trabajadores(user_pass, tipo_usuario) " + "values (" + maxId(conexion.buscarPS(query))
				+ ", '" + rsa.Encrypt(Datos.adnimnistrador) + "')";
		conexion.insertarPS(query);
		
		// busca el id del trabajador 
		query = "Select max(id) from trabajadores";
		int idTrabajador = maxId(conexion.buscarPS(query));
		
		// Crea un nuevo contrato de empresa en la tabla contrato_empresa
		query = "insert into contrato_empresa values()";
		conexion.insertarPS(query);
		
		// busca el id del contrato de empresa
		query = "Select max(id) from contrato_empresa";
		int idEmpresa = maxId(conexion.buscarPS(query));
		
		// inserta un nuevo contrato de trabajador con los datos de idTrabajador y idEmpresa
		query = "insert into contratos_trabajadores(trabajador,empresa) values("+idTrabajador+", "+idEmpresa+")";
		conexion.insertarPS(query);

	}

	/**
	 * Debuelve el resultado de la query, 'select max(id) from
	 * <table>
	 * 
	 * @param buscarPS
	 * @return
	 * @throws SQLException
	 */
	private static int maxId(ResultSet buscarPS) throws SQLException {

		int maxId = 0;

		while (buscarPS.next()) {

			maxId = buscarPS.getInt(1);

		}

		return maxId;
	}

}
