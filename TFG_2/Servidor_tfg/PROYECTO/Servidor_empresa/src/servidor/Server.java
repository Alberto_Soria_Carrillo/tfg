package servidor;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import conexion.Conexion;
import datos.Datos;
import rsa.RSA;

/**
 * Clase Servidor. Contiene lo objetos de conexion y el bucle para el pool de conexiones
 * 
 * @author Alberto Soria Carrillo
 *
 */
public class Server extends Thread {

	// Atributos
	public ServerSocket serverSocket;
	public Socket socket;
	private ServidorHilo servidorHilo;
	private int idUsers = 0;
	private int puertoSocket;
	public Conexion conexion;
	public RSA rsa;
	public Datos datos;

	// Constructor
	public Server(int puertoSocket, Conexion conexion, RSA rsa, Datos datos) {

		this.puertoSocket = puertoSocket;
		this.conexion = conexion;
		this.rsa = rsa;
		this.datos = datos;

		try {
			System.out.println("\n- Prueba de conexi�n -\n");
			this.conexion.conectar();
			//this.conexion.desconectar();
			System.out.println("- Conexi�n correcta -\n");
		} catch (Exception e) {
			System.out.println("Fallo de conexi�n");

			e.printStackTrace();

			System.exit(0);
		}

	}
	
	@Override
	public void run() {

		System.out.println("Iniciando el servidor... ");

		try {
			// Se crea el objeto ServerSocket
			serverSocket = new ServerSocket(Integer.valueOf(this.puertoSocket));
			System.out.println("Inicio terminado");
			// bucle que permite la recepcion de varios clientes
			while (!serverSocket.isClosed()) {
				// Aceptacion de la conexion del cliente
				socket = serverSocket.accept();
				// Creacion de un servicor con control de hilos
				idUsers++;
				servidorHilo = new ServidorHilo(socket, idUsers, conexion, rsa);
				// inicio del servicor por hilos
				servidorHilo.start();

			}
		} catch (IOException ex) {
			Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
